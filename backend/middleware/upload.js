const multer = require('multer');
const imageStoreFolder = require('../config/keys').imagesStore;

const storage = multer.diskStorage({
    destination(req, file, cb) {
        cb(null, imageStoreFolder);
    },
    filename(req, file, cb) {
        cb(null, `${file.originalname}`);
    }
})

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg' ) {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

const limits = {
    fileSize: 4096 * 4096 * 25
}


module.exports = multer({storage, fileFilter, limits});
