const TOKEN_TYPES = Object.freeze({
  ACCESS: 'access',
  REFRESH: 'refresh'
});
module.exports.TOKEN_TYPES = TOKEN_TYPES;
