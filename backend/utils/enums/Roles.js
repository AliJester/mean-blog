const ROLES = Object.freeze({
  OWNER_YO: 'owner',
  ADMIN: 'admin',
  USER: 'user'
});
module.exports.ROLES = ROLES;
