const Tag = require('../../models/tag');
const errorHandler = require('../../utils/errorHandler');

module.exports.getAllTags = async function (req, res) {
    try {
        const tags = await Tag.find();
        res.status(200).json(tags);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.getById = async function (req, res) {
    try {
        const tag = await Tag.findById(req.params.id);
        res.status(200).json(tag);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.update = async function (req, res) {
    try {
        const tag = await Tag.findOneAndUpdate(
            {_id: req.params.id},
            {$set: req.body},
            {new: true}
        );
        res.status(200).json({success: true, message: "Tag has been updated."});
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.remove = async function (req, res) {
    try {
        await Tag.remove({_id: req.params.id});
        res.status(200).json({
            message: 'Tag was removed.'
        });
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.create = async function (req, res) {
    let newTag = new Tag({
        name: req.body.name,
    });

    const tag = await Tag.findOne({'name': req.body.name});
    if (tag) {
        res.status(200).json(
            {
                success: true,
                message: "Tag exists",
                newTagId: tag.id,
            });
    } else {
        await this.addTag(newTag, (err) => {
            if (err) {
                res.json({success: false, message: "Tag has not been added."});
            } else {
                res.status(200).json(
                    {
                        success: true,
                        message: "Tag has been added.",
                        newTagId: newTag.id,
                    });
            }
        });
    }
}

addTag = function (newTag, callback) {
    newTag.save(callback);
};
