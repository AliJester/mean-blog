const Comment = require('../../models/comments/comment');
const AdditionalData = require('../../models/additionalData');
const errorHandler = require('../../utils/errorHandler');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');

module.exports.getByPostId = async function (req, res) {
    try {
        const comments = await Comment.find({'postId': req.params.postId});
        res.status(200).json(comments);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.create = async function (req, res) {
    const authorName = this.getCommentAuthorName(req);
    let newComment = new Comment({
        postId: req.body.postId,
        postCoolId: req.body.postCoolId,
        text: req.body.text,
        author: authorName,
        createdDate: req.body.createdDate,
        subComments: [],
    });

    this.addComment(newComment, (err) => {
        if (err) {
            res.json({success: false, message: "Comment has not been added."});
        } else {
            res.status(200).json(
                {
                    success: true,
                    message: "Comment has been added.",
                    newCommentId: newComment.id,
                });
        }
    });
}

addComment = function (newComment, callback) {
    newComment.save(callback);
};

module.exports.remove = async function (req, res) {
    try {
        const parentId = req.query.parentId;
        if (parentId === 'null') {
            await Comment.remove({_id: req.params.id});
            res.status(200).json({
                message: 'Comment was removed.'
            });
        } else {
            await Comment.update(
                {_id: parentId},
                {$pull: {subComments: {_id: req.params.id}}},
                {multi: true}
            );
            res.status(200).json({
                message: 'SubComment was removed.'
            });
        }
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.update = async function (req, res) {
    const authorName = this.getCommentAuthorName(req);
    req.body.author = authorName;
    try {
        const comment = await Comment.findOneAndUpdate(
            {_id: req.params.id},
            {$push: {subComments: {...req.body}}},
            {new: true}
        );
        res.status(200).json({success: true, message: "Comment has been updated.", comment: comment});
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.getAllComments = async function (req, res) {
    try {
        const comments = await Comment.find();
        res.status(200).json(comments);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.getNewComments = async function (req, res) {
    const comments = await getNewComments()
    return res.status(200).json(comments);
}

module.exports.getNewCommentsAmount = async function (req, res) {
    const comments = await getNewComments()
    return res.status(200).json({newCommentsAmount: comments.length});
}

async function getNewComments() {
    try {
        const additionalData = await AdditionalData.findOne();
        let dateFrom;
        if (additionalData) {
            dateFrom = additionalData.commentsLastSeenDate;
        } else {
            dateFrom = Date.now();
        }
        const comments = await Comment.find({
                $or: [
                    {"createdDate": {$gte: new Date(dateFrom)}},
                    {subComments: {$elemMatch: {createdDate: {$gte: new Date(dateFrom)}}}}
                ]
            }
        );
        return comments;
    } catch (e) {
        errorHandler(res, e);
    }
}

getCommentAuthorName = function (req) {
    let authorName = req.body.author;
    if (authorName === 'AliJester') {
        if (req && req.headers && req.headers.cookie) {
            const cookies = req.headers.cookie;
            const cookieArray = cookies.split(';').filter((cookie) => cookie.includes('SESSIONID='));
            if (cookieArray && cookieArray.length > 0) {
                const cookie = cookieArray[0].trim();
                const decodedToken = jwt.decode(cookie.slice('SESSIONID='.length));
                try {
                    const result = jwt.verify(cookie.slice('SESSIONID='.length), keys.jwt.secret);
                    if (result && result.login !== 'AliJester') {
                        authorName = 'Some Reader';
                    }
                } catch (e) {
                    authorName = 'Some Reader';
                }
            } else {
                authorName = 'Some Reader';
            }
        } else {
            authorName = 'Some Reader';
        }
    }
    return authorName;
}
