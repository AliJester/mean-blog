const Post = require('../../models/post');
const Tag = require('../../models/tag');
const Category = require('../../models/category');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const errorHandler = require('../../utils/errorHandler');
const url = require('url');

module.exports.getAllBlogPosts = async function (req, res) {
    const query = req.query;
    let posts;
    try {
        if (Object.keys(query).length !== 0) {
            const limit = query.limit ? Number.parseInt(query.limit) : 0;
            const curPage = query.page ? Number.parseInt(query.page) : 0;
            const offset = curPage == 0 ? 0 : curPage - 1;
            if (query.isMain) {
                posts = await Post.find({'isMain': true, 'isHidden': false})
                    .populate('tags')
                    .populate("category")
                    .limit(2)
                    .sort({'createdDate': -1});
            } else {
                if (query.tag !== undefined && query.tag !== 'undefined' && query.tag) {
                    tag = await Tag.findOne({'name': query.tag});
                    posts = await Post.find({'_id': {$in: tag.posts}, 'isHidden': false})
                        .populate('tags')
                        .populate("category")
                        .skip(limit * (offset))
                        .limit(limit)
                        .sort({'createdDate': -1});
                } else if (query.category !== undefined && query.category !== 'undefined' && query.category) {
                    category = await Category.findOne({'name': query.category});
                    posts = await Post.find({'category': category._id, 'isHidden': false})
                        .populate('tags')
                        .populate("category")
                        .skip(limit * (offset))
                        .limit(limit)
                        .sort({'createdDate': -1});
                } else if (query.author !== undefined && query.author !== 'undefined' && query.author) {
                    posts = await Post.find({'author': query.author, 'isHidden': false})
                        .populate('tags')
                        .populate("category")
                        .skip(limit * (offset))
                        .limit(limit)
                        .sort({'createdDate': -1});
                } else {
                    posts = await Post.find({'isHidden': false}, {})
                        .populate('tags')
                        .populate("category")
                        .skip(limit * (offset))
                        .limit(limit)
                        .sort({'createdDate': -1});
                }
            }
        } else {
            posts = await Post.find()
                .populate('tags')
                .populate("category");
        }
        res.status(200).json(posts);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.getByCoolId = async function (req, res) {
    try {
        const post = await Post.findOne({'coolId': req.params.id})
            .populate('tags')
            .populate("category");
        return isTokenValid(post, req.query) ? res.status(200).json(post) : res.status(404).json(null);
    } catch (e) {
        errorHandler(res, e);
    }
}

const isTokenValid = (post, query) => {
    let result = false;
    if (post.availableByToken && post.availabilityToken) {
        if (query.token === post.availabilityToken) {
            const timestampFromJWT = jwt.decode(query.token).exp
            const currentTimestamp = Date.now();
            result = timestampFromJWT < currentTimestamp;
        }
    } else {
        result = true;
    }
    return result;
}

module.exports.getPostsAmount = async function (req, res) {
    const query = req.query;
    let postsAmount;
    try {
        if (query.tag !== undefined && query.tag !== 'undefined' && query.tag) {
            tag = await Tag.findOne({'name': query.tag});
            postsAmount = await Post.find({'_id': {$in: tag.posts}, 'isHidden': false}).countDocuments();
        } else if (query.category !== undefined && query.category !== 'undefined' && query.tag) {
            category = await Category.findOne({'name': query.category});
            postsAmount = await Post.find({'category': category._id, 'isHidden': false}).countDocuments();
        } else {
            postsAmount = await Post.find({'isHidden': false}).countDocuments();
        }
        res.status(200).json(postsAmount);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.getMainPostsAmount = async function (req, res) {
    try {
        const mainPostsAmount = await Post.find({'isMain': true}).countDocuments();
        res.status(200).json(mainPostsAmount);
    } catch (e) {
        errorHandler(res, e);
    }
}
