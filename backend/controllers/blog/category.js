const Category = require('../../models/category');
const Post = require('../../models/post');
const errorHandler = require('../../utils/errorHandler');

module.exports.getAllCategories = async function (req, res) {
    try {
        const categories = await Category.find().sort({'name': -1});
        res.status(200).json(categories);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.getById = async function (req, res) {
    try {
        const category = await Category.findById(req.params.id);
        res.status(200).json(category);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.update = async function (req, res) {
    try {
        const category = await Category.findOneAndUpdate(
            {_id: req.params.id},
            {$set: req.body},
            {new: true}
        );
        res.status(200).json({success: true, message: "Category has been updated."});
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.remove = async function (req, res) {
    let posts;
    try {
        posts = await Post.find({'category': req.params.id});
        if (posts.length > 0) {
            errorHandler(res, 'Category is used.');
        } else {
            await Category.remove({_id: req.params.id});
            res.status(200).json({
                message: 'Category was removed.'
            });
        }
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.create = function (req, res) {
    let newCategory = new Category({
        name: req.body.name,
    });

    this.addCategory(newCategory, (err) => {
        if (err) {
            res.json({success: false, message: "Category has not been added."});
        } else {
            res.status(200).json(
                {
                    success: true,
                    message: "Category has been added.",
                    newCategoryId: newCategory.id,
                });
        }
    });
}

addCategory = function (newCategory, callback) {
    newCategory.save(callback);
};
