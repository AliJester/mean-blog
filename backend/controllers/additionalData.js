const AdditionalData = require('../models/additionalData');
const errorHandler = require('../utils/errorHandler');

module.exports.getCommentsLastSeenDate = async function (req, res) {
    try {
        const additionalData = await AdditionalData.findOne();
        let dateFrom;
        if (additionalData) {
            dateFrom = additionalData.commentsLastSeenDate;
        } else {
            dateFrom = Date.now();
        }
        res.status(200).json({success: true, message: "CommentsLastSeenDate has been updated.", commentsLastSeenDate:dateFrom});
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.setCommentsLastSeenDate = async function (req, res) {
    try {
        let commentsLastSeenDate = req.body.commentsLastSeenDate;
        if (!commentsLastSeenDate) {
            commentsLastSeenDate = Date.UTC(2021, 1, 1);
        }
        const update = {commentsLastSeenDate: commentsLastSeenDate};
        const options = {upsert: true, new: true, setDefaultsOnInsert: true};

        await AdditionalData.findOneAndUpdate({name:'settings'}, update, options);
        res.status(200).json({success: true, message: "CommentsLastSeenDate has been updated.", commentsLastSeenDate});
    } catch (e) {
        errorHandler(res, e);
    }
}
