const Post = require('../../models/post');
const Tag = require('../../models/tag');
const Category = require('../../models/category');
const errorHandler = require('../../utils/errorHandler');

module.exports.getAllAdminPosts = async function (req, res) {
    const query = req.query;
    let posts;
    try {
        if (Object.keys(query).length !== 0) {
            const limit = query.limit ? Number.parseInt(query.limit) : 0;
            const curPage = query.page ? Number.parseInt(query.page) : 0;
            const offset = curPage == 0 ? 0 : curPage - 1;
            if (String(query.admin).toLowerCase() == "true") {
                posts = await Post.find({}, {
                    mainPostBriefDesc: 0,
                    text: 0,
                    photo: 0,
                    cutPosition: 0,
                    tags: 0,
                    availableByToken: 0,
                    availabilityToken: 0,
                })
                    //.populate('tags')
                    .populate("category")
                    .skip(limit * (offset))
                    .limit(limit)
                    .sort({'createdDate': -1});
            }
        } else {
            posts = await Post.find()
                .populate('tags')
                .populate("category");
        }
        //setTimeout(() => {
        res.status(200).json(posts);
        //}, 1500)

    } catch (e) {
        errorHandler(res, e);
    }
}


module.exports.getById = async function (req, res) {
    try {
        const post = await Post.findById(req.params.id)
            .populate('tags')
            .populate("category");
        res.status(200).json(post);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.getByCoolId = async function (req, res) {
    try {
        const post = await Post.findOne({'coolId': req.params.id})
            .populate('tags')
            .populate("category");

        //const post = await Post.findById('5fd6a277acf01b2e7c4f9845');
        res.status(200).json(post);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.update = async function (req, res) {
    try {
        const postBeforeUpdate = await Post.findOne(
            {_id: req.params.id}
        );
        const post = await Post.findOneAndUpdate(
            {_id: req.params.id},
            {$set: req.body},
            {new: true}
        );
        const currentPostTags = post.tags;
        const postBeforeUpdateTags = postBeforeUpdate.tags;
        const tagsToRemove = postBeforeUpdateTags.filter((tag) => {
            return currentPostTags.indexOf(tag) == -1;
        });
        const postForTag = new Post({_id: req.params.id})
        const postsArray = [];
        postsArray.push(postForTag);
        const tagsToUpdateFromReq = req.body.tags;
        tagsToUpdateFromReq.map(async tag => {
            await Tag.updateOne(
                {_id: tag._id},
                {$addToSet: {posts: postsArray}},
                {new: true}
            );
        });
        tagsToRemove.map(async tag => {
            await Tag.updateOne(
                {_id: tag._id},
                {$pull: {posts: post._id}},
            );
        });
        res.status(200).json({success: true, message: "Post has been updated."});
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.remove = async function (req, res) {
    try {
        await Post.remove({_id: req.params.id});
        res.status(200).json({
            message: 'Post was removed'
        });
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.create = async function (req, res) {
    let newPost = new Post({
        category: req.body.category,
        title: req.body.title,
        photo: req.body.photo,
        text: req.body.text,
        author: req.body.author,
        createdDate: req.body.createdDate,
        updatedDate: req.body.updatedDate,
        cutPosition: req.body.cutPosition,
        coolId: req.body.coolId,
        isHidden: req.body.isHidden,
        isMain: req.body.isMain,
        availabilityToken: req.body.availabilityToken,
        availableByToken: req.body.availableByToken,
        tags: req.body.tags,
    });

    // Tag.findOneAndUpdate(
    //     {_id: tag._id},
    //     { $push: { posts: newPost.id } },
    //     { new: true, useFindAndModify: false }
    // );
    await this.addPost(newPost, (err, user) => {
        if (err) {
            res.json({success: false, message: "Post has not been added."});
        } else {
            const tags = req.body.tags;
            const post = new Post({_id: newPost.id})
            const postsArray = [];
            postsArray.push(post);
            tags.map(async tag => {
                await Tag.findOneAndUpdate(
                    {_id: tag._id},
                    {$push: {posts: postsArray}},
                    {new: true}
                );
            });
            res.status(200).json(
                {
                    success: true,
                    message: "Post has been added.",
                    newPostId: newPost.id,
                });
        }
    });
}

module.exports.getAllPostsAmount = async function (req, res) {
    try {
        const postsAllAmount = await Post.countDocuments();
        res.status(200).json(postsAllAmount);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.getPostsAmount = async function (req, res) {
    const query = req.query;
    let postsAmount;
    try {
        if (query.tag !== undefined && query.tag !== 'undefined' && query.tag) {
            tag = await Tag.findOne({'name': query.tag});
            postsAmount = await Post.find({'_id': {$in: tag.posts}, 'isHidden': false}).countDocuments();
        } else if (query.category !== undefined && query.category !== 'undefined' && query.tag) {
            category = await Category.findOne({'name': query.category});
            postsAmount = await Post.find({'category': category._id, 'isHidden': false}).countDocuments();
        } else {
            postsAmount = await Post.find({'isHidden': false}).countDocuments();
        }
        res.status(200).json(postsAmount);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.getMainPostsAmount = async function (req, res) {
    try {
        const mainPostsAmount = await Post.find({'isMain': true}).countDocuments();
        res.status(200).json(mainPostsAmount);
    } catch (e) {
        errorHandler(res, e);
    }
}

addPost = function (newPost, callback) {
    newPost.save(callback);
};
