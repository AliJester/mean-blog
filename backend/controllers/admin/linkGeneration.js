const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const errorHandler = require('../../utils/errorHandler');

module.exports.generate = async function (req, res) {
    const time = req.query.availableTime;
    const token = jwt.sign({
    }, keys.jwt.secret, {expiresIn: time});
    //setTimeout(() => {
        res.status(200)
            .json({
                token: token
            });
    //}, 3000);

}

