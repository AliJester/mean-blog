const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const keys = require('../config/keys');
const errorHandler = require('../utils/errorHandler');

module.exports.getUserByLogin = function (login, callback) {
    const query = {login: login};
    User.findOne(query, callback);
};

module.exports.getUserById = function (id, callback) {
    User.findById(id, callback);
};

module.exports.addUser = function (newUser, callback) {
    bcrypt.genSalt(10, function (err, salt) {
        bcrypt.hash(newUser.password, salt, function (err, hash) {
            if (err) throw err;
            newUser.password = hash;
            newUser.save(callback);
        });
    });
};

module.exports.comparePass = function (passFromUser, userDbPass, callback) {
    bcrypt.compare(passFromUser, userDbPass, (err, isMatch) => {
        if (err) throw err;
        callback(null, isMatch);
    });
};

module.exports.login = async function (req, res) {
    let user = await User.findOne({email: req.body.login});
    if (!user) {
        user = await User.findOne({login: req.body.login});
    }
    if (user) {
        const passwordResult = bcrypt.compareSync(req.body.password, user.password);
        if (passwordResult) {
            const token = jwt.sign({
                email: user.email,
                login: user.login,
                //role: user.role,
                userId: user._id
            }, keys.jwt.secret, {expiresIn: 3600});

            res.cookie('SESSIONID', `${token}`, {httpOnly: true, secure: true, sameSite: 'none'})
            res.status(200)
                .json({
                    //token: `Bearer ${token}` токен для локалсторадж
                    token: 'anyToken', //токен для куки он для нас не важен
                    author: user.name
                });
        } else {
            res.status(401).json({
                message: 'Login or password is incorrect!'
            });
        }
    } else {
        res.status(404).json({
            message: 'User not found.'
        });
    }
}

module.exports.autoLogin = function (req, res) {
    res.status(200).json({
        token: `someToken`
    });
}

module.exports.logout = async function (req, res) {
    res.cookie('SESSIONID', '', {})
    res.status(200)
        .json({
            //token: `Bearer ${token}` токен для локалсторадж
            //token: 'anyToken' //токен для куки он для нас не важен
        });
}


module.exports.register = async function (req, res) {
    const userByEmail = await User.findOne({email: req.body.email})
    if (userByEmail) {
        res.status(409).json({
            message: 'Email already exists!'
        })
    } else {
        const userByLogin = await User.findOne({login: req.body.login})
        if (userByLogin || req.body.login !== 'AliJester') {
            res.status(409).json({
                message: 'Login already exists!'
            })
        } else {
            const salt = bcrypt.genSaltSync();
            const password = req.body.password;
            const newUser = new User({
                name: req.body.name,
                login: req.body.login,
                email: req.body.email,
                password: bcrypt.hashSync(password, salt)
            });
            try {
                await newUser.save();
                res.status(201).json(newUser);
            } catch (e) {
                errorHandler(res, e);
            }
        }
    }
}
