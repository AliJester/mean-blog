const Title = require('../../models/settings/title');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const keys = require('../../config/keys');
const errorHandler = require('../../utils/errorHandler');
const titleId = 'TitleOnlyOneId';

module.exports.getTitle = async function (req, res) {
    try {
        const title = await Title.findOne({id: titleId});
        res.status(200).json(title);
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.createOrUpdate = async function (req, res) {
    const title = {
        id: titleId
    };
    if (req.files) {
        req.files.map(file => {
            if (file.originalname.startsWith('night')) {
                title.nightImageSrc = changeImgPath(file.path)
            }
            if (file.originalname.startsWith('morning')) {
                title.morningImageSrc = changeImgPath(file.path)
            }
            if (file.originalname.startsWith('day')) {
                title.dayImageSrc = changeImgPath(file.path)
            }
            if (file.originalname.startsWith('evening')) {
                title.eveningImageSrc = changeImgPath(file.path)
            }
            if (file.originalname.startsWith('default')) {
                title.defaultImageSrc = changeImgPath(file.path)
            }
        });
    }
    try {
        await Title.findOneAndUpdate(
            {id: titleId},
            {$set: title},
            {new: true, upsert: true},
        );
        res.status(200).json({success: true, message: "Title has been updated."});
    } catch (e) {
        errorHandler(res, e);
    }
}

module.exports.create = async function (req, res) {
    const title = new Title({
        id: titleId,
    });
    if (req.files) {
        req.files.map(file => {
            if (file.originalname.startsWith('night')) {
                title.nightImageSrc = changeImgPath(file.path)
            }
            if (file.originalname.startsWith('morning')) {
                title.morningImageSrc = changeImgPath(file.path)
            }
            if (file.originalname.startsWith('day')) {
                title.dayImageSrc = changeImgPath(file.path)
            }
            if (file.originalname.startsWith('evening')) {
                title.eveningImageSrc = changeImgPath(file.path)
            }
            if (file.originalname.startsWith('default')) {
                title.defaultImageSrc = changeImgPath(file.path)
            }
        });
    }
    await addTitle(title, (err) => {
        if (err) {
            res.json({success: false, message: "Title has not been added."});
        } else {
            res.status(200).json(
                {
                    success: true,
                    message: "Title has been added.",
                    titleId: title.id,
                });
        }
    });
}

const changeImgPath = (imagePath) => {
    return imagePath.replace(keys.imagesStore, keys.imagesAddress);
}

const addTitle = (newTitle, callback) => {
    newTitle.save(callback);
};
