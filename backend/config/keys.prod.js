module.exports = {
    mongoURI: process.env.MONGO_URI,
    secret: process.env.SECRET,
    imagesStore: process.env.IMAGE_STORE,
    imagesAddress: process.env.IMAGE_ADDRESS,
    jwt: {
        secret: process.env.JWT,
        tokens: {
            access: {
                type: 'access',
                expiresIn: '15m'
            },
            refresh: {
                type: 'refresh',
                expiresIn: '1h'
            }
        }
    },
}
