const express = require('express');
const router = express.Router();
const passport = require('passport');
const categoryController = require('../../controllers/blog/category');

router.get('/', categoryController.getAllCategories);
router.get('/:id', categoryController.getById);
router.delete('/:id', passport.authenticate('jwt', {session: false}), categoryController.remove);
router.patch('/:id', passport.authenticate('jwt', {session: false}), categoryController.update);
router.post('/', passport.authenticate('jwt', {session: false}), categoryController.create);


module.exports = router;
