const express = require('express');
const router = express.Router();
const passport = require('passport');
const commentController = require('../../../controllers/blog/comment');

router.get('/all', commentController.getAllComments);
router.get('/new', passport.authenticate('jwt', {session: false}), commentController.getNewComments);
router.get('/new/count', passport.authenticate('jwt', {session: false}), commentController.getNewCommentsAmount);
router.get('/:postId', commentController.getByPostId);
router.post('/', commentController.create);
router.delete('/:id', passport.authenticate('jwt', {session: false}), commentController.remove);
router.patch('/:id', commentController.update);

module.exports = router;
