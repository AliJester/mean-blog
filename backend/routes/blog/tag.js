const express = require('express');
const router = express.Router();
const passport = require('passport');
const tagController = require('../../controllers/blog/tag');

router.get('/', tagController.getAllTags);
router.get('/:id', tagController.getById);
router.delete('/:id', passport.authenticate('jwt', {session: false}), tagController.remove);
router.patch('/:id', passport.authenticate('jwt', {session: false}), tagController.update);
router.post('/', passport.authenticate('jwt', {session: false}), tagController.create);


module.exports = router;
