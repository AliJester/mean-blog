const express = require('express');
const router = express.Router();
const blogPostController = require('../../controllers/blog/post');

router.get('/', blogPostController.getAllBlogPosts);
router.get('/postsAmount', blogPostController.getPostsAmount);
router.get('/mainPostsAmount', blogPostController.getMainPostsAmount);
router.get('/:id', blogPostController.getByCoolId);

module.exports = router;
