const express = require('express');
const router = express.Router();
const user = require('../controllers/user');

router.post('/registration', user.register);
router.post('/auth', user.login);

module.exports = router;
