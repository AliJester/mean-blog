const express = require('express');
const router = express.Router();
const upload = require('../../middleware/upload')
// const passport = require('passport');
// const jwt = require('jsonwebtoken');
// const config = require('../../config/keys');
const titleController = require('../../controllers/settings/title');


router.get('/', titleController.getTitle);
router.post('/', upload.array('images', 5), titleController.createOrUpdate);


module.exports = router;
