const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Post = require('../models/post');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/keys');
const test = require('../controllers/test');

// router.post('/registration', user.register);
// router.post('/auth', user.login);
// router.post('/dashboard', /*passport.authenticate('jwt', {session: false}),*/ user.dashboard);

router.get('/test', test.test)
// router.get('/post/:id', post.getById)
// router.delete('/post/:id', post.remove)

module.exports = router;