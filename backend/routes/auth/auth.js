const express = require('express');
const router = express.Router();
const passport = require('passport');
//const jwt = require('jsonwebtoken');
//const config = require('../config/keys');
const user = require('../../controllers/user');

router.post('/registration', user.register);
router.post('/logout', passport.authenticate('jwt', {session: false}), user.logout);
router.post('/auth', user.login);
router.get('/auth', passport.authenticate('jwt', {session: false}), user.autoLogin);

module.exports = router;
