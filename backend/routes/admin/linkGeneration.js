const express = require('express');
const router = express.Router();
const passport = require('passport');
const linkGenerationController = require('../../controllers/admin/linkGeneration');

router.get('/', passport.authenticate('jwt', {session: false}), linkGenerationController.generate);

module.exports = router;
