const express = require('express');
const router = express.Router();
const passport = require('passport');
const adminPostController = require('../../controllers/admin/post');

router.get('/', passport.authenticate('jwt', {session: false}), adminPostController.getAllAdminPosts);
router.get('/postsAmount', passport.authenticate('jwt', {session: false}), adminPostController.getPostsAmount);
router.get('/allPostsAmount', passport.authenticate('jwt', {session: false}), adminPostController.getAllPostsAmount);
router.get('/mainPostsAmount', passport.authenticate('jwt', {session: false}), adminPostController.getMainPostsAmount);
router.get('/:id', passport.authenticate('jwt', {session: false}), adminPostController.getByCoolId);
router.delete('/:id', passport.authenticate('jwt', {session: false}), adminPostController.remove);
router.patch('/:id', passport.authenticate('jwt', {session: false}), adminPostController.update);
router.post('/', passport.authenticate('jwt', {session: false}), adminPostController.create);

module.exports = router;
