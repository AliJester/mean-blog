const express = require('express');
const router = express.Router();
const passport = require('passport');
const additionalDataController = require('../controllers/additionalData');

router.get('/comments/lastSeenDate', passport.authenticate('jwt', {session: false}), additionalDataController.getCommentsLastSeenDate);
router.post('/comments/lastSeenDate', passport.authenticate('jwt', {session: false}), additionalDataController.setCommentsLastSeenDate);

module.exports = router;
