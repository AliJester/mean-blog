const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TagSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    posts: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Post',
            required: false,
        }
    ]
});

const Tag = module.exports = mongoose.model('Tag', TagSchema);


