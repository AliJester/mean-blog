const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = mongoose.Schema({
    category: {
        ref: 'Category',
        type: Schema.Types.ObjectId,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    photo: {
        type: String,
        required: false
    },
    text: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: false
    },
    createdDate: {
        type: String,
        required: true
    },
    updatedDate: {
        type: String,
        required: true
    },
    cutPosition: {
        type: Number,
        required: false
    },
    isMain: {
        type: Boolean,
        required: false,
        default: false
    },
    mainPostBriefDesc: {
        type: String,
        required: false,
    },
    isHidden: {
        type: Boolean,
        required: false,
        default: false
    },
    availableByToken: {
        type: Boolean,
        required: false,
        default: false
    },
    availabilityToken: {
        type: String,
        required: false,
    },
    coolId: {
        type: String,
        required: false,
    },
    tags: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Tag',
            required: false,
        }
    ]
});

const Post = module.exports = mongoose.model('Post', PostSchema);


