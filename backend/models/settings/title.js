const mongoose = require('mongoose');

const TitleSchema = mongoose.Schema({
    nightImageSrc: {
        type: String,
        default: '',
    },
    morningImageSrc: {
        type: String,
        default: '',
    },
    dayImageSrc: {
        type: String,
        default: '',
    },
    eveningImageSrc: {
        type: String,
        default: '',
    },
    defaultImageSrc: {
        type: String,
        default: '',
    },
    id: {
        type: String,
        default: 'TitleOnlyOneId',
    },
    text: {
        type: String,
        default: '',
    }
});

const Title = module.exports = mongoose.model('title', TitleSchema);


