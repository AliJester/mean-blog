const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const config = require('../config/keys');
const { ROLES } = require('../utils/enums/Roles');

const UserSchema = mongoose.Schema({
    name: {
        type: String
    },
    email: {
        type: String,
        required: true
    },
    login: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: Object.values(ROLES),
        default: ROLES.USER
    },
});

module.exports = mongoose.model('users', UserSchema);

