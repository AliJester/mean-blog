const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentSchema = mongoose.Schema({
    postId: {
        type: Schema.Types.ObjectId,
        ref: 'Post',
        required: true,
    },
    postCoolId: {
        type: String,
        required:true,
    },
    text: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    createdDate: {
        type: Date,
        required: true
    },
    subComments: [
        {
            //_id : false,
            text: {
                type: String,
                required: true
            },
            author: {
                type: String,
                required: true
            },
            replyPostAuthor: {
                type: String,
                required: true
            },
            replyPostId: {
                type: String,
                required: true
            },
            createdDate: {
                type: Date,
                required: true
            },
        }
    ]
});

const Comment = module.exports = mongoose.model('Comment', CommentSchema);


