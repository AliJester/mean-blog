const mongoose = require('mongoose');

const SubCommentSchema = mongoose.Schema({
    text: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: false
    },
    replyPostAuthor: {
        type: String,
        required: true
    },
    replyPostId: {
        type: String,
        required: true
    },
    createdDate: {
        type: String,
        required: true
    },
});

const SubComment = module.exports = mongoose.model('SubComment', SubCommentSchema);


