const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AdditionalDataSchema = mongoose.Schema({
    commentsLastSeenDate: {
        type: Date,
        required: true
    },
    name: {
        type: String,
        required: false,
        default: 'settings'
    },
});

const AdditionalData = module.exports = mongoose.model('AdditionalData', AdditionalDataSchema);


