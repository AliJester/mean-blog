const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport');
const path = require('path');
const morgan = require('morgan');

const config = require('./backend/config/keys');
const authRoutes = require('./backend/routes/auth/auth');
//const adminRoutes = require('./backend/routes/admin');
const blogPostRoutes = require('./backend/routes/blog/post');
const adminPostRoutes = require('./backend/routes/admin/post');
const linkGenerationRoutes = require('./backend/routes/admin/linkGeneration');
const categoryRoutes = require('./backend/routes/blog/category');
const tagRoutes = require('./backend/routes/blog/tag');
const commentRoutes = require('./backend/routes/blog/comments/comment');
const titleRoutes = require('./backend/routes/settings/title');
const additionalDataRoutes = require('./backend/routes/additionalData');
const testRoutes = require('./backend/routes/test');

const app = express();

app.use(passport.initialize());
app.use(passport.session());

require('./backend/middleware/passport')(passport);
//
app.use(morgan('dev')); // для логирования запросов к сервису
app.use(cors()); // сервер сможет обрабатывать корс запросы

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 1000000}));

app.use(config.imagesAddress, express.static(path.join(config.imagesStore))); // keys.imageStore - это папка, в которой хранятся картинки, т.е. 'backend/uploads/'
// keys.imageAddress - это адрес в браузере, который будет отобажаться
// грубо говоря keys.imageStore будет замещен на keys.imageAddress
// keys.imageAddress должен быть указан в прокси client/proxy.conf.json

// Подключение бд.
mongoose.connect(config.mongoURI, {useUnifiedTopology: true, useNewUrlParser: true})
    .then(() => {
        console.log('MongoDB is connected.');
    })
    .catch(error => console.log(error));

//
// mongoose.connection.on('connected', () => {
//   console.log("Successful connection to the database")
// });
//
// mongoose.connection.on('error', (err) => {
//   console.log("Not successful connection to the database" + err)
// });

// require('./routes')(app);
app.use('/api/auth', authRoutes);
//app.use('/api/admin', adminRoutes);
app.use('/api/admin/posts/', adminPostRoutes);
app.use('/api/blog/posts/', blogPostRoutes);
app.use('/api/categories/', categoryRoutes);
app.use('/api/tags/', tagRoutes);
app.use('/api/blog/posts/comments/', commentRoutes);
app.use('/api/settings/title', titleRoutes);
app.use('/api/admin/link/generation', linkGenerationRoutes);
app.use('/api/additionalData', additionalDataRoutes);
app.use('/test', testRoutes);

if (process.env.NODE_ENV === 'production') {
    app.use(express.static('client/dist/client'));
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(
            __dirname, 'client', 'dist', 'client', 'index.html'
        ));
    });
}

module.exports = app;
