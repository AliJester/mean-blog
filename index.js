const app = require('./app');

const port = process.env.PORT || 5005;

app.listen(port, () => {
  console.log(`Server has been started on ${port}!`);
});

// Можно подключать прослушку нескольких портов
/*app.listen(5001, () => {
  console.log(`Server has been started on 5001!`);
});

app.listen(5002, () => {
  console.log(`Server has been started on 5002!`);
});*/
