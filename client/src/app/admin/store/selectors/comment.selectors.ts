import { createFeatureSelector, createSelector } from '@ngrx/store'
import { AppStateInterface } from '../../../shared/store/types/appState.interface';
import { AdminStateInterface } from '../types/adminState.interface';
import { ADMIN_STORE } from '../../../shared/utils/constants/storeConstants';

export const commentsFeatureSelector = createFeatureSelector<
  AppStateInterface,
  AdminStateInterface
  >(ADMIN_STORE)

export const isCommentsLoadingSelector = createSelector(
  commentsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.commentsState.isCommentsLoading
);

export const isCommentsLoadedSelector = createSelector(
  commentsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.commentsState.isCommentsLoaded
);

export const commentsErrorSelector = createSelector(
  commentsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.commentsState.commentsErrors
);

export const commentsMessageSelector = createSelector(
  commentsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.commentsState.message
);

export const commentsSelector = createSelector(
  commentsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.commentsState.comments
);

export const newCommentsSelector = createSelector(
  commentsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.commentsState.newComments
);
