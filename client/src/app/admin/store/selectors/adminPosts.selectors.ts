import {createFeatureSelector, createSelector} from '@ngrx/store'
import { AppStateInterface } from '../../../shared/store/types/appState.interface';
import { AdminStateInterface } from '../types/adminState.interface';
import { ADMIN_STORE } from '../../../shared/utils/constants/storeConstants';


export const adminPostsFeatureSelector = createFeatureSelector<
  AppStateInterface,
  AdminStateInterface
  >(ADMIN_STORE)

export const isLoadingSelector = createSelector(
  adminPostsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.adminPostsState.isPostsLoading
);

export const postErrorSelector = createSelector(
  adminPostsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.adminPostsState.postErrors
);

export const postMessageSelector = createSelector(
  adminPostsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.adminPostsState.message
);

export const adminPostsSelector = createSelector(
  adminPostsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.adminPostsState.posts
);

export const adminCurrentPostSelector = createSelector(
  adminPostsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.adminPostsState.currentPost
);

export const isPostsLoadedSelector = createSelector(
  adminPostsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.adminPostsState.isPostsLoaded
);

export const allPostsAmountSelector = createSelector(
  adminPostsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.adminPostsState.allPostsAmount
);
