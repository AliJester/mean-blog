import {createFeatureSelector, createSelector} from '@ngrx/store'
import { AppStateInterface } from '../../../shared/store/types/appState.interface';
import { AdminStateInterface } from '../types/adminState.interface';
import { ADMIN_STORE } from '../../../shared/utils/constants/storeConstants';


export const categoriesFeatureSelector = createFeatureSelector<
  AppStateInterface,
  AdminStateInterface
  >(ADMIN_STORE)

export const isLoadingSelector = createSelector(
  categoriesFeatureSelector,
  (adminState: AdminStateInterface) => adminState.categoriesState.isCategoriesLoading
);

export const categoriesErrorSelector = createSelector(
  categoriesFeatureSelector,
  (adminState: AdminStateInterface) => adminState.categoriesState.categoriesErrors
)

export const categoriesSelector = createSelector(
  categoriesFeatureSelector,
  (adminState: AdminStateInterface) => adminState.categoriesState.categories
);

export const isCategoriesLoadedSelector = createSelector(
  categoriesFeatureSelector,
  (adminState: AdminStateInterface) => adminState.categoriesState.isCategoriesLoaded
);



