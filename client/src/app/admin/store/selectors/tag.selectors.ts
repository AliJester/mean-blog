import {createFeatureSelector, createSelector} from '@ngrx/store'
import { AppStateInterface } from '../../../shared/store/types/appState.interface';
import { AdminStateInterface } from '../types/adminState.interface';
import { ADMIN_STORE } from '../../../shared/utils/constants/storeConstants';


export const tagsFeatureSelector = createFeatureSelector<
  AppStateInterface,
  AdminStateInterface
  >(ADMIN_STORE)

export const isLoadingSelector = createSelector(
  tagsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.tagState.isTagsLoading
);

export const tagsErrorSelector = createSelector(
  tagsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.tagState.tagsErrors
)

export const tagsSelector = createSelector(
  tagsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.tagState.tags
);

export const isTagsLoadedSelector = createSelector(
  tagsFeatureSelector,
  (adminState: AdminStateInterface) => adminState.tagState.isTagsLoaded
);



