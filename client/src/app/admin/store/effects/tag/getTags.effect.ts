import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'

import {
  getTagsAction,
  getTagsFailureAction,
  getTagsSuccessAction
} from '../../actions/tag.action';
import { TagService } from '../../../../shared/services/tag.service';
import { Tag } from '../../../../shared/models/Tag.model';

@Injectable()
export class GetTagsEffect {
  getAdminTags$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getTagsAction),
      switchMap(() => {
        return this.tagService.getAllTags().pipe(
          map((tags: Tag[]) => {
            return getTagsSuccessAction({tags})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(getTagsFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  constructor(private actions$: Actions, private tagService: TagService) {
  }
}
