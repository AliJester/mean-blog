import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'

import {
  getTagAction, getTagFailureAction, getTagSuccessAction
} from '../../actions/tag.action';
import { TagService } from '../../../../shared/services/tag.service';
import { Tag } from '../../../../shared/models/Tag.model';

@Injectable()
export class GetTagsEffect {
  getTags$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getTagAction),
      switchMap((tag) => {
        return this.catService.getTagById(tag._id).pipe(
          map((tag: Tag) => {
            return getTagSuccessAction({tag})
          }),
          catchError(() => {
            return of(getTagFailureAction)
          })
        )
      })
    )
  )

  constructor(private actions$: Actions, private catService: TagService) {
  }
}
