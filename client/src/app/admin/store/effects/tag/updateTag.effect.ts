import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import {
  updateTagAction,
  updateTagFailureAction,
  updateTagSuccessAction,
} from '../../actions/tag.action';
import { TagService } from '../../../../shared/services/tag.service';
import { ADMIN_TAG_LIST } from '../../../../shared/utils/constants/urlConstants';

@Injectable()
export class UpdateTagEffect {
  addTag$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateTagAction),
      switchMap(({tag}) => {
        return this.catService.updateTag(tag).pipe(
          map((updateTagResult) => {
            if (updateTagResult.success) {
              return updateTagSuccessAction({tag})
            } else {
              return updateTagFailureAction({message: updateTagResult.message});
            }
          }),
          catchError((error: HttpErrorResponse) => {
            return of(updateTagFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  redirectAfterUpdate$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updateTagSuccessAction),
        tap(() => {
          this.router.navigate([ADMIN_TAG_LIST])
        })
      ),
    {dispatch: false}
  )

  constructor(
    private actions$: Actions,
    private catService: TagService,
    private router: Router,
  ) {
  }
}
