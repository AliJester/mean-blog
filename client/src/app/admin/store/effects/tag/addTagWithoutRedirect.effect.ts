import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError, map, switchMap } from 'rxjs/operators';
import {
  addTagFailureAction, addTagWithoutRedirectAction, addTagWithoutRedirectSuccessAction,
} from '../../actions/tag.action';
import { TagService } from '../../../../shared/services/tag.service';
import { Tag } from '../../../../shared/models/Tag.model';
import { of } from 'rxjs';

@Injectable()
export class AddTagWithoutRedirectEffect {
  addTagWithoutRedirect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addTagWithoutRedirectAction),
      switchMap(({tagName}) => {
        const newTag = new Tag(tagName, null);
        return this.tagService.createTag(newTag).pipe(
          map((createTagResult) => {
            if (createTagResult.success) {
              newTag._id = createTagResult.newTagId
              return addTagWithoutRedirectSuccessAction({tag: newTag})
            } else {
              return addTagFailureAction({message: createTagResult.message});
            }
          }),
          catchError((error: HttpErrorResponse) => {
            return of(addTagFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  constructor(
    private actions$: Actions,
    private tagService: TagService,
  ) {
  }
}
