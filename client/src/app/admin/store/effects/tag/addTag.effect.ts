import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import {
  addTagAction, addTagFailureAction, addTagSuccessAction,
} from '../../actions/tag.action';
import { TagService } from '../../../../shared/services/tag.service';
import { Tag } from '../../../../shared/models/Tag.model';
import { of } from 'rxjs';
import { ADMIN_TAG_LIST } from '../../../../shared/utils/constants/urlConstants';

@Injectable()
export class AddTagEffect {
  addTag$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addTagAction),
      switchMap(({tagName}) => {
        const newTag = new Tag(tagName, null);
        return this.tagService.createTag(newTag).pipe(
          map((createTagResult) => {
            if (createTagResult.success) {
              newTag._id = createTagResult.newTagId
              return addTagSuccessAction({tag: newTag})
            } else {
              return addTagFailureAction({message: createTagResult.message});
            }
          }),
          catchError((error: HttpErrorResponse) => {
            return of(addTagFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  redirectAfterAdding$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(addTagSuccessAction),
        tap(() => {
          this.router.navigate([ADMIN_TAG_LIST])
        })
      ),
    {dispatch: false}
  )

  constructor(
    private actions$: Actions,
    private tagService: TagService,
    private router: Router,
  ) {
  }
}
