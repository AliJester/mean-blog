import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  deleteTagAction, deleteTagFailureAction, deleteTagSuccessAction,
} from '../../actions/tag.action';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { TagService } from '../../../../shared/services/tag.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { ADMIN_TAG_LIST } from '../../../../shared/utils/constants/urlConstants';

@Injectable()
export class DeleteTagEffect {
  deleteTag$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteTagAction),
      switchMap((tag) => {
        return this.catService.deleteTag(tag._id).pipe(
          map(() => {
            return deleteTagSuccessAction({_id: tag._id})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(deleteTagFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  redirectAfterDelete$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(deleteTagSuccessAction),
        tap(() => {
          this.router.navigate([ADMIN_TAG_LIST])
        })
      ),
    {dispatch: false}
  )

  constructor(
    private actions$: Actions,
    private catService: TagService,
    private router: Router,
  ) {
  }
}
