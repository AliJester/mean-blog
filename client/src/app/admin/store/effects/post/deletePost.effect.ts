import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import {
  deletePostAction,
  deletePostFailureAction,
  deletePostSuccessAction,
} from '../../actions/adminPost.action';
import { AdminPostService } from '../../../shared/services/adminPost.service';
import { ADMIN_POST_LIST } from '../../../../shared/utils/constants/urlConstants';

@Injectable()
export class DeletePostEffect {
  deletePost$ = createEffect(() => {
      return this.actions$.pipe(
        ofType(deletePostAction),
        switchMap((post) => {
          return this.postService.deletePost(post._id).pipe(
            map((success) => {
              return deletePostSuccessAction({_id: post._id, message: success.message})
            }),
            catchError((error: HttpErrorResponse) => {
              return of(deletePostFailureAction({message: error.message}))
            }),
          )
        })
      );
    }
  )

  redirectAfterDelete$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(deletePostSuccessAction),
        tap(() => {
          this.router.navigate([ADMIN_POST_LIST])
        })
      ),
    {dispatch: false}
  )

  constructor(
    private actions$: Actions,
    private postService: AdminPostService,
    private router: Router,
  ) {
  }
}
