import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import {
  addPostAction,
  addPostFailureAction,
  addPostSuccessAction,
} from '../../actions/adminPost.action';
import { AdminPostService } from '../../../shared/services/adminPost.service';
import { ADMIN_POST_LIST } from '../../../../shared/utils/constants/urlConstants';

@Injectable()
export class AddPostEffect {
  addPost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addPostAction),
      switchMap(({post}) => {
        const newPost = {...post}
        return this.postService.create(post).pipe(
          map((createPostResult) => {
            if (createPostResult.success) {
              newPost._id = createPostResult.newPostId
              return addPostSuccessAction({post: post, message: createPostResult.message})
            } else {
              return addPostFailureAction({message: createPostResult.message});
            }
          }),
          catchError((error: HttpErrorResponse) => {
            return of(addPostFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  redirectAfterAdding$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(addPostSuccessAction),
        tap(() => {
          this.router.navigate([ADMIN_POST_LIST])
        })
      ),
    {dispatch: false}
  )

  constructor(
    private actions$: Actions,
    private postService: AdminPostService,
    private router: Router,
  ) {
  }
}
