import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'

import { Post } from '../../../../shared/models/Post.model';
import {
  getPostsAction,
  getPostsFailureAction,
  getPostsSuccessAction
} from '../../actions/adminPost.action';
import { AdminPostService } from '../../../shared/services/adminPost.service';

@Injectable()
export class GetAdminPostsEffect {
  getPosts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getPostsAction),
      switchMap(({params}) => {
        return this.postService.getAllPostsForAdmin(params).pipe(
          map((posts: Post[]) => {
            return getPostsSuccessAction({posts})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(getPostsFailureAction({message: error.message}))
          }),
        )
      })
    )
  );

  constructor(private actions$: Actions, private postService: AdminPostService) {
  }
}
