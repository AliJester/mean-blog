import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'

import { Post } from '../../../../shared/models/Post.model';
import {
  getPostAction,
  getPostFailureAction,
  getPostSuccessAction
} from '../../actions/adminPost.action';
import { AdminPostService } from '../../../shared/services/adminPost.service';

@Injectable()
export class GetAdminPostEffect {
  getPost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getPostAction),
      switchMap((post) => {
        return this.postService.getById(post.coolId).pipe(
          map((post: Post) => {
            return getPostSuccessAction({currentPost: post})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(getPostFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  constructor(private actions$: Actions, private postService: AdminPostService) {
  }
}
