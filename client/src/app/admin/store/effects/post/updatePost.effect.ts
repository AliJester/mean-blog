import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import {
  updatePostAction,
  updatePostFailureAction,
  updatePostSuccessAction,
} from '../../actions/adminPost.action';
import { AdminPostService } from '../../../shared/services/adminPost.service';
import { ADMIN_POST_LIST } from '../../../../shared/utils/constants/urlConstants';

@Injectable()
export class UpdatePostEffect {
  updatePost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updatePostAction),
      switchMap(({post}) => {
        return this.postService.update(post).pipe(
          map((updatePostResult) => {
            if (updatePostResult.success) {
              return updatePostSuccessAction({post, message: updatePostResult.message})
            } else {
              return updatePostFailureAction({message: updatePostResult.message});
            }
          }),
          catchError((error: HttpErrorResponse) => {
            return of(updatePostFailureAction({message: error.message}))
          }),
        )
      })
    )
  );

  redirectAfterUpdate$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updatePostSuccessAction),
        tap(() => {
          this.router.navigate([ADMIN_POST_LIST])
        })
      ),
    {dispatch: false}
  )

  constructor(
    private actions$: Actions,
    private postService: AdminPostService,
    private router: Router,
  ) {
  }
}
