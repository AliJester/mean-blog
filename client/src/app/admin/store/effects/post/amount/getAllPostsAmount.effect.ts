import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'

import {
  getAllPostsAmountAction,
  getAllPostsAmountFailureAction, getAllPostsAmountSuccessAction,
} from '../../../actions/adminPost.action';
import { AdminPostService } from '../../../../shared/services/adminPost.service';

@Injectable()
export class GetAllPostsAmountEffect {
  getAllPostsAmount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getAllPostsAmountAction),
      switchMap(() => {
        return this.postService.getAllPostsAmount().pipe(
          map((allPostsAmount) => {
            return getAllPostsAmountSuccessAction({allPostsAmount})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(getAllPostsAmountFailureAction({message: error.message}))
          }),
        )
      })
    )
  );

  constructor(private actions$: Actions, private postService: AdminPostService) {
  }
}
