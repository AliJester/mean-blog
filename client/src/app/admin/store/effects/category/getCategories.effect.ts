import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'
import {
  getCategoriesAction,
  getCategoriesFailureAction,
  getCategoriesSuccessAction
} from '../../actions/category.action';
import { CategoryService } from '../../../../shared/services/category.service';
import { Category } from '../../../../shared/models/Category.model';

@Injectable()
export class GetCategoriesEffect {
  getCategories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCategoriesAction),
      switchMap(() => {
        return this.catService.getAllCategories().pipe(
          map((categories: Category[]) => {
            return getCategoriesSuccessAction({categories})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(getCategoriesFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  constructor(private actions$: Actions, private catService: CategoryService) {
  }
}
