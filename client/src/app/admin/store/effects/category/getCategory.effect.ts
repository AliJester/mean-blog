import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'
import {
  getCategoryAction, getCategoryFailureAction, getCategorySuccessAction
} from '../../actions/category.action';
import { CategoryService } from '../../../../shared/services/category.service';
import { Category } from '../../../../shared/models/Category.model';

@Injectable()
export class GetCategoriesEffect {
  getCategories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCategoryAction),
      switchMap((cat) => {
        return this.catService.getCategoryById(cat._id).pipe(
          map((category: Category) => {
            return getCategorySuccessAction({category})
          }),
          catchError(() => {
            return of(getCategoryFailureAction)
          })
        )
      })
    )
  )

  constructor(private actions$: Actions, private catService: CategoryService) {
  }
}
