import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import {
  addCategoryAction, addCategoryFailureAction, addCategorySuccessAction, addCategorySuccessWithoutRedirectAction,
} from '../../actions/category.action';
import { CategoryService } from '../../../../shared/services/category.service';
import { Category } from '../../../../shared/models/Category.model';
import { ADMIN_CATEGORY_LIST } from '../../../../shared/utils/constants/urlConstants';

@Injectable()
export class AddCategoryEffect {
  addCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addCategoryAction),
      switchMap(({catName, redirect}) => {
        const newCategory = new Category(catName, null);
        return this.catService.createCategory(newCategory).pipe(
          map((createCategoryResult) => {
            if (createCategoryResult.success) {
              newCategory._id = createCategoryResult.newCategoryId;
              if (redirect) {
                return addCategorySuccessAction({cat: newCategory});
              } else {
                return addCategorySuccessWithoutRedirectAction({cat: newCategory});
              }
            } else {
              return addCategoryFailureAction({message: createCategoryResult.message});
            }
          }),
          catchError((error: HttpErrorResponse) => {
            return of(addCategoryFailureAction({message: error.message}));
          }),
        )
      })
    )
  )

  redirectAfterAdding$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(addCategorySuccessAction),
        tap(() => {
          this.router.navigate([ADMIN_CATEGORY_LIST]);
        })
      ),
    {dispatch: false}
  )

  constructor(
    private actions$: Actions,
    private catService: CategoryService,
    private router: Router,
  ) {
  }
}
