import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { CategoryService } from '../../../../shared/services/category.service';
import {
  deleteCategoryAction, deleteCategoryFailureAction, deleteCategorySuccessAction,
} from '../../actions/category.action';
import { ADMIN_CATEGORY_LIST } from '../../../../shared/utils/constants/urlConstants';

@Injectable()
export class DeleteCategoryEffect {
  deleteCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteCategoryAction),
      switchMap((cat) => {
        return this.catService.deleteCategory(cat._id).pipe(
          map(() => {
            return deleteCategorySuccessAction({_id: cat._id})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(deleteCategoryFailureAction({message: error.error.message}))
          }),
        )
      })
    )
  )

  redirectAfterDelete$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(deleteCategorySuccessAction),
        tap(() => {
          this.router.navigate([ADMIN_CATEGORY_LIST])
        })
      ),
    {dispatch: false}
  )

  constructor(
    private actions$: Actions,
    private catService: CategoryService,
    private router: Router,
  ) {
  }
}
