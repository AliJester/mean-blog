import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import {
  updateCategoryAction,
  updateCategoryFailureAction,
  updateCategorySuccessAction,
} from '../../actions/category.action';
import { CategoryService } from '../../../../shared/services/category.service';
import { ADMIN_CATEGORY_LIST } from '../../../../shared/utils/constants/urlConstants';

@Injectable()
export class UpdateCategoryEffect {
  addCategory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateCategoryAction),
      switchMap(({cat}) => {
        return this.catService.updateCategory(cat).pipe(
          map((updateCategoryResult) => {
            if (updateCategoryResult.success) {
              return updateCategorySuccessAction({cat})
            } else {
              return updateCategoryFailureAction({message: updateCategoryResult.message});
            }
          }),
          catchError((error: HttpErrorResponse) => {
            return of(updateCategoryFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  redirectAfterUpdate$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updateCategorySuccessAction),
        tap(() => {
          this.router.navigate([ADMIN_CATEGORY_LIST])
        })
      ),
    {dispatch: false}
  )

  constructor(
    private actions$: Actions,
    private catService: CategoryService,
    private router: Router,
  ) {
  }
}
