import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'

import { PostComment } from '../../../../shared/models/PostComment.model';
import {
  getNewCommentsAction,
  getNewCommentsFailureAction,
  getNewCommentsSuccessAction
} from '../../actions/comment.action';
import { CommentService } from '../../../../shared/services/comment.service';

@Injectable()
export class GetNewCommentsEffect {
  getNewComments$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getNewCommentsAction),
      switchMap(() => {
        return this.commentService.getNewComments().pipe(
          map((newComments: PostComment[]) => {
            return getNewCommentsSuccessAction({newComments})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(getNewCommentsFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  constructor(private actions$: Actions, private commentService: CommentService) {
  }
}
