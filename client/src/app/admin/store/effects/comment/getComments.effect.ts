import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'

import { PostComment } from '../../../../shared/models/PostComment.model';
import { getCommentsAction, getCommentsFailureAction, getCommentsSuccessAction } from '../../actions/comment.action';
import { CommentService } from '../../../../shared/services/comment.service';

@Injectable()
export class GetCommentsEffect {
  getComments$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCommentsAction),
      switchMap(() => {
        return this.commentService.getAllComments().pipe(
          map((comments: PostComment[]) => {
            return getCommentsSuccessAction({comments})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(getCommentsFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  constructor(private actions$: Actions, private commentService: CommentService) {
  }
}
