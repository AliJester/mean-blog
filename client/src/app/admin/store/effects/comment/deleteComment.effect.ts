import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';

import { ADMIN_COMMENT_LIST } from '../../../../shared/utils/constants/urlConstants';
import {
  deleteCommentAction,
  deleteCommentFailureAction,
  deleteCommentSuccessAction
} from '../../actions/comment.action';
import { CommentService } from '../../../../shared/services/comment.service';

@Injectable()
export class DeleteCommentEffect {
  deleteComment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(deleteCommentAction),
      switchMap((props) => {
        return this.commentService.deleteComment(props._id, props.parentId).pipe(
          map((success) => {
            return deleteCommentSuccessAction({_id: props._id, parentId: props.parentId, message: success.message})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(deleteCommentFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  redirectAfterDelete$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(deleteCommentSuccessAction),
        tap(() => {
          this.router.navigate([ADMIN_COMMENT_LIST])
        })
      ),
    {dispatch: false}
  )

  constructor(
    private actions$: Actions,
    private commentService: CommentService,
    private router: Router,
  ) {
  }
}
