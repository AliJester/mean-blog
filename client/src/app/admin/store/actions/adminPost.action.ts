import { createAction, props } from '@ngrx/store';
import { Post } from '../../../shared/models/Post.model';
import { PostTypes } from './types/postTypes';
import { QueryParams } from '../../../shared/models/QueryParams.model';

export const getPostAction = createAction(
  PostTypes.GET_POST,
  props<{coolId: string}>()
);
export const getPostSuccessAction = createAction(
  PostTypes.GET_POST_SUCCESS,
  props<{currentPost: Post}>()
);
export const getPostFailureAction = createAction(
  PostTypes.GET_POST_FAILURE,
  props<{message: string}>()
);

export const getPostsAction = createAction(
  PostTypes.GET_POSTS,
  props<{params: QueryParams}>()
);
export const getPostsSuccessAction = createAction(
  PostTypes.GET_POSTS_SUCCESS,
  props<{posts: Post[]}>()
);
export const getPostsFailureAction = createAction(
  PostTypes.GET_POSTS_FAILURE,
  props<{message: string}>()
);

export const getAllPostsAmountAction = createAction(
  PostTypes.GET_ALL_POSTS_AMOUNT,
);
export const getAllPostsAmountSuccessAction = createAction(
  PostTypes.GET_ALL_POSTS_AMOUNT_SUCCESS,
  props<{allPostsAmount: number}>()
);
export const getAllPostsAmountFailureAction = createAction(
  PostTypes.GET_ALL_POSTS_AMOUNT_FAILURE,
  props<{message: string}>()
);

export const addPostAction = createAction(
  PostTypes.ADD_POST,
  props<{post: Post}>()
);
export const addPostSuccessAction = createAction(
  PostTypes.ADD_POST_SUCCESS,
  props<{post: Post, message: string}>()
);
export const addPostFailureAction = createAction(
  PostTypes.ADD_POST_FAILURE,
  props<{message: string}>()
);

export const deletePostAction = createAction(
  PostTypes.DELETE_POST,
  props<{_id: string}>()
);
export const deletePostSuccessAction = createAction(
  PostTypes.DELETE_POST_SUCCESS,
  props<{_id: string, message: string}>()
);
export const deletePostFailureAction = createAction(
  PostTypes.DELETE_POST_FAILURE,
  props<{message: string}>()
);

export const updatePostAction = createAction(
  PostTypes.UPDATE_POST,
  props<{post: Post}>()
);
export const updatePostSuccessAction = createAction(
  PostTypes.UPDATE_POST_SUCCESS,
  props<{post: Post, message: string}>()
);
export const updatePostFailureAction = createAction(
  PostTypes.UPDATE_POST_FAILURE,
  props<{message: string}>()
);

export const clearPostErrorsAction = createAction(
  PostTypes.CLEAR_POST_ERRORS
);

export const clearPostMessageAction = createAction(
  PostTypes.CLEAR_POST_MESSAGE
);
