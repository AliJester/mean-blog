export enum PostTypes {
  GET_POST = '[ADMIN] Get post',
  GET_POST_SUCCESS = '[ADMIN] Get post success',
  GET_POST_FAILURE = '[ADMIN] Get post failure',

  GET_POSTS = '[ADMIN] Get list of posts',
  GET_POSTS_SUCCESS = '[ADMIN] Get list of posts success',
  GET_POSTS_FAILURE = '[ADMIN] Get list of posts failure',

  DELETE_POST = '[ADMIN] Delete post',
  DELETE_POST_SUCCESS = '[ADMIN] Delete post success',
  DELETE_POST_FAILURE = '[ADMIN] Delete post failure',

  ADD_POST = '[ADMIN] Add post',
  ADD_POST_SUCCESS = '[ADMIN] Add post success',
  ADD_POST_FAILURE = '[ADMIN] Add post failure',

  UPDATE_POST = '[ADMIN] Update post',
  UPDATE_POST_SUCCESS = '[ADMIN] Update post success',
  UPDATE_POST_FAILURE = '[ADMIN] Update post failure',

  GET_ALL_POSTS_AMOUNT = '[ADMIN] Get all amount of posts',
  GET_ALL_POSTS_AMOUNT_SUCCESS = '[ADMIN] Get all amount of posts success',
  GET_ALL_POSTS_AMOUNT_FAILURE = '[ADMIN] Get all amount of posts failure',

  CLEAR_POST_ERRORS = '[ADMIN] Clear post errors',
  CLEAR_POST_MESSAGE = '[ADMIN] Clear post message',
}
