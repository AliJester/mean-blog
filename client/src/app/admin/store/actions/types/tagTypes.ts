export enum TagTypes {
  GET_TAGS = '[ADMIN] Get list of tags',
  GET_TAGS_SUCCESS = '[ADMIN] Get list of tags success',
  GET_TAGS_FAILURE = '[ADMIN] Get list of tags failure',

  GET_TAG = '[ADMIN] Get tag',
  GET_TAG_SUCCESS = '[ADMIN] Get tag success',
  GET_TAG_FAILURE = '[ADMIN] Get tag failure',

  DELETE_TAG = '[ADMIN] Delete tag',
  DELETE_TAG_SUCCESS = '[ADMIN] Delete tag success',
  DELETE_TAG_FAILURE = '[ADMIN] Delete tag failure',

  ADD_TAG = '[ADMIN] Add tag',
  ADD_TAG_SUCCESS = '[ADMIN] Add tag success',
  ADD_TAG_FAILURE = '[ADMIN] Add tag failure',

  ADD_TAG_NO_REDIRECT = '[ADMIN] Add tag no redirect',
  ADD_TAG_NO_REDIRECT_SUCCESS = '[ADMIN] Add tag no redirect success',
  ADD_TAG_NO_REDIRECT_FAILURE = '[ADMIN] Add tag no redirect failure',

  UPDATE_TAG = '[ADMIN] Update tag',
  UPDATE_TAG_SUCCESS = '[ADMIN] Update tag success',
  UPDATE_TAG_FAILURE = '[ADMIN] Update tag failure',

  CLEAR_TAGS_ERRORS = '[ADMIN] Clear tags errors',

}
