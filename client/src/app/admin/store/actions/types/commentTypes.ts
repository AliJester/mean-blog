export enum CommentTypes {
  GET_COMMENTS = '[ADMIN] Get list of comments',
  GET_COMMENTS_SUCCESS = '[ADMIN] Get list of comments success',
  GET_COMMENTS_FAILURE = '[ADMIN] Get list of comments failure',

  GET_NEW_COMMENTS = '[ADMIN] Get list of new comments',
  GET_NEW_COMMENTS_SUCCESS = '[ADMIN] Get list of new comments success',
  GET_NEW_COMMENTS_FAILURE = '[ADMIN] Get list of new comments failure',

  DELETE_COMMENT = '[ADMIN] Delete comment',
  DELETE_COMMENT_SUCCESS = '[ADMIN] Delete comment success',
  DELETE_COMMENT_FAILURE = '[ADMIN] Delete comment failure',

  CLEAR_COMMENTS_ERRORS = '[ADMIN] Clear comments errors',
  CLEAR_COMMENTS_MESSAGE = '[ADMIN] Clear comments message',

}
