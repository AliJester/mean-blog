export enum CategoryTypes {
  GET_CATEGORIES = '[ADMIN] Get list of categories',
  GET_CATEGORIES_SUCCESS = '[ADMIN] Get list of categories success',
  GET_CATEGORIES_FAILURE = '[ADMIN] Get list of categories failure',

  GET_CATEGORY = '[ADMIN] Get category',
  GET_CATEGORY_SUCCESS = '[ADMIN] Get category success',
  GET_CATEGORY_FAILURE = '[ADMIN] Get category failure',

  DELETE_CATEGORY = '[ADMIN] Delete category',
  DELETE_CATEGORY_SUCCESS = '[ADMIN] Delete category success',
  DELETE_CATEGORY_FAILURE = '[ADMIN] Delete category failure',

  ADD_CATEGORY = '[ADMIN] Add category',
  ADD_CATEGORY_SUCCESS = '[ADMIN] Add category success',
  ADD_CATEGORY_WITHOUT_REDIRECT_SUCCESS = '[ADMIN] Add category success no redirect',
  ADD_CATEGORY_FAILURE = '[ADMIN] Add category failure',

  UPDATE_CATEGORY = '[ADMIN] Update category',
  UPDATE_CATEGORY_SUCCESS = '[ADMIN] Update category success',
  UPDATE_CATEGORY_FAILURE = '[ADMIN] Update category failure',

  CLEAR_CATEGORIES_ERRORS = '[ADMIN] Clear categories errors',

}
