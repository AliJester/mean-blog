import { createAction, props } from '@ngrx/store';
import { Category } from '../../../shared/models/Category.model';
import { CategoryTypes } from './types/categoryTypes';

export const getCategoriesAction = createAction(
  CategoryTypes.GET_CATEGORIES
);
export const getCategoriesSuccessAction = createAction(
  CategoryTypes.GET_CATEGORIES_SUCCESS,
  props<{categories: Category[]}>()
);
export const getCategoriesFailureAction = createAction(
  CategoryTypes.GET_CATEGORIES_FAILURE,
  props<{message: string}>()
);

export const getCategoryAction = createAction(
  CategoryTypes.GET_CATEGORY,
  props<{_id: string}>()
);
export const getCategorySuccessAction = createAction(
  CategoryTypes.GET_CATEGORY_SUCCESS,
  props<{category: Category}>()
);
export const getCategoryFailureAction = createAction(
  CategoryTypes.GET_CATEGORY_FAILURE,
  props<{message: string}>()
);

export const deleteCategoryAction = createAction(
  CategoryTypes.DELETE_CATEGORY,
  props<{_id: string}>()
);
export const deleteCategorySuccessAction = createAction(
  CategoryTypes.DELETE_CATEGORY_SUCCESS,
  props<{_id: string}>()
);
export const deleteCategoryFailureAction = createAction(
  CategoryTypes.DELETE_CATEGORY_FAILURE,
  props<{message: string}>()
);

export const addCategoryAction = createAction(
  CategoryTypes.ADD_CATEGORY,
  props<{catName: string, redirect: boolean}>()
);
export const addCategorySuccessAction = createAction(
  CategoryTypes.ADD_CATEGORY_SUCCESS,
  props<{cat: Category}>()
);
export const addCategorySuccessWithoutRedirectAction = createAction(
  CategoryTypes.ADD_CATEGORY_WITHOUT_REDIRECT_SUCCESS,
  props<{cat: Category}>()
);
export const addCategoryFailureAction = createAction(
  CategoryTypes.ADD_CATEGORY_FAILURE,
  props<{message: string}>()
);

export const updateCategoryAction = createAction(
  CategoryTypes.UPDATE_CATEGORY,
  props<{cat: Category}>()
);
export const updateCategorySuccessAction = createAction(
  CategoryTypes.UPDATE_CATEGORY_SUCCESS,
  props<{cat: Category}>()
);
export const updateCategoryFailureAction = createAction(
  CategoryTypes.UPDATE_CATEGORY_FAILURE,
  props<{message: string}>()
);

export const clearCategoryErrorsAction = createAction(
  CategoryTypes.CLEAR_CATEGORIES_ERRORS,
);
