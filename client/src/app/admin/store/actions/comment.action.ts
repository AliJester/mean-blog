import { createAction, props } from '@ngrx/store';
import { PostComment } from '../../../shared/models/PostComment.model';
import { CommentTypes } from './types/commentTypes';

export const getCommentsAction = createAction(
  CommentTypes.GET_COMMENTS,
);
export const getCommentsSuccessAction = createAction(
  CommentTypes.GET_COMMENTS_SUCCESS,
  props<{comments: PostComment[]}>()
);
export const getCommentsFailureAction = createAction(
  CommentTypes.GET_COMMENTS_FAILURE,
  props<{message: string}>()
);

export const getNewCommentsAction = createAction(
  CommentTypes.GET_NEW_COMMENTS
);
export const getNewCommentsSuccessAction = createAction(
  CommentTypes.GET_NEW_COMMENTS_SUCCESS,
  props<{newComments: PostComment[]}>()
);
export const getNewCommentsFailureAction = createAction(
  CommentTypes.GET_NEW_COMMENTS_FAILURE,
  props<{message: string}>()
);

export const deleteCommentAction = createAction(
  CommentTypes.DELETE_COMMENT,
  props<{_id: string, parentId: string}>()
);
export const deleteCommentSuccessAction = createAction(
  CommentTypes.DELETE_COMMENT_SUCCESS,
  props<{_id: string, parentId: string, message: string}>()
);
export const deleteCommentFailureAction = createAction(
  CommentTypes.DELETE_COMMENT_FAILURE,
  props<{message: string}>()
);

export const clearCommentErrorsAction = createAction(
  CommentTypes.CLEAR_COMMENTS_ERRORS,
);
export const clearCommentsMessageAction = createAction(
  CommentTypes.CLEAR_COMMENTS_MESSAGE
);
