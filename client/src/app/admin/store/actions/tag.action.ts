import { createAction, props } from '@ngrx/store';
import { Tag } from '../../../shared/models/Tag.model';
import { TagTypes } from './types/tagTypes';

export const getTagsAction = createAction(
  TagTypes.GET_TAGS
);
export const getTagsSuccessAction = createAction(
  TagTypes.GET_TAGS_SUCCESS,
  props<{tags: Tag[]}>()
);
export const getTagsFailureAction = createAction(
  TagTypes.GET_TAGS_FAILURE,
  props<{message: string}>()
);

export const getTagAction = createAction(
  TagTypes.GET_TAG,
  props<{_id: string}>()
);
export const getTagSuccessAction = createAction(
  TagTypes.GET_TAG_SUCCESS,
  props<{tag: Tag}>()
);
export const getTagFailureAction = createAction(
  TagTypes.GET_TAG_FAILURE,
  props<{message: string}>()
);

export const deleteTagAction = createAction(
  TagTypes.DELETE_TAG,
  props<{_id: string}>()
);
export const deleteTagSuccessAction = createAction(
  TagTypes.DELETE_TAG_SUCCESS,
  props<{_id: string}>()
);
export const deleteTagFailureAction = createAction(
  TagTypes.DELETE_TAG_FAILURE,
  props<{message: string}>()
);

export const addTagAction = createAction(
  TagTypes.ADD_TAG,
  props<{tagName: string}>()
);
export const addTagWithoutRedirectAction = createAction(
  TagTypes.ADD_TAG_NO_REDIRECT,
  props<{tagName: string}>()
);
export const addTagSuccessAction = createAction(
  TagTypes.ADD_TAG_SUCCESS,
  props<{tag: Tag}>()
);
export const addTagWithoutRedirectSuccessAction = createAction(
  TagTypes.ADD_TAG_NO_REDIRECT_SUCCESS,
  props<{tag: Tag}>()
);
export const addTagFailureAction = createAction(
  TagTypes.ADD_TAG_FAILURE,
  props<{message: string}>()
);

export const updateTagAction = createAction(
  TagTypes.UPDATE_TAG,
  props<{tag: Tag}>()
);
export const updateTagSuccessAction = createAction(
  TagTypes.UPDATE_TAG_SUCCESS,
  props<{tag: Tag}>()
);
export const updateTagFailureAction = createAction(
  TagTypes.UPDATE_TAG_FAILURE,
  props<{message: string}>()
);

export const clearTagErrorsAction = createAction(
  TagTypes.CLEAR_TAGS_ERRORS,
);
