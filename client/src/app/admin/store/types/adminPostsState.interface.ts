import { Post } from '../../../shared/models/Post.model';

export interface AdminPostsStateInterface {
  isPostsLoading: boolean
  posts: Post[] | null
  postErrors: string | null
  isPostsLoaded: boolean
  allPostsAmount: number
  message: string
  currentPost: Post | null
}
