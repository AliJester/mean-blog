import { Category } from '../../../shared/models/Category.model';

export interface CategoriesStateInterface {
  isCategoriesLoading: boolean
  categories: Category[] | null
  categoriesErrors: string | null
  isCategoriesLoaded: boolean
}
