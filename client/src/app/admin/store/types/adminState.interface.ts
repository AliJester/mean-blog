import { AdminPostsStateInterface } from './adminPostsState.interface';
import { CategoriesStateInterface } from './categoriesState.interface';
import { TagsStateInterface } from './tagState.interface';
import { CommentsStateInterface } from './commentState.interface';

export interface AdminStateInterface {
  adminPostsState: AdminPostsStateInterface
  categoriesState: CategoriesStateInterface
  tagState: TagsStateInterface
  commentsState: CommentsStateInterface
  //error: string | null
}
