import { PostComment } from '../../../shared/models/PostComment.model';

export interface CommentsStateInterface {
  isCommentsLoading: boolean
  isCommentsLoaded: boolean
  comments: PostComment[] | null
  newComments: PostComment[] | null
  commentsErrors: string | null
  message: string
}
