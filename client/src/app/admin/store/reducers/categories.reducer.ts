import { Action, createReducer, on } from '@ngrx/store';
import { CategoriesStateInterface } from '../types/categoriesState.interface';
import {
  addCategoryFailureAction,
  addCategorySuccessAction, addCategorySuccessWithoutRedirectAction,
  clearCategoryErrorsAction, deleteCategoryFailureAction,
  deleteCategorySuccessAction,
  getCategoriesAction,
  getCategoriesSuccessAction,
  updateCategorySuccessAction,
} from '../actions/category.action';


const initialState: CategoriesStateInterface = {
  isCategoriesLoading: false,
  categories: [],
  categoriesErrors: null,
  isCategoriesLoaded: false,
}

const categoriesReducer = createReducer(
  initialState,
  on(
    getCategoriesAction,
    (state): CategoriesStateInterface => {
      return {
        ...state,
        isCategoriesLoading: true,
        isCategoriesLoaded: false,
      }
    }
  ),
  on(
    getCategoriesSuccessAction,
    (state, action): CategoriesStateInterface => ({
      ...state,
      isCategoriesLoading: false,
      isCategoriesLoaded: true,
      categories: action.categories,
    })
  ),
  on(
    deleteCategorySuccessAction,
    (state, action): CategoriesStateInterface => ({
      ...state,
      isCategoriesLoading: false,
      categories: state.categories.filter((category) => {
        return category._id !== action._id;
      })
    })
  ),
  on(
    deleteCategoryFailureAction,
    (state, action): CategoriesStateInterface => ({
      ...state,
      categoriesErrors: action.message
    })
  ),
  on(
    addCategorySuccessAction,
    (state, action): CategoriesStateInterface => {
      return {
        ...state,
        categories: [...state.categories, action.cat]
      }
    }
  ),
  on(
    addCategorySuccessWithoutRedirectAction,
    (state, action): CategoriesStateInterface => {
      return {
        ...state,
        categories: [...state.categories, action.cat]
      }
    }
  ),
  on(
    addCategoryFailureAction,
    (state, action): CategoriesStateInterface => {
      return {
        ...state,
        categoriesErrors: action.message
      }
    }
  ),
  on(
    updateCategorySuccessAction,
    (state, action): CategoriesStateInterface => {
      const catIndex = state.categories.findIndex( cat => cat._id === action.cat._id );
      const updatedCategories = [...state.categories];
      updatedCategories[catIndex] = action.cat;
      return {
        ...state,
        categories: updatedCategories
      }
    }
  ),
  on(
    clearCategoryErrorsAction,
    (state, action): CategoriesStateInterface => ({
      ...state,
      categoriesErrors: null
    })
  ),
)

export function categoriesReducers(state: CategoriesStateInterface, action: Action) {
  return categoriesReducer(state, action)
}
