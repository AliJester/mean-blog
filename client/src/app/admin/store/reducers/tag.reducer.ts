import { Action, createReducer, on } from '@ngrx/store';
import { TagsStateInterface } from '../types/tagState.interface';
import {
  addTagFailureAction,
  addTagSuccessAction, addTagWithoutRedirectSuccessAction,
  clearTagErrorsAction,
  deleteTagSuccessAction,
  getTagsAction,
  getTagsSuccessAction,
  updateTagSuccessAction,
} from '../actions/tag.action';


const initialState: TagsStateInterface = {
  isTagsLoading: false,
  tags: [],
  tagsErrors: null,
  isTagsLoaded: false,
}

const tagsReducer = createReducer(
  initialState,
  on(
    getTagsAction,
    (state): TagsStateInterface => {
      return {
        ...state,
        isTagsLoading: true,
        isTagsLoaded: false,
      }
    }
  ),
  on(
    getTagsSuccessAction,
    (state, action): TagsStateInterface => ({
      ...state,
      isTagsLoading: false,
      isTagsLoaded: true,
      tags: action.tags,
    })
  ),
  on(
    deleteTagSuccessAction,
    (state, action): TagsStateInterface => ({
      ...state,
      isTagsLoading: false,
      tags: state.tags.filter((tag) => {
        return tag._id !== action._id;
      })
    })
  ),
  on(
    addTagSuccessAction,
    (state, action): TagsStateInterface => {
      if (state.tags.length > 0) {
        const newTagsArray = [...state.tags];
        let index = newTagsArray.findIndex(tag => tag._id === action.tag._id);
        if (index > -1) {
          newTagsArray[index] = action.tag;
        } else {
          newTagsArray.push(action.tag);
        }
        return {
          ...state,
          tags: newTagsArray
        }
      }
      return {
        ...state,
        tags: [action.tag]
      }
    }
  ),
  on(
    addTagWithoutRedirectSuccessAction,
    (state, action): TagsStateInterface => {
      if (state.tags.length > 0) {
        const newTagsArray = [...state.tags];
        let index = newTagsArray.findIndex(tag => tag._id === action.tag._id);
        if (index > -1) {
          newTagsArray[index] = action.tag;
        } else {
          newTagsArray.push(action.tag);
        }
        return {
          ...state,
          tags: newTagsArray
        }
      }
      return {
        ...state,
        tags: [action.tag]
      }
    }
  ),
  on(
    addTagFailureAction,
    (state, action): TagsStateInterface => {
      return {
        ...state,
        tagsErrors: action.message
      }
    }
  ),
  on(
    updateTagSuccessAction,
    (state, action): TagsStateInterface => {
      const tagIndex = state.tags.findIndex(tag => tag._id === action.tag._id);
      const updatedTags = [...state.tags];
      updatedTags[tagIndex] = action.tag;
      return {
        ...state,
        tags: updatedTags
      }
    }
  ),
  on(
    clearTagErrorsAction,
    (state, action): TagsStateInterface => ({
      ...state,
      tagsErrors: null
    })
  ),
)

export function tagsReducers(state: TagsStateInterface, action: Action) {
  return tagsReducer(state, action)
}
