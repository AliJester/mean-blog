import { Action, createReducer, on } from '@ngrx/store';
import { CommentsStateInterface } from '../types/commentState.interface';
import {
  clearCommentErrorsAction,
  clearCommentsMessageAction,
  deleteCommentSuccessAction,
  getCommentsAction,
  getCommentsSuccessAction, getNewCommentsAction, getNewCommentsSuccessAction
} from '../actions/comment.action';
import { clone } from '../../../shared/utils/clone';

const initialState: CommentsStateInterface = {
  isCommentsLoading: false,
  isCommentsLoaded: false,
  comments: null,
  newComments: null,
  commentsErrors: null,
  message: null,
}

const commentsReducer = createReducer(
  initialState,
  on(
    getCommentsAction,
    (state): CommentsStateInterface => {
      return {
        ...state,
        isCommentsLoading: true,
        isCommentsLoaded: false,
      }
    }
  ),
  on(
    getCommentsSuccessAction,
    (state, action): CommentsStateInterface => ({
      ...state,
      isCommentsLoading: false,
      isCommentsLoaded: true,
      comments: action.comments,
    })
  ),
  on(
    getNewCommentsAction,
    (state): CommentsStateInterface => {
      return {
        ...state,
        isCommentsLoading: true,
        isCommentsLoaded: false,
      }
    }
  ),
  on(
    getNewCommentsSuccessAction,
    (state, action): CommentsStateInterface => ({
      ...state,
      isCommentsLoading: false,
      isCommentsLoaded: true,
      newComments: action.newComments,
    })
  ),
  on(
    deleteCommentSuccessAction,
    (state, action): CommentsStateInterface => {
      if (action.parentId) {
        const commentToRemoveSubCommentFromIndex = state.comments.findIndex( comment => comment._id === action.parentId );
        const updatedComments = clone(state.comments);
        updatedComments[commentToRemoveSubCommentFromIndex].subComments = updatedComments[commentToRemoveSubCommentFromIndex].subComments.filter(subComment =>
          subComment._id !== action._id
        );
        return {
          ...state,
          isCommentsLoading: false,
          message: action.message,
          comments: updatedComments
        }
      } else {
        return {
          ...state,
          isCommentsLoading: false,
          message: action.message,
          comments: state.comments.filter((comment) => {
            return comment._id !== action._id;
          })
        }
      }
    }
  ),
  on(
    clearCommentErrorsAction,
    (state, action): CommentsStateInterface => ({
      ...state,
      commentsErrors: null
    })
  ),
  on(
    clearCommentsMessageAction,
    (state, action): CommentsStateInterface => ({
      ...state,
      message: null
    })
  ),
)

export function commentsReducers(state: CommentsStateInterface, action: Action) {
  return commentsReducer(state, action)
}
