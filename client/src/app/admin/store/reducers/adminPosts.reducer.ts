import { Action, createReducer, on } from '@ngrx/store';

import {
  addPostFailureAction,
  addPostSuccessAction,
  clearPostErrorsAction,
  clearPostMessageAction,
  deletePostFailureAction,
  deletePostSuccessAction,
  getAllPostsAmountFailureAction,
  getAllPostsAmountSuccessAction,
  getPostAction,
  getPostFailureAction,
  getPostsAction,
  getPostsFailureAction,
  getPostsSuccessAction,
  getPostSuccessAction,
  updatePostFailureAction,
  updatePostSuccessAction
} from '../actions/adminPost.action';
import { AdminPostsStateInterface } from '../types/adminPostsState.interface';


const initialState: AdminPostsStateInterface = {
  isPostsLoading: false,
  posts:  [],
  postErrors: null,
  isPostsLoaded: false,
  allPostsAmount: 0,
  message: null,
  currentPost: null,
}

const adminPostsReducer = createReducer(
  initialState,
  on(
    getPostsAction,
    (state): AdminPostsStateInterface => {
      return {
        ...state,
        isPostsLoading: true,
        isPostsLoaded: false,
      }
    }
  ),
  on(
    getPostsSuccessAction,
    (state, action): AdminPostsStateInterface => ({
      ...state,
      isPostsLoading: false,
      posts: action.posts,
      isPostsLoaded: true,
    })
  ),
  on(
    getPostsFailureAction,
    (state, action): AdminPostsStateInterface => ({
      ...state,
      postErrors: action.message
    })
  ),
  on(
    getPostAction,
    (state): AdminPostsStateInterface => {
      return {
        ...state,
        isPostsLoading: true,
        isPostsLoaded: false,
      }
    }
  ),
  on(
    getPostSuccessAction,
    (state, action): AdminPostsStateInterface => ({
      ...state,
      isPostsLoading: false,
      currentPost: action.currentPost,
      isPostsLoaded: true,
    })
  ),
  on(
    getPostFailureAction,
    (state, action): AdminPostsStateInterface => ({
      ...state,
      postErrors: action.message
    })
  ),
  on(
    addPostSuccessAction,
    (state, action): AdminPostsStateInterface => {
      return {
        ...state,
        posts: [...state.posts, action.post],
        allPostsAmount: state.allPostsAmount + 1,
        message: action.message,
      }
    }
  ),
  on(
    addPostFailureAction,
    (state, action): AdminPostsStateInterface => {
      return {
        ...state,
        postErrors: action.message
      }
    }
  ),
  on(
    updatePostSuccessAction,
    (state, action): AdminPostsStateInterface => {
      const postIndex = state.posts.findIndex( post => post._id === action.post._id );
      const updatedPosts = [...state.posts];
      updatedPosts[postIndex] = action.post;
      return {
        ...state,
        posts: updatedPosts,
        message: action.message,
      }
    }
  ),
  on(
    updatePostFailureAction,
    (state, action): AdminPostsStateInterface => {
      return {
        ...state,
        postErrors: action.message
      }
    }
  ),
  on(
    deletePostSuccessAction,
    (state, action): AdminPostsStateInterface => ({
      ...state,
      posts: state.posts.filter((post) => {
        return post._id !== action._id;
      }),
      allPostsAmount: state.allPostsAmount - 1,
      message: action.message,
    })
  ),
  on(
    deletePostFailureAction,
    (state, action): AdminPostsStateInterface => {
      return {
        ...state,
        postErrors: action.message
      }
    }
  ),
  on(
    getAllPostsAmountSuccessAction,
    (state, action): AdminPostsStateInterface => ({
      ...state,
      allPostsAmount: action.allPostsAmount,
    })
  ),
  on(
    getAllPostsAmountFailureAction,
    (state, action): AdminPostsStateInterface => ({
      ...state,
      postErrors: action.message
    })
  ),
  on(
    clearPostErrorsAction,
    (state, action): AdminPostsStateInterface => ({
      ...state,
      postErrors: null
    })
  ),
  on(
    clearPostMessageAction,
    (state, action): AdminPostsStateInterface => ({
      ...state,
      message: null
    })
  ),
)

export function adminPostsReducers(state: AdminPostsStateInterface, action: Action) {
  return adminPostsReducer(state, action)
}
