import { Action, combineReducers } from '@ngrx/store';
import { AdminStateInterface } from '../types/adminState.interface';
import { adminPostsReducers } from './adminPosts.reducer';
import { categoriesReducers } from './categories.reducer';
import { tagsReducers } from './tag.reducer';
import { commentsReducers } from './comment.reducer';


const initialState: AdminStateInterface = {
  adminPostsState: null,
  categoriesState: null,
  tagState: null,
  commentsState: null,
}

export function adminReducers(state: AdminStateInterface, action: Action) {
  const adminReducer = combineReducers(
    {
      adminPostsState: adminPostsReducers,
      categoriesState: categoriesReducers,
      tagState: tagsReducers,
      commentsState: commentsReducers,
    }
  );
  return adminReducer(state, action);
}

