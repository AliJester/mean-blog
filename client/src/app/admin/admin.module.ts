import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';

import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';

import { CategoryListComponent } from './components/categories/category-list/category-list.component';
import { CommentListComponent } from './components/comments/comment-list/comment-list.component';
import { EditCategoryComponent } from './components/categories/edit-category/edit-category.component';
import { EditTagComponent } from './components/tags/edit-tag/edit-tag.component';
import { EditMainPageComponent } from './components/edit-main-page/edit-main-page.component';
import { NewCommentsListComponent } from './components/comments/new-comments-list/new-comments-list.component';
import { PostListPageComponent } from './components/posts/post-list-page/post-list-page.component';
import { TagListComponent } from './components/tags/tag-list/tag-list.component';
import { TitleEditComponent } from './components/settings/title-edit/title-edit.component';

import { ADMIN_STORE } from '../shared/utils/constants/storeConstants';
import { AdminHeaderComponent } from './shared/components/layouts/admin-header/admin-header.component';
import { DropdownDirective } from './shared/directives/dropdown.directive';
import { SharedModule } from '../shared/shared.module';
import { SidebarComponent } from './shared/components/layouts/sidebar/sidebar.component';
import { StringWithoutSpacesDirective } from '../shared/directives/validators/stringWithoutSpaces-validator.directive';

import { AddCategoryEffect } from './store/effects/category/addCategory.effect';
import { AddPostEffect } from './store/effects/post/addPost.effect';
import { AddTagEffect } from './store/effects/tag/addTag.effect';
import { AddTagWithoutRedirectEffect } from './store/effects/tag/addTagWithoutRedirect.effect';
import { DeleteCategoryEffect } from './store/effects/category/deleteCategory.effect';
import { DeleteCommentEffect } from './store/effects/comment/deleteComment.effect';
import { DeletePostEffect } from './store/effects/post/deletePost.effect';
import { DeleteTagEffect } from './store/effects/tag/deleteTag.effect';
import { GetAdminPostEffect } from './store/effects/post/getAdminPost.effect';
import { GetAdminPostsEffect } from './store/effects/post/getAdminPosts.effect';
import { GetAllPostsAmountEffect } from './store/effects/post/amount/getAllPostsAmount.effect';
import { GetCategoriesEffect } from './store/effects/category/getCategories.effect';
import { GetCommentsEffect } from './store/effects/comment/getComments.effect';
import { GetNewCommentsEffect } from './store/effects/comment/getNewComments.effect';
import { GetTagsEffect } from './store/effects/tag/getTags.effect';
import { UpdateCategoryEffect } from './store/effects/category/updateCategory.effect';
import { UpdatePostEffect } from './store/effects/post/updatePost.effect';
import { UpdateTagEffect } from './store/effects/tag/updateTag.effect';
import { adminReducers } from './store/reducers/admin.reducer';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AdminComponent,
    AdminHeaderComponent,
    CategoryListComponent,
    DropdownDirective,
    EditCategoryComponent,
    EditMainPageComponent,
    // EditMainPostComponent,
    EditTagComponent,
    PostListPageComponent,
    SidebarComponent,
    StringWithoutSpacesDirective,
    TagListComponent,
    TitleEditComponent,
    CommentListComponent,
    NewCommentsListComponent,
  ],
  exports: [
    // TagsComponent,
    // LinkToHiddenPostComponent,
    // EditMainPostComponent,
  ],
  imports: [
    AdminRoutingModule,
    //BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    SharedModule,
    StoreModule.forFeature(ADMIN_STORE, adminReducers),
    EffectsModule.forFeature([
      AddCategoryEffect,
      AddPostEffect,
      AddTagEffect,
      AddTagWithoutRedirectEffect,
      GetAdminPostEffect,
      GetAdminPostsEffect,
      GetAllPostsAmountEffect,
      GetCategoriesEffect,
      GetCommentsEffect,
      GetNewCommentsEffect,
      GetTagsEffect,
      DeleteCategoryEffect,
      DeleteCommentEffect,
      DeletePostEffect,
      DeleteTagEffect,
      UpdateCategoryEffect,
      UpdatePostEffect,
      UpdateTagEffect,
    ]),
  ]
})
export class AdminModule {
}
