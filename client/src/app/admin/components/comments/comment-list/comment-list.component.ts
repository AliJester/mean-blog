import { Component, Inject, OnInit, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { MessagesService } from '../../../../shared/services/messages.service';
import { PostComment } from '../../../../shared/models/PostComment.model';
import {
  commentsMessageSelector,
  commentsSelector,
  isCommentsLoadedSelector,
  isCommentsLoadingSelector
} from '../../../store/selectors/comment.selectors';
import {
  clearCommentsMessageAction,
  deleteCommentAction,
  getCommentsAction
} from '../../../store/actions/comment.action';
import { PostSubComment } from '../../../../shared/models/PostSubComment.model';
import { AdditionalDataService } from '../../../../shared/services/additionalData.service';
import { CommentService } from '../../../../shared/services/comment.service';
import { ModalService } from '../../../../shared/services/modal.service';

@Component({
  selector: 'ali-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.scss']
})
export class CommentListComponent implements OnInit {

  comments$: Observable<PostComment[]>;
  chosenComment: string = null;
  isLoading$: Observable<boolean>;
  isLoaded$: Observable<boolean>;

  constructor(
    private messagesService: MessagesService,
    private modalService: ModalService,
    private commentService: CommentService,
    private additionalDataService: AdditionalDataService,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store,
    @Inject(DOCUMENT) document,
    private renderer: Renderer2,
  ) {
  }

  ngOnInit(): void {
    this.initializeValues();
    this.fetchData();
  }

  initializeValues(): void {
    this.isLoading$ = this.store.pipe(select(isCommentsLoadingSelector));
    this.isLoaded$ = this.store.pipe(select(isCommentsLoadedSelector));
    this.comments$ = this.store.pipe(
      select(commentsSelector)
    );
  }

  fetchData(): void {
    this.store.dispatch(getCommentsAction());
  }

  deleteComment(comment: PostComment) {
    this.modalService.confirmThis('Вы уверены, что хотите удалить комментарий со всеми ответами?', () => {
        this.store.dispatch(deleteCommentAction({_id: comment._id, parentId: null}));
      },
      () => {
      });
    this.store.pipe(select(commentsMessageSelector)).subscribe(
      (text) => {
        if (text) {
          this.messagesService.success(text);
          this.store.dispatch(clearCommentsMessageAction());
          this.messagesService.clearMessage();
        }
      });
  }

  deleteSubComment(subComment: PostSubComment, parentId: string) {
    this.modalService.confirmThis('Вы уверены, что хотите удалить ответ на комментарий?', () => {
        this.store.dispatch(deleteCommentAction({_id: subComment._id, parentId: parentId}));
      },
      () => {
      });
    this.store.pipe(select(commentsMessageSelector)).subscribe(
      (text) => {
        if (text) {
          this.messagesService.success(text);
          this.store.dispatch(clearCommentsMessageAction());
          this.messagesService.clearMessage();
        }
      });
  }

  rowClick(id: string) {
    this.chosenComment = id;
    const el: HTMLElement = document.getElementById(id);
    const elClassName = el.className;
    if (elClassName.includes('show')) {
      this.renderer.setAttribute(el, 'class', elClassName.replace(' show', ''));
    } else {
      const collapses: NodeListOf<any> = document.querySelectorAll('[class*=\'collapse\']');
      if (collapses) {
        for (let i = 0; i < collapses.length; i++) {
          let collapse = collapses[i];
          let collapseClassName = collapse.className;
          if (collapseClassName.includes('show')) {
            this.renderer.setAttribute(collapse, 'class', collapseClassName.replace(' show', ''));
          }
        }
      }
      this.renderer.setAttribute(el, 'class', elClassName + ' show');
    }
  }
}
