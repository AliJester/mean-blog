import { Component, ElementRef, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { switchMap, map } from 'rxjs/operators';
import { of } from 'rxjs';
import { Category } from '../../../../shared/models/Category.model';
import { MessagesService } from '../../../../shared/services/messages.service';
import { Message } from '../../../../shared/models/Message.model';
import {
  DANGER_MESSAGE,
} from '../../../../shared/utils/constants/messageConstants';
import { CategoryService } from '../../../../shared/services/category.service';
import {
  categoriesErrorSelector,
  categoriesSelector,
} from '../../../store/selectors/categories.selectors';
import {
  addCategoryAction,
  clearCategoryErrorsAction,
  updateCategoryAction
} from '../../../store/actions/category.action';
import { ADMIN_CATEGORY_LIST } from '../../../../shared/utils/constants/urlConstants';

@Component({
  selector: 'ali-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {
  isNew: boolean = true;
  categoryName: string;
  id: string;

  constructor(
    private messagesService: MessagesService,
    private categoryService: CategoryService,
    private router: Router,
    private route: ActivatedRoute,
    private elem: ElementRef,
    private store: Store,
  ) {
  }

  ngOnInit(): void {
    this.route.params
      .pipe(
        switchMap(
          (params: Params) => {
            const id = params['id'];
            if (id) {
              this.isNew = false;
              this.id = id;
              return this.store.pipe(
                select(categoriesSelector),
                map(cats => {
                  return cats.find((cat) => {
                    return cat._id === id;
                  });
                })
              );
            }
            return of(null);
          }
        )
      )
      .subscribe(
        (category: Category) => {
          if (category) {
            this.categoryName = category.name;
          }
        },
        error => {
          const message = new Message('Category wasn\'t updated.', 2500, DANGER_MESSAGE);
          this.messagesService.showMessageEmitter.next(message);
        }
      );

    this.store.pipe(select(categoriesErrorSelector)).subscribe(
      (text) => {
        if (text) {
          const message = new Message(text, 2500, DANGER_MESSAGE);
          this.messagesService.showMessageEmitter.next(message);
          this.store.dispatch(clearCategoryErrorsAction());
        }
      }
    );
  }

  createOrUpdateCategory(f: NgForm) {
    if (this.isNew) {
      const category = {
        catName: f.value.categoryName,
        redirect: true,
      };
      this.store.dispatch(addCategoryAction(category));
    } else {
      const category: Category = {
        _id:this.id,
        name: f.value.categoryName,
      };
      this.store.dispatch(updateCategoryAction({cat: category}));
    }
  }

  cancel() {
    this.router.navigate([ADMIN_CATEGORY_LIST]);
  }
}
