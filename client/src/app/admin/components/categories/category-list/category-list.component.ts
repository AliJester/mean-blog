import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { CategoryService } from '../../../../shared/services/category.service';
import { Category } from '../../../../shared/models/Category.model';
import { MessagesService } from '../../../../shared/services/messages.service';
import {
  categoriesErrorSelector,
  categoriesSelector,
  isCategoriesLoadedSelector
} from '../../../store/selectors/categories.selectors';
import {
  clearCategoryErrorsAction,
  deleteCategoryAction,
  getCategoriesAction
} from '../../../store/actions/category.action';
import { Message } from '../../../../shared/models/Message.model';
import { DANGER_MESSAGE } from '../../../../shared/utils/constants/messageConstants';
import { ModalService } from '../../../../shared/services/modal.service';

@Component({
  selector: 'ali-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {

  categories$: Observable<Category[]>;

  constructor(
    private catService: CategoryService,
    private messagesService: MessagesService,
    private modalService: ModalService,
    private router: Router,
    private store: Store,
  ) {
  }

  ngOnInit(): void {
    this.initializeValues();
    this.fetchData();
  }

  initializeValues(): void {
    this.categories$ = this.store.pipe(
      select(categoriesSelector)
    );
    this.store.pipe(select(categoriesErrorSelector)).subscribe(
      (text) => {
        if (text) {
          const message = new Message(text, 2500, DANGER_MESSAGE);
          this.messagesService.showMessageEmitter.next(message);
          this.store.dispatch(clearCategoryErrorsAction());
        }
      }
    );
  }

  fetchData(): void {
    this.store.pipe(
      select(isCategoriesLoadedSelector),
      take(1))
      .subscribe(isCategoriesLoaded => {
        if (!isCategoriesLoaded) {
          this.store.dispatch(getCategoriesAction());
        }
      });
  }

  deleteCategory(cat: Category) {
    this.modalService.confirmThis(`Вы уверены, что хотите удалить категорию '${cat.name}'`, () => {
        this.store.dispatch(deleteCategoryAction({_id: cat._id}));
      },
      () => {
      });
  }
}
