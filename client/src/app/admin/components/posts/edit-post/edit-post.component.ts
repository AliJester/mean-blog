import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map, switchMap, take } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import 'quill-emoji/dist/quill-emoji.js';
import { MessagesService } from '../../../../shared/services/messages.service';
import { BlogPostService } from '../../../../blog/services/blogPost.service';
import { Post } from '../../../../shared/models/Post.model';
import { CUT_POST } from '../../../../shared/utils/constants/commonConstants';
import { Category } from '../../../../shared/models/Category.model';
import { CategoryService } from '../../../../shared/services/category.service';
import {
  addPostAction,
  clearPostErrorsAction, clearPostMessageAction,
  getPostAction,
  updatePostAction
} from '../../../store/actions/adminPost.action';
import {
  adminCurrentPostSelector,
  isLoadingSelector, isPostsLoadedSelector,
  postErrorSelector, postMessageSelector
} from '../../../store/selectors/adminPosts.selectors';
import { categoriesSelector, isCategoriesLoadedSelector } from '../../../store/selectors/categories.selectors';
import { addCategoryAction, getCategoriesAction } from '../../../store/actions/category.action';
import { Tag } from '../../../../shared/models/Tag.model';
import { tagsSelector } from '../../../store/selectors/tag.selectors';
import { ADMIN_POST_LIST } from '../../../../shared/utils/constants/urlConstants';

@Component({
  selector: 'ali-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit {

  isNew: boolean = true;
  category: Category;
  title: string;
  photo: string;
  text: string;
  id: string;
  cutPosition: number = 250;
  isMain: boolean = false;
  isHidden: boolean = false;
  availableByToken: boolean = false;
  availabilityToken: string;
  mainPostBriefDesc: string;
  mainPostBriefDescCounter: number = 0;
  coolId: string;
  createdDate: Date;
  tag: string;
  tags: Tag[] = [];
  postIdToGet: string;

  @ViewChild('modal') modalRef: ElementRef
  modalDisplay: string = 'none';
  categoryName: string;

  isLoading$: Observable<boolean>;
  isLoaded$: Observable<boolean>;
  categories$: Observable<Category[]>;
  tags$: Observable<Tag[]>;

  editor;

  showMainPostText: boolean = false;
  hiddenStyle: string;

  constructor(
    private messagesService: MessagesService,
    private postService: BlogPostService,
    private catService: CategoryService,
    private router: Router,
    private route: ActivatedRoute,
    private elem: ElementRef,
    private store: Store,
  ) {
  }

  modulesForPhoto = {
    toolbar: [
      ['image']                         // link and image, video
    ]
  };
  modulesForText = {
    'emoji-shortname': true,
    'emoji-textarea': true,
    'emoji-toolbar': true,
    toolbar: {
      container:
        [
          ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
          ['blockquote', 'code-block'],

          [{'header': 1}, {'header': 2}],               // custom button values
          [{'list': 'ordered'}, {'list': 'bullet'}],
          [{'script': 'sub'}, {'script': 'super'}],      // superscript/subscript
          [{'indent': '-1'}, {'indent': '+1'}],          // outdent/indent
          [{'direction': 'rtl'}],                         // text direction

          [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
          [{'header': [1, 2, 3, 4, 5, 6, false]}],

          [{'color': []}, {'background': []}],          // dropdown with defaults from theme
          [{'font': ['sofia', 'slabo', 'roboto', 'inconsolata', 'ubuntu']}],
          [{'align': []}],

          [{'addcut': CUT_POST}], // my custom button

          ['clean'],                                    // remove formatting button
          ['link', 'image', 'video'],
          ['emoji']

        ],
      handlers: {
        "addcut": function (value) {
          if (value) {
            let text = this.quill.getText();
            const indexOf = text.indexOf(CUT_POST);
            let cursorPosition = this.quill.getSelection().index;
            if (~indexOf) {
              text = text.slice(0, indexOf) + text.slice(indexOf + CUT_POST.length);
              this.quill.setText(text);
              if (indexOf > cursorPosition || indexOf == -1) {
                cursorPosition = cursorPosition;
              } else {
                cursorPosition = cursorPosition - CUT_POST.length;
              }
            }
            this.quill.insertText(cursorPosition, value, {
              'color': '#00ccff',
              'bold': true,
              'id': 'CreateCutHere',
            });
            this.quill.setSelection(cursorPosition + value.length);
          }
        }
      }
    }
  }

  getCutPosition(text: string) {
    if (text) {
      return text.indexOf('CreateCutHere')
    }
    return -1;
  }

  ngOnInit(): void {
    this.initializeValues();
    this.fetchData();
  }

  initializeValues(): void {
    this.isLoading$ = this.store.pipe(select(isLoadingSelector));
    this.isLoaded$ = this.store.pipe(select(isPostsLoadedSelector));
    this.postIdToGet = this.route.snapshot.paramMap.get('id')
    this.route.params
      .pipe(
        switchMap(
          (params: Params) => {
            const id = params['id'];
            if (id) {
              this.isNew = false;
              this.id = id;
              return this.store.pipe(
                select(adminCurrentPostSelector),
                map(post => {
                    return post;
                  }
                )
              );
            }
            return of(null);
          }
        )
      )
      .subscribe(
        (post: Post) => {
          if (post) {
            this.category = post.category;
            this.title = post.title;
            this.photo = post.photo;
            this.text = post.text;
            this.id = post._id;
            this.createdDate = post.createdDate;
            this.cutPosition = post.cutPosition;
            this.isHidden = post.isHidden;
            this.availableByToken = post.availableByToken;
            this.availabilityToken = post.availabilityToken;
            this.isMain = post.isMain;
            this.showMainPostText = post.isMain;
            this.mainPostBriefDesc = post.mainPostBriefDesc;
            this.mainPostBriefDescCounter = post.mainPostBriefDesc ? post.mainPostBriefDesc.length : 0;
            this.coolId = post.coolId;
            this.tags = post.tags;
            this.hiddenStyle = this.isHidden ? '0.7' : '1';
          }
        },
        error => {
          this.messagesService.danger('Post wasn\'t found.');
        }
      );
    this.store.pipe(select(postErrorSelector)).subscribe(
      (text) => {
        if (text) {
          this.messagesService.danger(text);
          this.store.dispatch(clearPostErrorsAction());
          this.messagesService.clearMessage();
        }
      }
    );
    this.store.pipe(select(postMessageSelector)).subscribe(
      (text) => {
        if (text) {
          this.messagesService.success(text);
          this.store.dispatch(clearPostMessageAction());
          this.messagesService.clearMessage();
        }
      }
    );

    this.categories$ = this.store.pipe(
      select(categoriesSelector)
    );
    this.tags$ = this.store.pipe(
      select(tagsSelector)
    );
  }

  fetchData(): void {
    this.store.dispatch(getPostAction({coolId: this.postIdToGet}));
    this.store.pipe(
      select(isCategoriesLoadedSelector),
      take(1))
      .subscribe(isCategoriesLoaded => {
        if (!isCategoriesLoaded) {
          this.store.dispatch(getCategoriesAction());
        }
      });
  }

  createOrUpdatePost() {
    const lsAuthor = localStorage.getItem('author');
    const author = (typeof lsAuthor === 'string' ? lsAuthor : 'author');
    const post: Post = {
      category: this.category,
      title: this.title,
      photo: this.photo,
      text: this.text,
      author: author,
      updatedDate: new Date(),
      createdDate: this.createdDate ? this.createdDate : new Date(),
      cutPosition: this.getCutPosition(this.text),
      isHidden: this.isHidden,
      availableByToken: this.availableByToken,
      availabilityToken: this.availabilityToken,
      isMain: this.isMain,
      mainPostBriefDesc: this.mainPostBriefDesc,
      coolId: this.coolId,
      tags: this.tags,//this.getTags(this.tags),
    }
    if (this.isNew) {
      this.store.dispatch(addPostAction({post: post}));
    } else {
      this.store.dispatch(updatePostAction({post: Object.assign({_id: this.id}, post)}));
    }
  }

  onEditorCreated(quill: any) {
    this.editor = quill;
    let placeholderPickerItems = this.elem.nativeElement.querySelector('.ql-addcut.ql-picker');
    var button = document.createElement("button");
    var t = document.createTextNode("Click me");
    button.appendChild(t);
    button.innerHTML = "My Action";

    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute('viewbox', '0 0 18 18');
    var path = document.createElementNS("http://www.w3.org/2000/svg", 'path');
    path.setAttribute('d', 'M5.5 3C4.11929 3 3 4.11929 3 5.5C3 6.88071 4.11929 8 5.5 8C6.88071 8 8 6.88071 8 5.5C8 4.11929 6.88071 3 5.5 3ZM1 5.5C1 3.01472 3.01472 1 5.5 1C7.98528 1 10 3.01472 10 5.5C10 7.12631 9.13728 8.55113 7.84452 9.34175L11.2045 10.898L21.5797 6.0926C22.0809 5.86049 22.6753 6.07859 22.9074 6.57973C23.1395 7.08087 22.9214 7.67529 22.4203 7.9074L13.5839 12L22.4203 16.0926C22.9214 16.3247 23.1395 16.9191 22.9074 17.4203C22.6753 17.9214 22.0809 18.1395 21.5797 17.9074L11.2045 13.102L7.84452 14.6583C9.13728 15.4489 10 16.8737 10 18.5C10 20.9853 7.98528 23 5.5 23C3.01472 23 1 20.9853 1 18.5C1 16.668 2.09478 15.0916 3.66581 14.3895L8.82508 12L3.66579 9.61044C2.09477 8.90834 1 7.33202 1 5.5ZM5.5 16C4.11929 16 3 17.1193 3 18.5C3 19.8807 4.11929 21 5.5 21C6.88071 21 8 19.8807 8 18.5C8 17.1193 6.88071 16 5.5 16Z');
    path.setAttribute('clip-rule', 'evenodd');
    path.setAttribute('fill-rule', 'evenodd');
    path.setAttribute('fill', '#00ccff');
    svg.appendChild(path);
    const myButton = this.elem.nativeElement.querySelector('.ql-addcut');
    myButton.appendChild(svg);
    if (placeholderPickerItems) {
      placeholderPickerItems.appendChild(myButton);
    }
  }

  isHiddenCheck(event: Event) {
    if ((<HTMLInputElement>event.target).checked) {
      this.hiddenStyle = '0.7';
    } else {
      this.hiddenStyle = '1';
    }
  }

  cancel() {
    this.router.navigate([ADMIN_POST_LIST]);
  }

  compareCatsFn(c1: Category, c2: Category): boolean {
    return c1 && c2 ? c1._id === c2._id : c1 === c2;
  }

  mainPostBriefDescWasEdited(event: string) {
    this.mainPostBriefDesc = event;
  }

  isMainWasChanged(event: boolean) {
    this.isMain = event;
  }

  tagWasAdded(event: any) {
    this.tags = event;
  }

  tagWasDeleted(event: any) {
    this.tags = event;
  }

  onTokenGenerated(event: string) {
    if (event === null) {
      this.availabilityToken = event;
      this.availableByToken = false;
    } else {
      this.availabilityToken = event;
      this.availableByToken = true;
    }
  }

  openAddingCategoryModal() {
    this.modalDisplay = 'block';
  }

  addCategory(categoryName: string) {
    const category = {
      catName: categoryName,
      redirect: false
    };
    this.store.dispatch(addCategoryAction(category));
    this.store.pipe(
      select(categoriesSelector),
      map((cats: Category[]) => {
        return cats.find((cat) => {
          return cat.name === categoryName;
        });
      })
    ).subscribe( (resultCat) => {
      this.category = resultCat;
    })
    this.modalDisplay = 'none';
  }

  cancelAddingCategory() {
    this.modalDisplay = 'none';
  }
}
