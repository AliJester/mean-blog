import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { BlogPostService } from '../../../../blog/services/blogPost.service';
import { MessagesService } from '../../../../shared/services/messages.service';
import { Post } from '../../../../shared/models/Post.model';
import {
  clearPostErrorsAction, clearPostMessageAction,
  deletePostAction,
  getPostsAction,
  getAllPostsAmountAction
} from '../../../store/actions/adminPost.action';
import {
  adminPostsSelector,
  isLoadingSelector,
  isPostsLoadedSelector, postErrorSelector, postMessageSelector, allPostsAmountSelector
} from '../../../store/selectors/adminPosts.selectors';
import { Message } from '../../../../shared/models/Message.model';
import { DANGER_MESSAGE } from '../../../../shared/utils/constants/messageConstants';
import { ModalService } from '../../../../shared/services/modal.service';

@Component({
  selector: 'ali-post-list-page',
  templateUrl: './post-list-page.component.html',
  styleUrls: ['./post-list-page.component.scss']
})
export class PostListPageComponent implements OnInit, OnDestroy {

  posts$: Observable<Post[]>;
  isLoading$: Observable<boolean>;
  isLoaded$: Observable<boolean>;
  queryParamsSubscription: Subscription;
  currentPage: number;
  limit: number = 10;
  allPostsAmount$: Observable<number>;
  allPostsAmount: number;

  constructor(
    private postService: BlogPostService,
    private modalService: ModalService,
    private router: Router,
    private route: ActivatedRoute,
    private messagesService: MessagesService,
    private store: Store,
  ) {
  }

  ngOnInit(): void {
    this.initializeValues();
    this.initializeListeners();
  }

  initializeListeners(): void {
    this.queryParamsSubscription = this.route.queryParams.subscribe(
      (params: Params) => {
        this.currentPage = Number(params.page || '1')
        this.fetchData();
      }
    );
  }

  initializeValues(): void {
    this.isLoading$ = this.store.pipe(select(isLoadingSelector));
    this.isLoaded$ = this.store.pipe(select(isPostsLoadedSelector));
    this.posts$ = this.store.pipe(
      select(adminPostsSelector)
    );
    this.allPostsAmount$ = //this.postService.getPostsAmount();
      this.store.pipe(
        select(allPostsAmountSelector),
      );
    this.store.pipe(select(postErrorSelector)).subscribe(
      (text) => {
        if (text) {
          const message = new Message(text, 2500, DANGER_MESSAGE);
          this.messagesService.showMessageEmitter.next(message);
          this.store.dispatch(clearPostErrorsAction());
          this.messagesService.clearMessage();
        }
      }
    );
  }

  fetchData(): void {
    const page: number = this.currentPage;
    const params = {
      limit: this.limit,
      page,
      tag: null,
    };
    this.store.dispatch(getAllPostsAmountAction());
    this.store.dispatch(getPostsAction({params: params}));
  }

  deletePost(_id: string, title: string) {
    this.modalService.confirmThis(`Вы уверены, что хотите удалить пост '${title}'`, () => {
        this.store.dispatch(deletePostAction({_id: _id}));
      },
      () => {
      });

    this.store.pipe(select(postMessageSelector)).subscribe(
      (text) => {
        if (text) {
          this.messagesService.success(text);
          this.store.dispatch(clearPostMessageAction());
          this.messagesService.clearMessage();
        }
      });
  }

  choosePost(coolId: any) {
    this.router.navigate(['/post', coolId]);
  }

  ngOnDestroy(): void {
    this.queryParamsSubscription.unsubscribe();
  }
}
