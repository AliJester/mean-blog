import { NgForm } from '@angular/forms';
import { Component, ElementRef, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Tag } from '../../../../shared/models/Tag.model';
import { MessagesService } from '../../../../shared/services/messages.service';
import { TagService } from '../../../../shared/services/tag.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { tagsErrorSelector, tagsSelector } from '../../../store/selectors/tag.selectors';
import { Message } from '../../../../shared/models/Message.model';
import { DANGER_MESSAGE } from '../../../../shared/utils/constants/messageConstants';
import {
  addTagAction,
  clearTagErrorsAction,
  updateTagAction
} from '../../../store/actions/tag.action';
import { ADMIN_TAG_LIST } from '../../../../shared/utils/constants/urlConstants';

@Component({
  selector: 'ali-edit-tag',
  templateUrl: './edit-tag.component.html',
  styleUrls: ['./edit-tag.component.scss']
})
export class EditTagComponent implements OnInit {
  isNew: boolean = true;
  tagName: string;
  id: string;

  tag$: Observable<Tag>

  constructor(
    private messagesService: MessagesService,
    private tagService: TagService,
    private router: Router,
    private route: ActivatedRoute,
    private elem: ElementRef,
    private store: Store,
  ) {
  }

  ngOnInit(): void {
    this.route.params
      .pipe(
        switchMap(
          (params: Params) => {
            const id = params['id'];
            if (id) {
              this.isNew = false;
              this.id = id;
              return this.store.pipe(
                select(tagsSelector),
                map(tags => {
                  return tags.find((tag) => {
                    return tag._id === id;
                  });
                })
              );
            }
            return of(null);
          }
        )
      )
      .subscribe(
        (tag: Tag) => {
          if (tag) {
            this.tagName = tag.name;
          }
        },
        error => {
          const message = new Message('Tag wasn\'t updated.', 2500, DANGER_MESSAGE);
          this.messagesService.showMessageEmitter.next(message);
        }
      );

    this.store.pipe(select(tagsErrorSelector)).subscribe(
      (text) => {
        if (text) {
          const message = new Message(text, 2500, DANGER_MESSAGE);
          this.messagesService.showMessageEmitter.next(message);
          this.store.dispatch(clearTagErrorsAction());
        }
      }
    )
  }

  createOrUpdateTag(f: NgForm) {
    if (this.isNew) {
      const tag = {
        tagName: f.value.tagName,
      }
      this.store.dispatch(addTagAction(tag));
    } else {
      const tag: Tag = {
        _id:this.id,
        name: f.value.tagName,
      }
      this.store.dispatch(updateTagAction({tag: tag}));
    }
  }

  cancel() {
    this.router.navigate([ADMIN_TAG_LIST]);
  }
}
