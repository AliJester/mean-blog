import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Tag } from '../../../../shared/models/Tag.model';
import { TagService } from '../../../../shared/services/tag.service';
import { MessagesService } from '../../../../shared/services/messages.service';
import { tagsSelector, isTagsLoadedSelector } from '../../../store/selectors/tag.selectors';
import { deleteTagAction, getTagsAction } from '../../../store/actions/tag.action';
import { ModalService } from '../../../../shared/services/modal.service';

@Component({
  selector: 'ali-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss']
})
export class TagListComponent implements OnInit {

  tags$: Observable<Tag[]>;

  constructor(
    private tagService: TagService,
    private modalService: ModalService,
    private messagesService: MessagesService,
    private router: Router,
    private store: Store,
  ) {
  }

  ngOnInit(): void {
    this.initializeValues();
    this.fetchData();
  }

  initializeValues(): void {
    this.tags$ = this.store.pipe(
      select(tagsSelector)
    );
  }

  fetchData(): void {
    this.store.pipe(
      select(isTagsLoadedSelector),
      take(1))
      .subscribe(isTagsLoaded => {
        if (!isTagsLoaded){
          this.store.dispatch(getTagsAction());
        }
      });
  }

  deleteTag(tag: Tag) {
    this.modalService.confirmThis(`Вы уверены, что хотите удалить тэг '${tag.name}'`, () => {
        this.store.dispatch(deleteTagAction({_id: tag._id}));
      },
      () => {
      });
  }
}
