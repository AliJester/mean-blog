import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { SettingsService } from '../../../../shared/services/settings.service';
import { Router } from '@angular/router';
import { DAY_PART } from '../../../../shared/utils/constants/dayPartConstants';
import { TitleImageList } from '../../../../shared/models/TitleImagesList.model';
import { MessagesService } from '../../../../shared/services/messages.service';

@Component({
  selector: 'ali-title-edit',
  templateUrl: './title-edit.component.html',
  styleUrls: ['./title-edit.component.scss']
})
export class TitleEditComponent implements OnInit {

  @ViewChild('loadImg') loadImgInputRef: ElementRef;
  nightImagePreview: string | ArrayBuffer = '';
  morningImagePreview: string | ArrayBuffer = '';
  dayImagePreview: string | ArrayBuffer = '';
  eveningImagePreview: string | ArrayBuffer = '';
  defaultImagePreview: string | ArrayBuffer = '';
  nightImage: File;
  morningImage: File;
  dayImage: File;
  eveningImage: File;
  defaultImage: File;
  currentDayPart: string = '';
  readonly NIGHT = DAY_PART.NIGHT.NAME;
  readonly MORNING = DAY_PART.MORNING.NAME;
  readonly DAY = DAY_PART.DAY.NAME;
  readonly EVENING = DAY_PART.EVENING.NAME;

  constructor(private settingsService: SettingsService,
              private messagesService: MessagesService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.settingsService.getTitle()
      .subscribe((title) => {
        this.nightImagePreview = title.nightImageSrc;
        this.morningImagePreview = title.morningImageSrc;
        this.dayImagePreview = title.dayImageSrc;
        this.eveningImagePreview = title.eveningImageSrc;
        this.defaultImagePreview = title.defaultImageSrc;
      });
  }

  updateTitlePage() {
    const images: TitleImageList = {
      nightImage: this.nightImage,
      morningImage: this.morningImage,
      dayImage: this.dayImage,
      eveningImage: this.eveningImage,
      defaultImage: this.defaultImage
    }
    this.settingsService.createOrUpdate(images)
      .subscribe((message) => {
          if (message.success) {
            this.messagesService.success(message.message);
          } else {
            this.messagesService.danger(message.message);
          }
        this.messagesService.clearMessage();

      });
  }

  cancel() {
    this.router.navigate(['/admin']);
  }

  onFileUpload(event: Event) {
    const target = event.target as HTMLInputElement;
    const reader = new FileReader();
    const image = target.files[0];
    if (this.currentDayPart === this.NIGHT) {
      this.nightImage = image;
      reader.onload = () => {
        this.nightImagePreview = reader.result;
      }
    }
    if (this.currentDayPart === this.MORNING) {
      this.morningImage = image;
      reader.onload = () => {
        this.morningImagePreview = reader.result;
      }
    }
    if (this.currentDayPart === this.DAY) {
      this.dayImage = image;
      reader.onload = () => {
        this.dayImagePreview = reader.result;
      }
    }
    if (this.currentDayPart === this.EVENING) {
      this.eveningImage = image;
      reader.onload = () => {
        this.eveningImagePreview = reader.result;
      }
    }
    if (this.currentDayPart === 'default') {
      this.defaultImage = image;
      reader.onload = () => {
        this.defaultImagePreview = reader.result;
      }
    }
    reader.readAsDataURL(image);
  }

  loadImage(dayPart: string) {
    this.currentDayPart = dayPart;
    this.loadImgInputRef.nativeElement.click();
  }
}
