import { Component, OnInit } from '@angular/core';
import { MessagesService } from "../../shared/services/messages.service";
import { Router } from "@angular/router";
import { PostService } from "../../shared/services/post.service";
import { Message } from "../../shared/models/Message.model";
import { SUCCESS_MESSAGE } from "../../shared/utils/constants/messageConstants";

@Component({
  selector: 'ali-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  category: string
  title: string
  photo: string
  text: string

  constructor(
    private messagesService: MessagesService,
    private postService: PostService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  createPost() {
    const post = {
      category: this.category,
      title: this.title,
      photo: this.photo,
      text: this.text,
      author: 'author', //JSON.parse(localStorage.getItem("user")).login,
      date: new Date()
    }
    // if(!post.category) {
    //   this._flashMessagesService.show('Select a category',
    //     { cssClass: 'alert-danger', timeout: 3000 });
    //   return false
    // }
    // else if(!post.title) {
    //   this._flashMessagesService.show('Enter title',
    //     { cssClass: 'alert-danger', timeout: 3000 });
    //   return false
    // }
    // else if(!post.photo) {
    //   this._flashMessagesService.show('Insert a photo',
    //     { cssClass: 'alert-danger', timeout: 3000 });
    //   return false
    // }
    // else if(!post.text) {
    //   this._flashMessagesService.show('Enter text',
    //     { cssClass: 'alert-danger', timeout: 3000 });
    //   return false
    // }

    console.log('post is',post);

    this.postService.create(post).subscribe( data => {
      console.log('data = ',data);
      // if (!data.success) {
      //   const message = new Message('You are logged out.', 2500, SUCCESS_MESSAGE);
      //   this.messagesService.showMessageEmitter.emit(message);
      //
      // } else {
      //   const message = new Message('You are logged out.', 2500, SUCCESS_MESSAGE);
      //   this.messagesService.showMessageEmitter.emit(message);
      //
      //   this.router.navigate(['/'])
      // }
    })


  }

}
