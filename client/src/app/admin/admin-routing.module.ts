import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminComponent } from './admin.component';
import { CategoryListComponent } from './components/categories/category-list/category-list.component';
import { CommentListComponent } from './components/comments/comment-list/comment-list.component';
import { EditCategoryComponent } from './components/categories/edit-category/edit-category.component';
import { EditPostComponent } from './components/posts/edit-post/edit-post.component';
import { EditTagComponent } from './components/tags/edit-tag/edit-tag.component';
import { NewCommentsListComponent } from './components/comments/new-comments-list/new-comments-list.component';
import { PostListPageComponent } from './components/posts/post-list-page/post-list-page.component';
import { TagListComponent } from './components/tags/tag-list/tag-list.component';
import { TitleEditComponent } from './components/settings/title-edit/title-edit.component';

import { AdminGuard } from './shared/guards/admin.guard';

const routes: Routes = [
  {
    path: '', component: AdminComponent, canActivate: [AdminGuard], canActivateChild: [AdminGuard], children: [//, canActivate: [AuthGuard]},
      {path: 'post/list', component: PostListPageComponent},//, canActivate: [AuthGuard]},
      {path: 'post/new', component: EditPostComponent},//, canActivate: [AuthGuard]},
      {path: 'post/:id', component: EditPostComponent},//, canActivate: [AuthGuard]},
      {path: 'category/list', component: CategoryListComponent},//, canActivate: [AuthGuard]},
      {path: 'category/new', component: EditCategoryComponent},//, canActivate: [AuthGuard]},
      {path: 'category/:id', component: EditCategoryComponent},//, canActivate: [AuthGuard]},
      {path: 'comment/list', component: CommentListComponent},//, canActivate: [AuthGuard]},
      {path: 'comment/list/new', component: NewCommentsListComponent},//, canActivate: [AuthGuard]},
      {path: 'tag/list', component: TagListComponent},//, canActivate: [AuthGuard]},
      {path: 'tag/new', component: EditTagComponent},//, canActivate: [AuthGuard]},
      {path: 'tag/:id', component: EditTagComponent},//, canActivate: [AuthGuard]},
      {path: 'settings/title', component: TitleEditComponent},//, canActivate: [AuthGuard]},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
