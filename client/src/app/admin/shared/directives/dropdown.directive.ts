import { Directive, HostListener, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[aliAdminSidebarDropdown]'
})
export class DropdownDirective {
  constructor(private elRef: ElementRef, private renderer: Renderer2) {
  }

  @HostListener('click', ['$event.target'])
  onClick() {
    const nextDiv = this.elRef.nativeElement.nextElementSibling;
    const nextDivClassName = nextDiv.className;
    const parent = this.elRef.nativeElement.parentElement;
    const parentClassName = parent.className;

    if (parent.className.includes('active')) {
      this.renderer.setAttribute(nextDiv, 'class', nextDivClassName.replace(' toggled',''));
      this.renderer.setAttribute(parent, 'class', parentClassName.replace(' active',''));
    } else {
      this.renderer.setAttribute(nextDiv, 'class', nextDivClassName + ' toggled');
      this.renderer.setAttribute(parent, 'class', parentClassName + ' active');
    }
  }
}
