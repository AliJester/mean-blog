import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ADMIN_LINK_GENERATION_URL } from '../constants/adminConstants';

@Injectable({
  providedIn: 'root'
})
export class LinkGenerationService {

  constructor(private http : HttpClient ) {}

  generate(time: number) : Observable<{token: string}> {
    let options =
      { params: new HttpParams()
          .set('availableTime', time.toString())
      } ;
    // let headers = new HttpHeaders();
    // headers.append('Content-Type', 'application/json');
    return this.http.get<{token:string}>(ADMIN_LINK_GENERATION_URL, options);
  }
}
