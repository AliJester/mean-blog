import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../../../shared/models/Post.model';
import { QueryParams } from '../../../shared/models/QueryParams.model';
import { Message } from '../../../shared/models/Message.model';
import { ADMIN_POSTS_URL } from '../constants/adminConstants';

@Injectable({
  providedIn: 'root'
})
export class AdminPostService {

  constructor(private http : HttpClient ) {}

  create(post:Post): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<Post>(ADMIN_POSTS_URL + '/', post,
      { headers: headers});
  }

  update(post:Post): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.patch<Post>(ADMIN_POSTS_URL + `/${post._id}`, post, { headers: headers});
  }

  getAllPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(ADMIN_POSTS_URL + '/' );
  }

  getAllPostsForAdmin(queryParams: QueryParams): Observable<Post[]> {
    let options =
      { params: new HttpParams().set('admin', 'true')
          .set('limit', (queryParams?.limit).toString())
          .set('page', (queryParams?.page).toString())
      } ;
    return this.http.get<Post[]>(ADMIN_POSTS_URL + `/`, options );
  }

  getById(id) {
    return this.http.get<Post>(ADMIN_POSTS_URL + `/${id}` );
  }

  deletePost(id) {
    return this.http.delete<Message>(ADMIN_POSTS_URL + `/${id}` );
  }

  getAllPostsAmount() {
    return this.http.get<number>(ADMIN_POSTS_URL + `/allPostsAmount/` );
  }

}
