import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommentService } from '../../../../../shared/services/comment.service';
import { AdditionalDataService } from '../../../../../shared/services/additionalData.service';

@Component({
  selector: 'ali-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],

})
export class SidebarComponent implements OnInit, OnDestroy {

  newCommentsAmount: number = 0;

  constructor(private commentService: CommentService,
              private additionalDataService: AdditionalDataService,
  ) {
  }

  ngOnInit(): void {
    this.commentService.getNewCommentsCount().subscribe(
      result => {
        this.newCommentsAmount = result.newCommentsAmount;
      });
    this.commentService.setNewCommentsAmountSubject.subscribe( value => {
      this.newCommentsAmount = value;
    });
  }

  ngOnDestroy(): void {
    this.additionalDataService.setCommentsLastSeenData(new Date()).subscribe();
  }
}
