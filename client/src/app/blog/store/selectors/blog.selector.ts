import { createFeatureSelector, createSelector } from '@ngrx/store'
import { AppStateInterface } from '../../../shared/store/types/appState.interface';
import { BlogStateInterface } from '../types/blogState.interface';


export const blogPostsFeatureSelector = createFeatureSelector<AppStateInterface,
  BlogStateInterface>('blog')

export const isPostsLoadingSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface) => blogState.blogPostsState.isPostsLoading
);

export const isPostsLoadedSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface) => blogState.blogPostsState.isPostsLoaded
);

export const isCurrentPostLoadingSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface) => blogState.blogPostsState.isCurrentPostLoading
);

export const isCurrentPostCommentsLoadedSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface) => blogState.postCommentsState.isCommentsLoaded
);

export const isCurrentPostCommentsLoadingSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface) => blogState.postCommentsState.isCommentsLoading
);

export const isCurrentPostLoadedSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface) => blogState.blogPostsState.isCurrentPostLoaded
);

export const postErrorsSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface) => blogState.blogPostsState.postErrors
);

export const postsSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface) => blogState.blogPostsState.posts
);

export const currentPostSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface) => blogState.blogPostsState.currentPost
);

export const currentPostCommentsSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface) => blogState.postCommentsState.comments
);

export const currentPostCommentsCountSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface, { props }) => {
    let count: number = 0;
    const comments = blogState.postCommentsState.comments;
    if (comments) {
      comments.map((comment) => {
        if (comment.postId === props) {
          count += 1;
          count += comment.subComments ? comment.subComments.length : 0;
        }
      });
    }
    return count;
  }
);

export const mainPostsSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface) => blogState.blogPostsState.mainPosts
);

export const postsAmountSelector = createSelector(
  blogPostsFeatureSelector,
  (blogState: BlogStateInterface) => blogState.blogPostsState.postsAmount
);

