import {createFeatureSelector, createSelector} from '@ngrx/store'
import { BlogStateInterface } from '../types/blogState.interface';
import { AppStateInterface } from '../../../shared/store/types/appState.interface';
import { BLOG_STORE } from '../../../shared/utils/constants/storeConstants';

export const categoriesFeatureSelector = createFeatureSelector<
  AppStateInterface,
  BlogStateInterface
  >(BLOG_STORE)

// export const isLoadingSelector = createSelector(
//   categoriesFeatureSelector,
//   (blogState: BlogStateInterface) => blogState.categoriesState.isCategoriesLoading
// );
//
// export const categoriesErrorSelector = createSelector(
//   categoriesFeatureSelector,
//   (blogState: BlogStateInterface) => blogState.categoriesState.categoriesErrors
// )
//
// export const categoriesSelector = createSelector(
//   categoriesFeatureSelector,
//   (blogState: BlogStateInterface) => blogState.categoriesState.categories
// );
//
// export const isCategoriesLoadedSelector = createSelector(
//   categoriesFeatureSelector,
//   (blogState: BlogStateInterface) => blogState.categoriesState.isCategoriesLoaded
// );



