export enum CategoryTypes {
  GET_CATEGORY = '[BLOG] Get category',
  GET_CATEGORY_SUCCESS = '[BLOG] Get category success',
  GET_CATEGORY_FAILURE = '[BLOG] Get category failure',
}
