export enum TagTypes {
  GET_TAGS = '[BLOG] Get list of tags',
  GET_TAGS_SUCCESS = '[BLOG] Get list of tags success',
  GET_TAGS_FAILURE = '[BLOG] Get list of tags failure',
}
