import { createAction, props } from '@ngrx/store';
import { BlogActionTypes } from './types/blogActionTypes';
import { Post } from '../../../shared/models/Post.model';
import { QueryParams } from '../../../shared/models/QueryParams.model';
import { PostComment } from '../../../shared/models/PostComment.model';
import { PostSubComment } from '../../../shared/models/PostSubComment.model';

export const getCurrentPostAction = createAction(
  BlogActionTypes.GET_POST,
  props<{coolId: string, token: string}>()
);

export const getCurrentPostSuccessAction = createAction(
  BlogActionTypes.GET_POST_SUCCESS,
  props<{currentPost: Post}>()
);

export const getCurrentPostFailureAction = createAction(
  BlogActionTypes.GET_POST_FAILURE,
  props<{message: string}>()
);

export const getCurrentPostCommentsAction = createAction(
  BlogActionTypes.GET_POST_COMMENTS,
  props<{postId: string}>()
);

export const getCurrentPostCommentsSuccessAction = createAction(
  BlogActionTypes.GET_POST_COMMENTS_SUCCESS,
  props<{comments: PostComment[]}>()
);

export const getCurrentPostCommentsFailureAction = createAction(
  BlogActionTypes.GET_POST_COMMENTS_FAILURE,
  props<{message: string}>()
);

export const addCurrentPostCommentAction = createAction(
  BlogActionTypes.ADD_POST_COMMENT,
  props<{comment: PostComment}>()
);

export const addCurrentPostCommentSuccessAction = createAction(
  BlogActionTypes.ADD_POST_COMMENT_SUCCESS,
  props<{comment: PostComment}>()
);

export const addCurrentPostCommentFailureAction = createAction(
  BlogActionTypes.ADD_POST_COMMENT_FAILURE,
  props<{message: string}>()
);

export const updateCurrentPostCommentAction = createAction(
  BlogActionTypes.UPDATE_POST_COMMENT,
  props<{comment: PostSubComment, parentCommentId: string}>()
);

export const updateCurrentPostCommentSuccessAction = createAction(
  BlogActionTypes.UPDATE_POST_COMMENT_SUCCESS,
  props<{parentCommentId: string, fullComment: any}>()
);

export const updateCurrentPostCommentFailureAction = createAction(
  BlogActionTypes.UPDATE_POST_COMMENT_FAILURE,
  props<{message: string}>()
);

export const getPostsAction = createAction(
  BlogActionTypes.GET_POSTS,
  props<{params: QueryParams}>()
);

export const getPostsSuccessAction = createAction(
  BlogActionTypes.GET_POSTS_SUCCESS,
  props<{posts: Post[]}>()
);

export const getPostsFailureAction = createAction(
  BlogActionTypes.GET_POSTS_FAILURE,
  props<{message: string}>()
);

export const getMainPostsAction = createAction(
  BlogActionTypes.GET_MAIN_POSTS,
);

export const getMainPostsSuccessAction = createAction(
  BlogActionTypes.GET_MAIN_POSTS_SUCCESS,
  props<{mainPosts: Post[]}>()
);

export const getMainPostsFailureAction = createAction(
  BlogActionTypes.GET_MAIN_POSTS_FAILURE
);

export const getPostsAmountAction = createAction(
  BlogActionTypes.GET_POSTS_AMOUNT,
  props<{params: QueryParams}>()
);

export const getPostsAmountSuccessAction = createAction(
  BlogActionTypes.GET_POSTS_AMOUNT_SUCCESS,
  props<{postsAmount: number}>()
);

export const getPostsAmountFailureAction = createAction(
  BlogActionTypes.GET_POSTS_AMOUNT_FAILURE,
  props<{message: string}>()
);

export const clearPostErrorsAction = createAction(
  BlogActionTypes.CLEAR_POST_ERRORS
);
