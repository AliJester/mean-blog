import { createAction, props } from '@ngrx/store';
import { Tag } from '../../../shared/models/Tag.model';
import { TagTypes } from './types/tagTypes';

export const getTagsAction = createAction(
  TagTypes.GET_TAGS
);
export const getTagsSuccessAction = createAction(
  TagTypes.GET_TAGS_SUCCESS,
  props<{tags: Tag[]}>()
);
export const getTagsFailureAction = createAction(
  TagTypes.GET_TAGS_FAILURE,
  props<{message: string}>()
);
