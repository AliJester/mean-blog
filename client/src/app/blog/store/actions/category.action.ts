import { createAction, props } from '@ngrx/store';
import { Category } from '../../../shared/models/Category.model';
import { CategoryTypes } from './types/categoryTypes';

export const getCategoryAction = createAction(
  CategoryTypes.GET_CATEGORY,
  props<{_id: string}>()
);
export const getCategorySuccessAction = createAction(
  CategoryTypes.GET_CATEGORY_SUCCESS,
  props<{category: Category}>()
);
export const getCategoryFailureAction = createAction(
  CategoryTypes.GET_CATEGORY_FAILURE,
  props<{message: string}>()
);
