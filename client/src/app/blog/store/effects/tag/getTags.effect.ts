import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { map, catchError, switchMap } from 'rxjs/operators'
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs'

import { TagService } from '../../../../shared/services/tag.service';
import { Tag } from '../../../../shared/models/Tag.model';
import { getTagsAction, getTagsFailureAction, getTagsSuccessAction } from '../../actions/tag.action';

@Injectable()
export class GetTagsEffect {
  getTags$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getTagsAction),
      switchMap(() => {
        return this.tagService.getAllTags().pipe(
          map((tags: Tag[]) => {
            return getTagsSuccessAction({tags})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(getTagsFailureAction({message: error.message}))
          }),
        )
      })
    )
  )

  constructor(private actions$: Actions, private tagService: TagService) {
  }
}
