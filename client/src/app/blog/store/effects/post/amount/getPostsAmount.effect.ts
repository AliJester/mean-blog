import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'

import { BlogPostService } from '../../../../services/blogPost.service';
import {
  getPostsAmountAction, getPostsAmountFailureAction, getPostsAmountSuccessAction
} from '../../../actions/blog.action';

@Injectable()
export class GetPostsAmountEffect {
  getPostsAmount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getPostsAmountAction),
      switchMap(({params}) => {
        return this.postService.getPostsAmount(params).pipe(
          map((postsAmount) => {
            return getPostsAmountSuccessAction({postsAmount})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(getPostsAmountFailureAction({message: error.message}))
          }),
        )
      })
    )
  );

  constructor(private actions$: Actions, private postService: BlogPostService) {
  }
}
