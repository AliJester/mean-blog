import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'

import { BlogPostService } from '../../../services/blogPost.service';
import {
  getMainPostsAction,
  getMainPostsFailureAction,
  getMainPostsSuccessAction
} from '../../actions/blog.action';
import { Post } from '../../../../shared/models/Post.model';

@Injectable()
export class GetMainPostsEffect {
  getMainPosts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getMainPostsAction),
      switchMap(() => {
        return this.postService.getAllMainPosts().pipe(
          map((mainPosts: Post[]) => {
            return getMainPostsSuccessAction({mainPosts})
          }),
          catchError(() => {
            return of(getMainPostsFailureAction())
          })
        )
      })
    )
  )

  constructor(private actions$: Actions, private postService: BlogPostService) {
  }
}
