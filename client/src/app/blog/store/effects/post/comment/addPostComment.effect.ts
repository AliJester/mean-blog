import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { catchError, map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { CommentService } from '../../../../../shared/services/comment.service';
import {
  addCurrentPostCommentAction, addCurrentPostCommentFailureAction, addCurrentPostCommentSuccessAction
} from '../../../actions/blog.action';

@Injectable()
export class AddPostCommentEffect {
  addPost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(addCurrentPostCommentAction),
      switchMap(({comment}) => {
        const newComment = {...comment}
        return this.commentService.createComment(comment).pipe(
          map((createCommentResult) => {
            if (createCommentResult.success) {
              newComment._id = createCommentResult.newCommentId;
              return addCurrentPostCommentSuccessAction({comment: newComment})
            } else {
              return addCurrentPostCommentFailureAction({message: createCommentResult.message});
            }
          }),
          catchError((error: HttpErrorResponse) => {
            return of(addCurrentPostCommentFailureAction({message: error.message}))
          }),
        )
      })
    )
  );

  constructor(
    private actions$: Actions,
    private commentService: CommentService,
  ) {
  }
}
