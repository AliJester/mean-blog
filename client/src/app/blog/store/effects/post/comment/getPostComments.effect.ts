import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'
import {
  getCurrentPostCommentsAction, getCurrentPostCommentsFailureAction, getCurrentPostCommentsSuccessAction,
} from '../../../actions/blog.action';
import { CommentService } from '../../../../../shared/services/comment.service';
import { PostComment } from '../../../../../shared/models/PostComment.model';

@Injectable()
export class GetPostCommentsEffect {
  getPostComments$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCurrentPostCommentsAction),
      switchMap(({postId}) => {
        return this.commentService.getCommentsByPostId(postId).pipe(
          map((comments: PostComment[]) => {
            return getCurrentPostCommentsSuccessAction({comments})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(getCurrentPostCommentsFailureAction({message: error.message}))
          }),
        )
      })
    )
  );

  constructor(private actions$: Actions, private commentService: CommentService) {
  }
}
