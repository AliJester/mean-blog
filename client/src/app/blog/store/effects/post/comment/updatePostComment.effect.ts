import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import {
  updateCurrentPostCommentAction,
  updateCurrentPostCommentFailureAction,
  updateCurrentPostCommentSuccessAction
} from '../../../actions/blog.action';
import { CommentService } from '../../../../../shared/services/comment.service';


@Injectable()
export class UpdatePostCommentEffect {
  updatePostComment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateCurrentPostCommentAction),
      switchMap(({comment, parentCommentId}) => {
        return this.commentsService.updateComment(comment, parentCommentId).pipe(
          map((updatePostCommentResult) => {
            if (updatePostCommentResult.success) {
              return updateCurrentPostCommentSuccessAction({parentCommentId, fullComment: updatePostCommentResult.comment})
            } else {
              return updateCurrentPostCommentFailureAction({message: updatePostCommentResult.message});
            }
          }),
          catchError((error: HttpErrorResponse) => {
            return of(updateCurrentPostCommentFailureAction({message: error.message}))
          }),
        )
      })
    )
  );

  constructor(
    private actions$: Actions,
    private commentsService: CommentService,
  ) {
  }
}
