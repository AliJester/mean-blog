import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'

import { BlogPostService } from '../../../services/blogPost.service';
import {
  getCurrentPostAction, getCurrentPostFailureAction, getCurrentPostSuccessAction,
} from '../../actions/blog.action';
import { Post } from '../../../../shared/models/Post.model';

@Injectable()
export class GetPostEffect {
  getCurrentPost$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getCurrentPostAction),
      switchMap((post) => {
        return this.postService.getById(post.coolId, post.token).pipe(
          map((currentPost: Post) => {
            return getCurrentPostSuccessAction({currentPost})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(getCurrentPostFailureAction({message: error.message}))
          })
        )
      })
    )
  );

  constructor(private actions$: Actions, private postService: BlogPostService) {
  }
}
