import { Injectable } from '@angular/core'
import { createEffect, Actions, ofType } from '@ngrx/effects'
import { HttpErrorResponse } from '@angular/common/http';
import { map, catchError, switchMap } from 'rxjs/operators'
import { of } from 'rxjs'

import { BlogPostService } from '../../../services/blogPost.service';
import { getPostsAction, getPostsFailureAction, getPostsSuccessAction } from '../../actions/blog.action';
import { Post } from '../../../../shared/models/Post.model';

@Injectable()
export class GetPostsEffect {
  getPosts$ = createEffect(() =>
    this.actions$.pipe(
      ofType(getPostsAction),
      switchMap(({params}) => {
        return this.postService.getAllPostsForBlog(params).pipe(
          map((posts: Post[]) => {
            return getPostsSuccessAction({posts})
          }),
          catchError((error: HttpErrorResponse) => {
            return of(getPostsFailureAction({message: error.message}))
          })
        )
      })
    )
  );

  constructor(private actions$: Actions, private postService: BlogPostService) {
  }
}
