import { Post } from '../../../shared/models/Post.model';

export interface BlogPostsStateInterface {
  isPostsLoading: boolean
  isPostsLoaded: boolean
  isCurrentPostLoading: boolean
  isCurrentPostLoaded: boolean
  postsAmount: number
  message: string
  posts: Post[] | null
  currentPost: Post | null
  mainPosts: Post[] | null
  postErrors: string | null
}
