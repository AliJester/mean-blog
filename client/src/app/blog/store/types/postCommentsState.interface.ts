import { PostComment } from '../../../shared/models/PostComment.model';

export interface PostCommentsStateInterface {
  isCommentsLoading: boolean
  isCommentsLoaded: boolean
  comments: PostComment[] | null
  commentsErrors: string | null
}
