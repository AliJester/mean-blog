import { Tag } from '../../../shared/models/Tag.model';

export interface TagsStateInterface {
  isTagsLoading: boolean
  tags: Tag[] | null
  tagsErrors: string | null
  isTagsLoaded: boolean
}
