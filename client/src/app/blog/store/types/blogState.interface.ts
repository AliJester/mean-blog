import { BlogPostsStateInterface } from './blogPostsState.interface';
import { PostCommentsStateInterface } from './postCommentsState.interface';

export interface BlogStateInterface {
  blogPostsState: BlogPostsStateInterface
  postCommentsState: PostCommentsStateInterface
}
