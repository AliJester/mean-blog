import { Action, combineReducers, createReducer, on } from '@ngrx/store';
import { blogPostsReducers } from './blogPosts.reducer';
import { BlogStateInterface } from '../types/blogState.interface';
import { postCommentsReducers } from './postComments.reducer';

const initialState: BlogStateInterface = {
  blogPostsState: null,
  postCommentsState: null,
}

export function blogReducers(state: BlogStateInterface, action: Action) {
  const blogReducer = combineReducers(
    {
      blogPostsState: blogPostsReducers,
      postCommentsState: postCommentsReducers,
    }
  );
  return blogReducer(state, action)
}
