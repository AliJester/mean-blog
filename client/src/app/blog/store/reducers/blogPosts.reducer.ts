import { Action, createReducer, on } from '@ngrx/store';
import {
  getCurrentPostAction,
  getCurrentPostFailureAction,
  getCurrentPostSuccessAction, getMainPostsAction, getMainPostsFailureAction, getMainPostsSuccessAction,
  getPostsAction, getPostsAmountFailureAction, getPostsAmountSuccessAction, getPostsFailureAction, getPostsSuccessAction
} from '../actions/blog.action';
import { BlogPostsStateInterface } from '../types/blogPostsState.interface';
import { clearPostErrorsAction } from '../../../admin/store/actions/adminPost.action';

const initialState: BlogPostsStateInterface = {
  posts: null,
  currentPost: null,
  mainPosts: null,
  isPostsLoading: false,
  isPostsLoaded: false,
  isCurrentPostLoading: false,
  isCurrentPostLoaded: false,
  postsAmount: 0,
  message: null,
  postErrors: null
  //error: null
}


const blogPostsReducer = createReducer(
  initialState,
  on(
    getCurrentPostAction,
    (state): BlogPostsStateInterface => {
      return {
        ...state,
        isCurrentPostLoading: true,
        isCurrentPostLoaded: false,
      }
    }
  ),
  on(
    getCurrentPostSuccessAction,
    (state, action): BlogPostsStateInterface => ({
      ...state,
      isCurrentPostLoading: false,
      isCurrentPostLoaded: true,
      currentPost: action.currentPost
    })
  ),
  on(
    getCurrentPostFailureAction,
    (state, action): BlogPostsStateInterface => ({
      ...state,
      isCurrentPostLoading: false,
      postErrors: action.message
    })
  ),
  on(
    getPostsAction,
    (state): BlogPostsStateInterface => {
      return {
        ...state,
        isPostsLoading: true,
        isPostsLoaded: false,
      }
    }
  ),
  on(
    getPostsSuccessAction,
    (state, action): BlogPostsStateInterface => ({
      ...state,
      isPostsLoading: false,
      isPostsLoaded: true,
      posts: action.posts
    })
  ),
  on(
    getPostsFailureAction,
    (state, action): BlogPostsStateInterface => ({
      ...state,
      isPostsLoading: false,
      postErrors: action.message
    })
  ),
  on(
    getMainPostsAction,
    (state): BlogPostsStateInterface => {
      return {
        ...state,
        isPostsLoading: true,
      }
    }
  ),
  on(
    getMainPostsSuccessAction,
    (state, action): BlogPostsStateInterface => ({
      ...state,
      //isLoading: false,
      mainPosts: action.mainPosts,
    })
  ),
  on(
    getMainPostsFailureAction,
    (state, action): BlogPostsStateInterface => ({
      ...state,
//      postErrors: action.message

    })
  ),
  on(
    getPostsAmountSuccessAction,
    (state, action): BlogPostsStateInterface => ({
      ...state,
      postsAmount: action.postsAmount,
    })
  ),
  on(
    getPostsAmountFailureAction,
    (state, action): BlogPostsStateInterface => ({
      ...state,
      message: action.message
    })
  ),
  on(
    clearPostErrorsAction,
    (state, action): BlogPostsStateInterface => ({
      ...state,
      postErrors: null
    })
  ),
)

export function blogPostsReducers(state: BlogPostsStateInterface, action: Action) {
  return blogPostsReducer(state, action)
}
