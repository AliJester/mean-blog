import { Action, createReducer, on } from '@ngrx/store';
import {
  addCurrentPostCommentAction,
  addCurrentPostCommentFailureAction,
  addCurrentPostCommentSuccessAction,
  getCurrentPostCommentsAction,
  getCurrentPostCommentsFailureAction,
  getCurrentPostCommentsSuccessAction,
  updateCurrentPostCommentFailureAction,
  updateCurrentPostCommentSuccessAction,
} from '../actions/blog.action';
import { PostCommentsStateInterface } from '../types/postCommentsState.interface';
import { clone } from '../../../shared/utils/clone';

const initialState: PostCommentsStateInterface = {
  isCommentsLoading: false,
  isCommentsLoaded: false,
  comments: null,
  commentsErrors: null,
}

const postCommentsReducer = createReducer(
  initialState,
  on(
    getCurrentPostCommentsAction,
    (state): PostCommentsStateInterface => {
      return {
        ...state,
        isCommentsLoading: true,
        isCommentsLoaded: false,
      }
    }
  ),
  on(
    getCurrentPostCommentsSuccessAction,
    (state, action): PostCommentsStateInterface => ({
      ...state,
      isCommentsLoading: false,
      isCommentsLoaded: true,
      comments: action.comments
    })
  ),
  on(
    getCurrentPostCommentsFailureAction,
    (state, action): PostCommentsStateInterface => ({
      ...state,
      isCommentsLoading: false,
      commentsErrors: action.message
    })
  ),
  on(
    addCurrentPostCommentAction,
    (state): PostCommentsStateInterface => {
      return {
        ...state,
        isCommentsLoading: true,
        isCommentsLoaded: false,
      }
    }
  ),
  on(
    addCurrentPostCommentSuccessAction,
    (state, action): PostCommentsStateInterface => ({
      ...state,
      isCommentsLoading: false,
      isCommentsLoaded: true,
      comments: [...state.comments, action.comment],
    })
  ),
  on(
    addCurrentPostCommentFailureAction,
    (state, action): PostCommentsStateInterface => ({
      ...state,
      isCommentsLoading: false,
      commentsErrors: action.message
    })
  ),
  on(
    updateCurrentPostCommentSuccessAction,
    (state, action): PostCommentsStateInterface => {
      const commentIndex = state.comments.findIndex( comment => comment._id === action.parentCommentId );
      const updatedComments = clone(state.comments);
      updatedComments[commentIndex] = action.fullComment;
      return {
        ...state,
        comments: updatedComments
      }
    }
  ),
  on(
    updateCurrentPostCommentFailureAction,
    (state, action): PostCommentsStateInterface => ({
      ...state,
      isCommentsLoading: false,
      commentsErrors: action.message
    })
  ),
)

export function postCommentsReducers(state: PostCommentsStateInterface, action: Action) {
  return postCommentsReducer(state, action)
}
