import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { PostComponent } from './components/post/post.component';
import { BlogComponent } from './blog.component';
import { NotFoundComponent } from '../shared/components/not-found/not-found.component';
import { AboutComponent } from './components/about/about.component';
import { NOT_FOUND } from '../shared/utils/constants/urlConstants';

const routes: Routes = [
  {path: '', component: BlogComponent, children: [
      {path: 'home', component: HomeComponent},
      {path: 'about', component: AboutComponent},
      {path: 'post/:id', component: PostComponent},
      {path: NOT_FOUND, component: NotFoundComponent},
      {path: '**', redirectTo: NOT_FOUND},
    ] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlogRoutingModule { }
