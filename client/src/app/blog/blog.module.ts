import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { QuillModule } from 'ngx-quill';

import { BlogComponent } from './blog.component';
import { BlogRoutingModule } from './blog-routing.module';

import { AboutMeComponent } from '../shared/components/layouts/about-me/about-me.component';
import { AboutSidebarComponent } from '../shared/components/layouts/about/about-sidebar.component';
import { ArchivesComponent } from '../shared/components/layouts/archives/archives.component';
import { CategoryPaneComponent } from '../shared/components/layouts/category-pane/category-pane.component';
import { FooterComponent } from '../shared/components/layouts/footer/footer.component';
import { LeftSidebarComponent } from '../shared/components/layouts/sidebars/left-sidebar/left-sidebar.component';
import { PostTextPipe } from '../shared/pipes/post-text.pipe';
import { RightSidebarComponent } from '../shared/components/layouts/sidebars/right-sidebar/right-sidebar.component';
import { SharedModule } from '../shared/shared.module';
import { ShowMessageComponent } from '../shared/components/show-message/show-message.component';
import { SocialComponent } from '../shared/components/layouts/social/social/social.component';
import { TitleComponent } from '../shared/components/layouts/title/title.component';

import { AboutComponent } from './components/about/about.component';
import { BottomNavComponent } from './components/bottom-nav/bottom-nav.component';
import { CommentsComponent } from './components/post/comments/comments.component';
import { HomeComponent } from './components/home/home.component';
import { MainPostsPageComponent } from './components/main-posts-page/main-posts-page.component';
import { PostComponent } from './components/post/post.component';
import { ReadMoreComponent } from './components/read-more/read-more.component';

import { blogReducers } from './store/reducers/blog.reducer';
import { AddPostCommentEffect } from './store/effects/post/comment/addPostComment.effect';
import { GetPostsEffect } from './store/effects/post/posts.effect';
import { GetMainPostsEffect } from './store/effects/post/mainPosts.effect';
import { GetPostsAmountEffect } from './store/effects/post/amount/getPostsAmount.effect';
import { GetPostEffect } from './store/effects/post/post.effect';
import { GetPostCommentsEffect } from './store/effects/post/comment/getPostComments.effect';
import { UpdatePostCommentEffect } from './store/effects/post/comment/updatePostComment.effect';

@NgModule({
  declarations: [
    AboutComponent,
    AboutMeComponent,
    AboutSidebarComponent,
    ArchivesComponent,
    BlogComponent,
    BottomNavComponent,
    CategoryPaneComponent,
    CommentsComponent,
    FooterComponent,
    HomeComponent,
    LeftSidebarComponent,
    MainPostsPageComponent,
    PostComponent,
    PostTextPipe,
    RightSidebarComponent,
    ReadMoreComponent,
    SocialComponent,
    TitleComponent,
  ],
  exports: [
    ShowMessageComponent
  ],
  imports: [
    BlogRoutingModule,
    CommonModule,
    EffectsModule.forFeature([
      AddPostCommentEffect,
      GetPostCommentsEffect,
      GetPostEffect,
      GetPostsEffect,
      GetMainPostsEffect,
      GetPostsAmountEffect,
      UpdatePostCommentEffect,
    ]),
    QuillModule.forRoot(),
    SharedModule,
    StoreModule.forFeature('blog', blogReducers),
    FormsModule,

  ]
})
export class BlogModule {
}
