import { Component, Input } from '@angular/core';

@Component({
  selector: 'ali-read-more',
  templateUrl: './read-more.component.html',
  styleUrls: ['./read-more.component.scss']
})
export class ReadMoreComponent {

  @Input('linkToGo') linkToGoProps: string;
  @Input('queryParams') queryParamsProps: string;
  @Input('isStretchedLink') isStretchedLinkProps: boolean = false;

  constructor() { }
}
