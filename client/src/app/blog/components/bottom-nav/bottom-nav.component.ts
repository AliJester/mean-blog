import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'ali-bottom-nav',
  templateUrl: './bottom-nav.component.html',
  styleUrls: ['./bottom-nav.component.scss']
})
export class BottomNavComponent implements OnInit {
  @Input('itemsAmount') itemsAmountProps: number;
  @Input('currentPage') currentPageProps: number;
  @Input('numberOfItemsPerPage') numberOfItemsPerPageProps: number;
  @Input('apiUrl') apiUrlProps: string;
  @Input('hiddenLeft') hiddenLeftProps: boolean = false;
  @Input('hiddenRight') hiddenRightProps: boolean = false;
  @Input('leftButtonName') leftButtonNameProps: string = 'Позже';
  @Input('rightButtonName') rightButtonNameProps: string = 'Раньше';
  @Input('ignoreQueryParams') ignoreQueryParamsProps: boolean = false;
  numberOfPages: number = 1;
  pages: number[];
  queryParamLeft: object = {};
  queryParamRight: object = {};

  constructor(
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.numberOfPages = Math.ceil(this.itemsAmountProps / this.numberOfItemsPerPageProps);
    this.route.queryParams.subscribe(
      (params: Params) => {
        if (!this.ignoreQueryParamsProps) {
          if (this.currentPageProps === undefined) {
            if (params.page !== undefined) {
              this.currentPageProps = Number(params.page[0] - 1 || '0');
            } else {
              this.currentPageProps = 0;
            }
          }
          this.queryParamLeft = {page: this.currentPageProps + 1};
          this.queryParamRight = {page: this.currentPageProps - 1};
          if (params.tag !== undefined) {
            Object.assign(this.queryParamLeft, {tag: params.tag});
            Object.assign(this.queryParamRight, {tag: params.tag});
          } else if (params.category !== undefined) {
            Object.assign(this.queryParamLeft, {category: params.category});
            Object.assign(this.queryParamRight, {category: params.category});
          }
        }
      }
    );
  }

}
