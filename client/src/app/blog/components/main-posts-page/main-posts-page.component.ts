import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BlogPostService } from '../../services/blogPost.service';
import { Post } from '../../../shared/models/Post.model';
import { getMainPostsAction } from '../../store/actions/blog.action';
import { mainPostsSelector } from '../../store/selectors/blog.selector';

@Component({
  selector: 'ali-main-posts-page',
  templateUrl: './main-posts-page.component.html',
  styleUrls: ['./main-posts-page.component.scss']
})
export class MainPostsPageComponent implements OnInit {

  image;
  mainPosts$: Observable<Post[]>;

  constructor(private postService: BlogPostService,
              private sanitizer: DomSanitizer,
              private store: Store,
  ) { }

  ngOnInit(): void {
    this.initializeValues();
    this.fetchData();
  }
  fetchData(): void {
    this.store.dispatch(getMainPostsAction());
  }

  initializeValues(): void {
    this.mainPosts$ =
      this.store.pipe(
        select(mainPostsSelector),
        map((mainPosts) => {
          const newMainPosts = JSON.parse(JSON.stringify(mainPosts))
          if (mainPosts) {
            for (let i = 0; i < newMainPosts.length; i++) {
              const post = newMainPosts[i];
              const photo = post.photo;
              if (~photo.indexOf('src=')) {
                let lastPhotoEl = photo.indexOf('="></p>');
                if (~lastPhotoEl) {
                  const imageByteArray = photo.slice(photo.indexOf('src=') + 5, lastPhotoEl + 1);
                  post.image = this.sanitizer.bypassSecurityTrustUrl(imageByteArray);
                }
              }
            }
          }
          return newMainPosts;
        })
      );
  }

}
