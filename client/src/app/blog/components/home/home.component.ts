import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { BlogPostService } from '../../services/blogPost.service';
import { Post } from '../../../shared/models/Post.model';
import {
  isPostsLoadedSelector,
  isPostsLoadingSelector,
  postsAmountSelector,
  postsSelector
} from '../../store/selectors/blog.selector';
import { getPostsAction, getPostsAmountAction } from '../../store/actions/blog.action';
import { MessagesService } from '../../../shared/services/messages.service';

@Component({
  selector: 'ali-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  posts = [];
  // private postsSub: Subscription;
  posts$: Observable<Post[]>;
  queryParamsSubscription: Subscription;
  isTagInUrl: boolean = false;
  isCatInUrl: boolean = false;
  isAuthorInUrl: boolean = false;

  currentPage: number;
  tagName:string;
  catName:string;
  authorName:string;
  limit: number = 5;
  postsAmount$: Observable<number>;
  isLoading$: Observable<boolean>;
  isLoaded$: Observable<boolean>;

  constructor(
    private postService: BlogPostService,
    private store: Store,
    private router: Router,
    private route: ActivatedRoute,
    private messageService: MessagesService,

  ) { }

  ngOnInit(): void {
    this.messageService.clearMessage();
    this.initializeValues();
    this.initializeListeners();
  }

  initializeListeners(): void {
    this.queryParamsSubscription = this.route.queryParams.subscribe(
      (params: Params) => {
        this.currentPage = Number(params.page || '1');
        this.tagName = params.tag;
        this.catName = params.category;
        this.authorName = params.author;
        // if (this.tagName) {
        //   this.isTagInUrl = true;
        // } else {
        //   this.isTagInUrl = false;
        // }
        this.isTagInUrl = !!this.tagName;
        this.isCatInUrl = !!this.catName;
        this.isAuthorInUrl = !!this.authorName;
        this.fetchData();
      }
    )
  }

  initializeValues(): void {
    this.isTagInUrl = false;
    this.isCatInUrl = false;
    this.isAuthorInUrl = false;
    this.isLoading$ = this.store.pipe(select(isPostsLoadingSelector));
    this.isLoaded$ = this.store.pipe(select(isPostsLoadedSelector));
    this.postsAmount$ =
      this.store.pipe(
        select(postsAmountSelector),
      );

    this.posts$ = this.store.pipe(
      select(postsSelector),
      map( (posts) => {
        const newPosts = JSON.parse(JSON.stringify(posts))
        if (posts) {
          for (let i = 0; i < newPosts.length; i++) {
            let cutPosition = newPosts[i].cutPosition;
            cutPosition = cutPosition == -1 ? 250 : cutPosition;
            newPosts[i].text = newPosts[i].text.substring(0, cutPosition-1);
          }
        }
        return newPosts;
      }),
    );
  }

  fetchData(): void {
    const page: number = this.currentPage;
    const params = {
      limit: this.limit,
      page,
      tag: this.tagName,
      category: this.catName,
      author: this.authorName,
    };
    this.store.dispatch(getPostsAmountAction({params: params}));
    this.store.dispatch(getPostsAction({params: params}));
  }
}
