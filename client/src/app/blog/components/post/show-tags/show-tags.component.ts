import { Component, Input, OnInit } from '@angular/core';
import { Tag } from '../../../../shared/models/Tag.model';

@Component({
  selector: 'ali-show-tags',
  templateUrl: './show-tags.component.html',
  styleUrls: ['./show-tags.component.scss']
})
export class ShowTagsComponent implements OnInit {

  @Input('tags') tagsProps: Tag[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
