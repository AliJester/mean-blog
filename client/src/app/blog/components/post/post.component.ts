import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ActivatedRoute, Params } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { Observable, of, Subscription } from 'rxjs';
import { AuthService } from '../../../shared/services/auth.service';
import { BlogPostService } from '../../services/blogPost.service';
import { Post } from '../../../shared/models/Post.model';
import {
  currentPostSelector,
  isCurrentPostLoadedSelector, isCurrentPostLoadingSelector,
  postErrorsSelector
} from '../../store/selectors/blog.selector';
import { getCurrentPostAction } from '../../store/actions/blog.action';


@Component({
  selector: 'ali-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit, OnDestroy {

  post$ : Observable<Post>;
  login: string;

  token: string

  isLoading$: Observable<boolean>;
  isLoaded$: Observable<boolean>;
  postIdToGet: string;

  commentsCount: number = 0;

  queryParamsSubscription: Subscription;

  currentPostId: string;
  currentPostCoolId: string;

  constructor(
    private postService: BlogPostService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private store: Store,

  ) { }

  ngOnInit(): void {
    this.initializeValues();
    this.initializeListeners();
  }
  initializeListeners(): void {
    this.queryParamsSubscription = this.route.queryParams.subscribe(
      (params: Params) => {
        this.token = params.token;
        this.fetchData();
      }
    )
  }

  initializeValues(): void {
    this.isLoading$ = this.store.pipe(select(isCurrentPostLoadingSelector));
    this.isLoaded$ = this.store.pipe(select(isCurrentPostLoadedSelector));
    this.postIdToGet = this.route.snapshot.paramMap.get('id');
    this.post$ = this.route.params
      .pipe(
        switchMap(
          (params: Params) => {
              return this.store.pipe(
                select(currentPostSelector),
                map(post => {
                  this.currentPostId = post._id;
                  this.currentPostCoolId = post.coolId;
                    return post;
                  }
                )
              );
            return of(null);
          }
        )
      );

    this.store.pipe(select(postErrorsSelector)).subscribe(
      (text) => {
        if (text) {
          // this.messagesService.danger(text);
          // this.store.dispatch(clearPostErrorsAction());
          // this.messagesService.clearMessage();
        }
      }
    );
  }

  fetchData(): void {
    // const params = {
    //   token: this.token,
    // };
    this.store.dispatch(getCurrentPostAction({coolId: this.postIdToGet, token: this.token}));
  }

  ngOnDestroy() {
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }
  }

  onCommentsCount(event: number) {
    this.commentsCount = event;
  }
}

