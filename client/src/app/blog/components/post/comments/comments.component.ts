import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  Renderer2,
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { NgModel } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { PostComment } from '../../../../shared/models/PostComment.model';
import { CommentService } from '../../../../shared/services/comment.service';
import {
  currentPostCommentsCountSelector,
  currentPostCommentsSelector,
  isCurrentPostCommentsLoadedSelector, isCurrentPostCommentsLoadingSelector,
} from '../../../store/selectors/blog.selector';
import {
  addCurrentPostCommentAction,
  getCurrentPostCommentsAction, updateCurrentPostCommentAction
} from '../../../store/actions/blog.action';
import { PostSubComment } from '../../../../shared/models/PostSubComment.model';

@Component({
  selector: 'ali-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {

  chosenComment: string = '';
  commentText: string;
  commentAuthorName: string;
  commentAuthorNamePlaceHolder: string = 'Введите Ваше имя';
  replyCommentAuthorNamePlaceHolder: string = 'Введите Ваше имя';
  replySubCommentAuthorNamePlaceHolder: string = 'Введите Ваше имя';
  commentTextPlaceHolder: string = 'Введите Ваш комментарий';
  replyCommentTextPlaceHolder: string = 'Введите Ваш комментарий';
  replySubCommentTextPlaceHolder: string = 'Введите Ваш комментарий';
  commentAuthorNameToAnswer: string = '';
  replyCommentText: string;
  replyCommentAuthorName: string;
  replySubCommentText: string;

  commentTextLength: number = 0;
  replyCommentTextLength: number = 0;
  replySubCommentTextLength: number = 0;
  commentTextSpanStyle: string = 'green';
  replyCommentTextSpanStyle: string = 'green';
  replySubCommentTextSpanStyle: string = 'green';


  comments$ : Observable<PostComment[]>;

  isLoading$: Observable<boolean>;
  isLoaded$: Observable<boolean>;

  @Input('postId') postIdProps: string;
  @Input('postCoolId') postCoolIdProps: string;
  @Output() onCommentsCount = new EventEmitter<number>();

  constructor(private commentService: CommentService,
              private store: Store,
              @Inject(DOCUMENT) document,
              private renderer: Renderer2,
  ) {
  }

  ngOnInit(): void {
    this.initializeValues();
    this.fetchData();
  }

  initializeValues(): void {
    this.isLoading$ = this.store.pipe(select(isCurrentPostCommentsLoadingSelector));
    this.isLoaded$ = this.store.pipe(select(isCurrentPostCommentsLoadedSelector));
    this.comments$ = this.store.pipe(select(currentPostCommentsSelector));
    this.store.pipe(select(currentPostCommentsCountSelector, {props: this.postIdProps})).subscribe((commentsCount) => {
      if (commentsCount) {
        this.onCommentsCount.emit(commentsCount);
      }
    });
  }

  fetchData(): void {
    this.store.dispatch(getCurrentPostCommentsAction({postId: this.postIdProps}));
  }

  addComment() {
    const newComment: PostComment = {
      postId: this.postIdProps,
      postCoolId: this.postCoolIdProps,
      author: this.commentAuthorName,
      text: this.commentText,
      createdDate: new Date(),
    };
    this.store.dispatch(addCurrentPostCommentAction({comment: newComment}));
    this.commentText = '';
    this.commentAuthorName = '';
    this.commentTextLength = 0;
  }

  reply(replyPostId: string, parentCommentId: string, commentAuthor: string) {
    this.chosenComment = replyPostId;
    const newReplyComment: PostSubComment = {
      author: this.replyCommentAuthorName,
      replyPostAuthor: commentAuthor,
      replyPostId: replyPostId,
      text: this.replyCommentText,
      createdDate: new Date(),
    };
    this.store.dispatch(updateCurrentPostCommentAction({comment: newReplyComment, parentCommentId: parentCommentId}));
    this.replyCommentText='';
    this.replyCommentTextLength = 0;
    this.replySubCommentTextLength = 0;
    this.replyCommentAuthorName='';
    this.chosenComment = null;
  }

  showAddCommentBlock(id: string) {
    this.chosenComment = id;
  }

  showReplyComment(replyPostId: string) {
    const el: HTMLElement = document.getElementById(replyPostId);
    const elClassName = el.className;
    if (!elClassName.includes('active')) {
      this.renderer.setAttribute(el, 'class', elClassName + ' active');
    }
  }

  hideReplyComment(replyPostId: string) {
    const el: HTMLElement = document.getElementById(replyPostId);
    const elClassName = el.className;
    if (elClassName.includes('active')) {
      this.renderer.setAttribute(el, 'class', elClassName.replace(' active',''));
    }
  }

  commentTextInput(event: any) {
    this.commentTextLength = this.commentText.length;
    this.setCommentTextSpanStyle(this.commentTextLength);
  }

  replyCommentTextInput($event: any) {
    this.replyCommentTextLength = this.replyCommentText.length;
    this.setReplyCommentTextSpanStyle(this.replyCommentTextLength);
  }

  replySubCommentTextInput($event: any) {
    this.replySubCommentTextLength = this.replyCommentText.length;
    this.setReplySubCommentTextSpanStyle(this.replySubCommentTextLength);
  }

  private setCommentTextSpanStyle(counter: number) {
    this.commentTextSpanStyle = this.getSpanStyle(counter);
  }

  private setReplyCommentTextSpanStyle(counter: number) {
    this.replyCommentTextSpanStyle = this.getSpanStyle(counter);
  }
  private setReplySubCommentTextSpanStyle(counter: number) {
    this.replySubCommentTextSpanStyle = this.getSpanStyle(counter);
  }

  private getSpanStyle(counter: number) {
    if (counter < 1500) {
      return 'green';
    } else if (counter < 2250) {
      return 'gold';
    } else if (counter < 2850) {
      return 'orange';
    } else {
      return 'red';
    }
  }

  commentAuthorNameBlur(commentAuthorNameRef: NgModel) {
    if (commentAuthorNameRef.value===undefined) {
      this.commentAuthorNamePlaceHolder = 'Имя не должно быть пустым!';
    }
  }
  commentTextBlur(commentTextRef: NgModel) {
    if (commentTextRef.value===undefined) {
      this.commentTextPlaceHolder = 'Комментарий не должен быть пустым!';
    }
  }

  replyCommentAuthorNameBlur(replyCommentAuthorNameRef: NgModel) {
    if (replyCommentAuthorNameRef.value===undefined) {
      this.replyCommentAuthorNamePlaceHolder = 'Имя не должно быть пустым!';
    }
  }
  replyCommentTextBlur(replyCommentTextRef: NgModel) {
    if (replyCommentTextRef.value===undefined) {
      this.replyCommentTextPlaceHolder = 'Комментарий не должен быть пустым!';
    }
  }
  replySubCommentAuthorNameBlur(replySubCommentAuthorNameRef: NgModel) {
    if (replySubCommentAuthorNameRef.value===undefined) {
      this.replySubCommentAuthorNamePlaceHolder = 'Имя не должно быть пустым!';
    }
  }
  replySubCommentTextBlur(replySubCommentTextRef: NgModel) {
    if (replySubCommentTextRef.value===undefined) {
      this.replySubCommentTextPlaceHolder = 'Комментарий не должен быть пустым!';
    }
  }
}
