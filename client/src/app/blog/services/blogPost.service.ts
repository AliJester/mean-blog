import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../../shared/models/Post.model';
import { QueryParams } from '../../shared/models/QueryParams.model';
import { BLOG_POSTS_URL } from '../shared/blogConstants';

@Injectable({
  providedIn: 'root'
})
export class BlogPostService {

  constructor(private http : HttpClient ) {}

  getAllPosts(): Observable<Post[]> {
    return this.http.get<Post[]>('/api/blog/posts/' );
  }

  getAllPostsForBlog(queryParams: QueryParams): Observable<Post[]> {
    let options =
      { params: new HttpParams()
          .set('admin', 'false')
          .set('limit', (queryParams?.limit).toString())
          .set('page', (queryParams?.page).toString())
          .set('tag', (queryParams?.tag)?.toString())
          .set('category', (queryParams?.category)?.toString())
          .set('author', (queryParams?.author)?.toString())
      } ;
    return this.http.get<Post[]>(BLOG_POSTS_URL + `/`, options );
  }

  getAllMainPosts(): Observable<Post[]> {
    const options =
      { params: new HttpParams().set('isMain', 'true') } ;
    return this.http.get<Post[]>('/api/blog/posts/', options );
  }

  getById(id, token) {
    const options =
      { params: new HttpParams().set('token', token) } ;
    return this.http.get<Post>(BLOG_POSTS_URL + `/${id}`, options );
  }

  // getByTagId(tagId) {
  //   return this.http.get<Post>(BLOG_POSTS_URL + `/tag/${tagId}` );
  // }

  getPostsAmount(queryParams: QueryParams) {
    let options =
      { params: new HttpParams()
          .set('admin', 'false')
          .set('tag', (queryParams?.tag)?.toString())
          .set('category', (queryParams?.category)?.toString())
          .set('author', (queryParams?.author)?.toString())
      } ;
    return this.http.get<number>(BLOG_POSTS_URL + `/postsAmount/`, options );
  }

  // getAllPostsAmount() {
  //   return this.http.get<number>(BLOG_POSTS_URL + `/allPostsAmount/` );
  // }

  getMainPostsAmount() {
    return this.http.get<number>(BLOG_POSTS_URL + `/mainPostsAmount/` );
  }
}
