import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ali-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent {
  constructor(public router: Router) {}
}
