import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from '../../shared/services/auth.service';
import { MessagesService } from '../../shared/services/messages.service';
import { AUTH, REG } from '../../shared/utils/constants/urlConstants';

@Component({
  selector: 'ali-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnDestroy {

  @ViewChild('regForm') signupForm: NgForm;
  @ViewChild('inputName',{ read: ElementRef }) inputName: ElementRef;
  authSubscription: Subscription;

  constructor(
    private authService: AuthService,
    private messagesService: MessagesService,
    private router: Router
  ) { }

  signUp(form: NgForm) {
    this.authSubscription = this.authService.registerUser(form.value).subscribe(
      () => {
        this.messagesService.success('Registration is successful');
        this.router.navigate([AUTH]);
        this.messagesService.clearMessage();
      },
      (error) => {
        let message;
        if (error.error.message) {
          this.messagesService.danger(error.error.message);
        } else {
          this.messagesService.defaultMessage();
        }
        form.form.enable();
        this.messagesService.clearMessage();
        this.router.navigate([REG]);
      }
    );
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }
}

