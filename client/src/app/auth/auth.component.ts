import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NOT_FOUND } from '../shared/utils/constants/urlConstants';

@Component({
  selector: 'ali-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
    if (!this.router.url.includes('/auth/auth') && !this.router.url.includes('/auth/reg') ) {
      this.router.navigate([NOT_FOUND]);
    }
  }

}
