import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../shared/services/auth.service';
import { MessagesService } from '../../shared/services/messages.service';
import { ADMIN_POST_LIST } from '../../shared/utils/constants/urlConstants';

@Component({
  selector: 'ali-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit, OnDestroy {

  @ViewChild('loginForm') signinForm: NgForm;
  signInSubscription: Subscription;
  autoLoginSubscription: Subscription;

  constructor(
    private authService: AuthService,
    private messagesService: MessagesService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.autoLogin();
  }

  signIn(form: NgForm) {
    this.signInSubscription = this.authService.login(this.signinForm.value)
      .subscribe(
        () => {
          this.messagesService.success('Login success');
          this.router.navigate([ADMIN_POST_LIST]);
        },
        (error) => {
          this.messagesService.danger(error.error.message);
        }
      );
  }

  autoLogin() {
    this.autoLoginSubscription = this.authService.autoLogin()
      .subscribe(
        () => {
          this.messagesService.success('Auto login success');
          this.router.navigate([ADMIN_POST_LIST]);
        },
      );
  }

  ngOnDestroy(): void {
    if (this.signInSubscription) {
      this.signInSubscription.unsubscribe();
    }
    if (this.autoLoginSubscription) {
      this.autoLoginSubscription.unsubscribe();
    }

  }
}
