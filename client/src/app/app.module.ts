import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { QuillModule } from 'ngx-quill';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TokenInterceptor } from './shared/interceptors/token.interceptor';
// import { DashboardComponent } from './admin/components/dashboard/dashboard.component';
import { RegisterPageComponent } from './auth/register-page/register-page.component';
import { LoginPageComponent } from './auth/login-page/login-page.component';
import { PasswordValidatorDirective } from './shared/directives/validators/password-validator.directive';
import { ConfirmPasswordValidatorDirective } from './shared/directives/validators/confirm-password-validator.directive';
import { EditPostComponent } from './admin/components/posts/edit-post/edit-post.component';
import { BlogModule } from './blog/blog.module';
import { SharedModule } from './shared/shared.module';
import { environment } from '../environments/environment';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';
// import { StringWithoutSpacesDirective } from './shared/directives/validators/stringWithoutSpaces-validator.directive';

@NgModule({
  declarations: [
    AppComponent,
    ConfirmPasswordValidatorDirective,
    // DashboardComponent,
    EditPostComponent,
    LoginPageComponent,
    NotFoundComponent,
    PasswordValidatorDirective,
    RegisterPageComponent,
    // StringWithoutSpacesDirective,
  ],
  imports: [
    AppRoutingModule,
    BlogModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    QuillModule,
    ReactiveFormsModule,
    SharedModule,
    EffectsModule.forRoot([]),
    StoreModule.forRoot({}),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production
    }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: TokenInterceptor
    }

  ],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
