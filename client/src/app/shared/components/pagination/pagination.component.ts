import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { UtilsService } from '../../services/utils.service';

@Component({
  selector: 'ali-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaginationComponent implements OnInit {
  @Input('itemsAmount') itemsAmountProps: number;
  @Input('currentPage') currentPageProps: number;
  @Input('numberOfItemsPerPage') numberOfItemsPerPageProps: number;
  @Input('apiUrl') apiUrlProps: string;
  digitsNumber: number = 5;
  halfDigitsNumber: number = 2;
  numberOfPages: number = 1;
  pages: number[];

  constructor(private utilsService: UtilsService) { }

  ngOnInit(): void {
    this.halfDigitsNumber = Math.floor(this.digitsNumber/2);
    this.numberOfPages = Math.ceil(this.itemsAmountProps / this.numberOfItemsPerPageProps);
    if (this.numberOfPages > this.digitsNumber ) {
      if (this.currentPageProps > this.halfDigitsNumber) {
        this.pages = this.utilsService.range(this.currentPageProps - this.halfDigitsNumber, this.currentPageProps + this.halfDigitsNumber);
      } else {
        this.pages = this.utilsService.range(1, this.digitsNumber);
      }
    } else {
      this.pages = this.utilsService.range(1, this.numberOfPages);
    }
  }

  onClick() {
    this.ngOnInit();
  }
}
