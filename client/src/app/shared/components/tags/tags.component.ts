import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Tag } from '../../models/Tag.model';
import { addTagWithoutRedirectAction } from '../../../admin/store/actions/tag.action';
import { tagsSelector } from '../../../admin/store/selectors/tag.selectors';

@Component({
  selector: 'ali-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss']
})
export class TagsComponent implements OnInit {

  @Input('tags') tagsProps: Tag[] = [];
  @Output() onTagAddedEvent = new EventEmitter<Tag[]>();
  @Output() onTagDeletedEvent = new EventEmitter<Tag[]>();

  tags: Tag[] = [];
  tag: string;
  tags$: Observable<Tag[]>;

  constructor(
    private store: Store,
  ) {
  }

  ngOnInit(): void {
    this.tagsProps.map((tag) => {
      this.tags.push(new Tag(tag.name, [], tag._id));
    });
    this.initializeValues();

    let resultTags = [];
    this.tags$.subscribe(
      (tags) => {
        tags.map((tag) => {
          resultTags = this.createNewTagsArrayToEmit(this.tags, tags);
          this.onTagAddedEvent.emit(resultTags);
        });
      }
    );
  }

  initializeValues() {
    this.tags$ = this.store.pipe(
      select(tagsSelector)
    );
  }

  removeTag(event: MouseEvent, mySpan: HTMLSpanElement) {
    const tagNameToRemove = mySpan.innerText.replace(' X', '')
    this.tags = this.tags.filter(el => el.name !== tagNameToRemove);
    this.onTagDeletedEvent.emit(this.tags);
  }

  addTag(event: any) {
    event.preventDefault();
    if (this.tags.length < 5) {
      const tagName = event.target.value;
      const tagsCopy = [...this.tags];
      this.tags = [];
      tagsCopy.map((tag) => {
        this.tags.push(new Tag(tag.name, [], tag._id));
      });
      this.tags.push(new Tag(tagName));
      this.createTag(tagName);
      this.tag = '';
    } else {
      this.tag = '';
    }
  }

  createTag(tagName: string) {
    const tag = {
      tagName: tagName,
    }
    this.store.dispatch(addTagWithoutRedirectAction(tag));
  }

  private createNewTagsArrayToEmit(localTags: Tag[], tagsInStore: Tag[]): Tag[] {
    const tagsWithoutId = localTags.filter((tag) => tag._id === undefined && tag.name !== undefined);
    let result: Tag[] = [];
    if (tagsWithoutId.length > 0) {
      const tagsWithId = localTags.filter((tag) => tag._id !== undefined);
      result = tagsWithId;
      tagsWithoutId.map(tag => {
        tagsInStore.map(tagInStore => {
          if (tag.name === tagInStore.name) {
            result.push(new Tag(tag.name, [], tagInStore._id));
          }
        })
      })
    } else {
      result = localTags;
    }
    return result;
  }
}
