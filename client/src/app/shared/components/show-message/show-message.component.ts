import { Component, OnInit } from '@angular/core';
import { MessagesService } from '../../services/messages.service';

@Component({
  selector: 'ali-show-message',
  templateUrl: './show-message.component.html',
  styleUrls: ['./show-message.component.scss']
})
export class ShowMessageComponent implements OnInit {
  showPrimary:boolean = false;
  showSecondary: boolean = false;
  showSuccess: boolean = false;
  showDanger:boolean = false;
  showWarning: boolean = false;
  showInfo: boolean = false;
  message: string = '';
  mainDivClass: string = '';
  imgDiv: string = '';

  constructor(private messagesService: MessagesService) { }

  ngOnInit(): void {
    this.messagesService.showMessageEmitter.subscribe(message => {
      switch (message.type) {
        case 'primary':
          this.showPrimary = true;
          this.message = message.message;
          setTimeout(() => {
            this.showPrimary = false;
          }, message.timeout);
          break;
        case 'secondary':
          this.showSecondary = true;
          this.message = message.message;
          setTimeout(() => {
            this.showSecondary = false;
          }, message.timeout);
          break;
        case 'success':
          this.showSuccess = true;
          this.message = message.message;
          this.mainDivClass = 'show success';
          this.imgDiv = 'success';
          setTimeout(() => {
            this.showSuccess = false;
          }, message.timeout);
          break;
        case 'danger':
          this.showDanger = true;
          this.message = message.message;
          this.mainDivClass = 'show danger';
          this.imgDiv = 'danger';
          setTimeout(() => {
            this.showDanger = false;
          }, message.timeout);
          break;
        case 'warning':
          this.showWarning = true;
          this.message = message.message;
          setTimeout(() => {
            this.showWarning = false;
          }, message.timeout);
          break;
        case 'info':
          this.showInfo = true;
          this.message = message.message;
          setTimeout(() => {
            this.showInfo = false;
          }, message.timeout);
          break;
        default:
          break;
      }
    })
  }
}
