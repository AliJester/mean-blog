import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ali-right-sidebar',
  templateUrl: './right-sidebar.component.html',
  styleUrls: ['./right-sidebar.component.scss']
})
export class RightSidebarComponent {
  constructor(public router: Router) { }
}
