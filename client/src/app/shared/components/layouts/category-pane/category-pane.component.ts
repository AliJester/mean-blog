import { Component, OnInit } from '@angular/core';
import { CategoryService } from "../../../services/category.service";

@Component({
  selector: 'ali-category-pane',
  templateUrl: './category-pane.component.html',
  styleUrls: ['./category-pane.component.scss']
})
export class CategoryPaneComponent implements OnInit {

  categories = [];


  constructor(private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.categoryService.getAllCategories().subscribe( categories =>
        this.categories = categories,
      (err) => {},
    );
  }

}
