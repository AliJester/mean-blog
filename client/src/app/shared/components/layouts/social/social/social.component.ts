import { Component, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { NavigationEnd, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { currentPostSelector } from '../../../../../blog/store/selectors/blog.selector';

@Component({
  selector: 'ali-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.scss']
})
export class SocialComponent implements OnInit {
  constructor(
    @Inject(DOCUMENT) private document: any,
    private router: Router,
    private store: Store,
  ) {
  }

  vkShare = 'http://vkontakte.ru/share.php?&url=';
  fbShare = 'https://www.facebook.com/sharer/sharer.php?u=';
  liShare = 'https://www.linkedin.com/shareArticle?mini=true&url=';
  twShare = 'http://twitter.com/intent/tweet?text=';
  tgShare = 'https://telegram.me/share/url?url=';
  vkLink: string = '';
  fbLink: string = '';
  liLink: string = '';
  twLink: string = '';
  tgLink: string = '';
  blogLink: string = '';

  currentPostTitle: string;

  goToVk() {
    this.openWindow(this.vkLink);
  }

  goToFb() {
    this.openWindow(this.fbLink);
  }

  goToLi() {
    this.openWindow(this.liLink);
  }

  goToTw() {
    this.openWindow(this.twLink);
  }

  goToTg() {
    this.openWindow(this.tgLink);
  }

  openWindow(shareLinkToGo: string) {
    window.open(shareLinkToGo + this.blogLink, '_blank', 'width=600,height=400');
  }

  ngOnInit(): void {
    this.store.pipe(select(currentPostSelector)).subscribe(currentPost => {
      if (currentPost != null) {
        this.currentPostTitle = currentPost.title;
        this.initValues();
      }
    });

    this.router.events.subscribe((res) => {
        if (res instanceof NavigationEnd) {
          this.initValues();
        }
      }
    );
  }

  initValues() {
    this.blogLink = encodeURIComponent(this.document.location.href);
    this.vkLink = this.vkShare + this.blogLink;
    this.fbLink = this.fbShare + this.blogLink;
    this.liLink = this.liShare + this.blogLink;
    this.twLink = this.twShare + encodeURIComponent(this.currentPostTitle)  + '&url=';// + this.blogLink;
    this.tgLink = this.tgShare + this.blogLink;
  }
}
