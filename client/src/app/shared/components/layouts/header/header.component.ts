import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../services/auth.service';
import { MessagesService } from '../../../services/messages.service';
import { HOME } from '../../../utils/constants/urlConstants';

@Component({
  selector: 'ali-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  logoutSubscription: Subscription;


  constructor(public authService: AuthService,
              private router: Router,
              private messagesService: MessagesService) {
  }

  ngOnInit(): void {
  }

  logoutUser() {
    this.logoutSubscription =
      this.authService.logout()
        .subscribe(
          () => {
            this.router.navigate([HOME]);
            this.messagesService.success('You are logged out.');
            this.messagesService.clearMessage();
          },
          (error) => {
            this.messagesService.danger(error.error.message);
          }
        );
  }

  goToHome() {
    this.router.navigate([HOME]);
  }

  ngOnDestroy(): void {
    if (this.logoutSubscription) {
      this.logoutSubscription.unsubscribe();
    }
  }
}
