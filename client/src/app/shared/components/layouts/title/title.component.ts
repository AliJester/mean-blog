import { Component, Inject, Input, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { DAY_PART } from '../../../utils/constants/dayPartConstants';

@Component({
  selector: 'ali-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.scss']
})
export class TitleComponent implements OnInit {
  url: string = '';
  @Input('showDefault') showDefaultProps: boolean = false;

  constructor(@Inject(DOCUMENT) private document: any) {
  }

  ngOnInit(): void {
    const domain = this.document.location.origin;
    let imgAddress = '/images/default.jpg';

    if (!this.showDefaultProps) {
      const timeHours = new Date().getHours();
      if (timeHours >= DAY_PART.NIGHT.BEGINNING && timeHours <= DAY_PART.NIGHT.ENDING) {
        imgAddress = '/images/night.jpg';
      } else if (timeHours >= DAY_PART.MORNING.BEGINNING && timeHours <= DAY_PART.MORNING.ENDING) {
        imgAddress = '/images/morning.jpg';
      } else if (timeHours >= DAY_PART.DAY.BEGINNING && timeHours <= DAY_PART.DAY.ENDING) {
        imgAddress = '/images/day.jpg';
      } else if (timeHours >= DAY_PART.EVENING.BEGINNING && timeHours <= DAY_PART.EVENING.ENDING) {
        imgAddress = '/images/evening.jpg';
      }
    }
    this.url = domain + imgAddress;
  }
}
