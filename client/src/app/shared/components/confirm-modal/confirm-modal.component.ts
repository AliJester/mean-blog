import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'ali-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})

export class ConfirmModalComponent implements OnInit {
  message: any;
  constructor(
    private modalService: ModalService
  ) { }

  ngOnInit(): any {
    this.modalService.getMessage().subscribe(message => {
      this.message = message;
    });
  }
}
