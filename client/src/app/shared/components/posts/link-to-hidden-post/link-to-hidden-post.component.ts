import { Component, EventEmitter, Inject, Input, OnInit, Output } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { LinkGenerationService } from '../../../../admin/shared/services/linkGeneration.service';

@Component({
  selector: 'ali-link-to-hidden-post',
  templateUrl: './link-to-hidden-post.component.html',
  styleUrls: ['./link-to-hidden-post.component.scss']
})
export class LinkToHiddenPostComponent implements OnInit {
  @Input('isHidden') isHiddenProps: boolean = true;
  @Input('availableByToken') availableByTokenProps: boolean = false;
  @Input('availabilityToken') availabilityTokenProps: string = 'empty';
  @Input('coolId') coolIdProps: string = 'empty';
  @Output() onTokenGenerated = new EventEmitter<string>();

  availableByToken: boolean = false;

  showPostLinkBlock: boolean = false;
  availableByTokenTime: number = 15;
  availableByTokenResult: string = 'empty';
  token: string = 'empty';

  constructor(private linkGenerationService: LinkGenerationService,
              @Inject(DOCUMENT) private document: any) {
  }

  ngOnInit(): void {
    this.availableByToken = this.availableByTokenProps;
    this.showPostLinkBlock = this.availableByToken;
    this.token = this.availabilityTokenProps;
    this.availableByTokenResult = this.getDomain() + '/post/' + this.coolIdProps + '?token=' + this.token;

  }

  isAvailableByTokenCheck(event: Event) {
    this.showPostLinkBlock = (<HTMLInputElement>event.target).checked;
    if (!this.showPostLinkBlock) {
      this.onTokenGenerated.emit(null);
    }
  }

  generateLink() {
    this.linkGenerationService.generate(this.availableByTokenTime).subscribe((result) => {
        this.token = result.token;
        this.availableByTokenResult = this.getDomain() + '/post/' + this.coolIdProps + '?token=' + this.token;
        this.onTokenGenerated.emit(this.token);
      }
    )
  }

  getDomain() {
    return this.document.location.origin;
  }

  copyLink( resultElement: HTMLTextAreaElement) {
    resultElement.select();
    document.execCommand('copy');
  }
}
