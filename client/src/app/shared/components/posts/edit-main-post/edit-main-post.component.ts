import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ali-edit-main-post',
  templateUrl: './edit-main-post.component.html',
  styleUrls: ['./edit-main-post.component.scss']
})
export class EditMainPostComponent implements OnInit {

  @Input() mainPostBriefDesc: string = '';
  @Input() isMain: boolean = false;
  @Output() onMainPostBriefDescEditEvent = new EventEmitter<string>();
  @Output() onIsMainChangeEvent = new EventEmitter<boolean>();

  showMainPostText: boolean = false;
  mainPostBriefDescCounter: number = 0;
  spanStyle: string = 'green';

  constructor() { }

  ngOnInit(): void {
    this.showMainPostText = this.isMain;
    this.mainPostBriefDescCounter = this.mainPostBriefDesc ? this.mainPostBriefDesc.length : 0;
    this.setSpanStyle(this.mainPostBriefDescCounter);
  }

  mainTextInput(event: Event) {
    this.mainPostBriefDescCounter = this.mainPostBriefDesc.length;
    this.setSpanStyle(this.mainPostBriefDescCounter);
  }

  isMainCheck(event: Event) {
    this.showMainPostText = (<HTMLInputElement>event.target).checked;
    this.onIsMainChangeEvent.emit(this.showMainPostText);
  }

  private setSpanStyle(counter: number) {
    if (counter < 50) {
      this.spanStyle = 'green';
    } else if (counter < 75) {
      this.spanStyle = 'gold';
    } else if (counter < 95) {
      this.spanStyle = 'orange';
    } else {
      this.spanStyle = 'red';
    }
  }

  blurEvent() {
    this.onMainPostBriefDescEditEvent.emit(this.mainPostBriefDesc);
  }
}
