import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  API_ADDITIONAL_DATA_COMMENTS_LAST_SEEN_DATE,
} from '../utils/constants/urlConstants';
import { AdditionalData } from '../models/AdditionalData.model';

@Injectable({
  providedIn: 'root'
})
export class AdditionalDataService {
  constructor(private http: HttpClient) {
  }

  setCommentsLastSeenData(commentsLastSeenDate: Date): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<AdditionalData>(API_ADDITIONAL_DATA_COMMENTS_LAST_SEEN_DATE + '/', {commentsLastSeenDate}, {headers: headers});
  }
  getCommentsLastSeenData(): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.get<AdditionalData>(API_ADDITIONAL_DATA_COMMENTS_LAST_SEEN_DATE, { headers: headers});
  }
}
