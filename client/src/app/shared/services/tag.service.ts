import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tag } from '../models/Tag.model';
import { Message } from '../models/Message.model';
import { API_TAGS } from '../utils/constants/urlConstants';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private http : HttpClient ) {}

  createTag(tag:Tag): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<Tag>(API_TAGS, tag,
      { headers: headers});
  }

  updateTag(tag:Tag): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.patch<Tag>(API_TAGS + `/${tag._id}`, tag, { headers: headers});
  }

  getAllTags(): Observable<Tag[]> {
    return this.http.get<Tag[]>(API_TAGS );
  }

  getTagById(id) {
    return this.http.get<Tag>(API_TAGS + `/${id}` );
  }

  deleteTag(id) {
    return this.http.delete<Message>(API_TAGS +`/${id}` );
  }
}
