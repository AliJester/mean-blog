import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Category } from '../models/Category.model';
import { Message } from '../models/Message.model';
import { API_CATEGORIES } from '../utils/constants/urlConstants';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private http : HttpClient ) {}

  createCategory(category:Category): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<Category>(API_CATEGORIES, category,
      { headers: headers});
  }

  updateCategory(category:Category): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.patch<Category>(API_CATEGORIES + `/${category._id}`, category, { headers: headers});
  }

  getAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(API_CATEGORIES );
  }

  getCategoryById(id) {
    return this.http.get<Category>(API_CATEGORIES + `/${id}` );
  }

  deleteCategory(id) {
    return this.http.delete<Message>(API_CATEGORIES + `/${id}` );
  }
}
