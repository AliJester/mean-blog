import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { BLOG_POSTS_COMMENTS_URL } from '../../blog/shared/blogConstants';
import { PostComment } from '../models/PostComment.model';
import { PostSubComment } from '../models/PostSubComment.model';
import { Message } from '../models/Message.model';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  setNewCommentsAmountSubject = new Subject<number>();

  constructor(private http : HttpClient ) {}

  getCommentsByPostId(postId: string): Observable<PostComment[]> {
    return this.http.get<PostComment[]>(BLOG_POSTS_COMMENTS_URL + `/${postId}` );
  }

  createComment(comment: PostComment): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<PostComment>(BLOG_POSTS_COMMENTS_URL + '/', comment,
      { headers: headers});
  }

  updateComment(comment: PostSubComment, parentCommentId: string): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.patch<PostComment>(BLOG_POSTS_COMMENTS_URL + `/${parentCommentId}`, comment,
      { headers: headers});
  }

  deleteComment(id, parentId) {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    let options =
      { params: new HttpParams()
          .set('parentId', parentId),
        headers: headers
      }
    return this.http.delete<Message>(BLOG_POSTS_COMMENTS_URL + `/${id}`, options );
  }

  getAllComments(): Observable<PostComment[]> {
    return this.http.get<PostComment[]>(BLOG_POSTS_COMMENTS_URL + '/all' );
  }

  getNewComments(): Observable<PostComment[]> {
      return this.http.get<PostComment[]>(BLOG_POSTS_COMMENTS_URL + '/new' );
  }

  getNewCommentsCount(): Observable<any> {
    return this.http.get<any>(BLOG_POSTS_COMMENTS_URL + '/new/count' );
  }
}
