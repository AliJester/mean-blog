import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HideComponentService {
  hideComponentEmitter = new Subject<boolean>();

  constructor() { }

  hide() {
    this.hideComponentEmitter.next(true);
  }

  clearHideComponent(){
    // const message = new Message('', 0, '');
    // this.showMessageEmitter.next(message);
  }
}
