import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TitleImage } from '../models/TitleImage.model';
import { TitleImageList } from '../models/TitleImagesList.model';
import { DAY_PART } from '../utils/constants/dayPartConstants';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  readonly IMAGE_ARRAY = 'images';
  readonly JPG_EXTENSION= '.jpg';

  constructor(private http : HttpClient ) {}

  createOrUpdate(images?: TitleImageList): Observable<any> {
    const formData = new FormData();
    if (images) {
      if (images.nightImage) formData.append(this.IMAGE_ARRAY, images.nightImage, DAY_PART.NIGHT.NAME + this.JPG_EXTENSION);
      if (images.morningImage) formData.append(this.IMAGE_ARRAY, images.morningImage, DAY_PART.MORNING.NAME + this.JPG_EXTENSION);
      if (images.dayImage) formData.append(this.IMAGE_ARRAY, images.dayImage, DAY_PART.DAY.NAME + this.JPG_EXTENSION);
      if (images.eveningImage) formData.append(this.IMAGE_ARRAY, images.eveningImage, DAY_PART.EVENING.NAME + this.JPG_EXTENSION);
      if (images.defaultImage) formData.append(this.IMAGE_ARRAY, images.defaultImage, 'default' + this.JPG_EXTENSION);
    }
    return this.http.post('/api/settings/title', formData);
  }
  getTitle(): Observable<TitleImage> {
    return this.http.get<TitleImage>('/api/settings/title');
  }
}
