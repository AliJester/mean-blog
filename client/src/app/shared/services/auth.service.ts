import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { User } from '../models/User.model';
import { AUTH_AUTH, AUTH_LOGOUT, AUTH_REG } from '../utils/constants/urlConstants';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private token: any = null;
  user: any

  constructor(private http: HttpClient) {
  }

  registerUser(user: User): Observable<User> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json')
    return this.http.post<User>(AUTH_REG, user,
      {headers: headers});
  }

  login(user: User): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json')
    return this.http.post<{ token: string }>(AUTH_AUTH, user,
      {headers: headers})
      .pipe(
        tap(
          ({token, author}) => {
            this.setToken(token);
            localStorage.setItem('author', author);
          }
        )
      );
  }

  autoLogin(): Observable<any> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json')
    return this.http.get<{ token: string }>(AUTH_AUTH,
      {headers: headers})
      .pipe(
        tap(
          ({token}) => {
            this.setToken(token);
          }
        )
      );
  }

  setToken(token: string) {
    this.token = token;
  }

  getToken(): string {
    return this.token;
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  logout(): Observable<any> {
    this.setToken(null);
    localStorage.clear();
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json')
    return this.http.post<any>(AUTH_LOGOUT,
      {headers: headers})
      .pipe(
        tap(
          (token) => {
            this.setToken(null);
          }
        )
      );
  }
}
