import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private subject = new Subject<any>();

  confirmThis(message: string, confirm: () => void, decline: () => void): any {
    this.setConfirmation(message, confirm, decline);
  }

  setConfirmation(message: string, confirm: () => void, decline: () => void): any {
    const that = this;
    this.subject.next({
      type: 'confirm',
      text: message,
      confirm(): any {
        that.subject.next();
        confirm();
      },
      decline(): any {
        that.subject.next();
        decline();
      }
    });
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
}
