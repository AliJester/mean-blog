import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Message } from '../models/Message.model';
import { DANGER_MESSAGE, DEFAULT_ERROR_MESSAGE, SUCCESS_MESSAGE } from '../utils/constants/messageConstants';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  showMessageEmitter = new BehaviorSubject<Message>({
    message: '',
    timeout: 0,
    type: ''});

  constructor() { }

  success(messageText: string) {
    const message = new Message(messageText, 2500, SUCCESS_MESSAGE);
    this.showMessageEmitter.next(message);
  }

  danger(messageText: string){
    const message = new Message(messageText, 2500, DANGER_MESSAGE);
    this.showMessageEmitter.next(message);
  }

  defaultMessage(){
    const message = new Message(DEFAULT_ERROR_MESSAGE, 2500, DANGER_MESSAGE);
    this.showMessageEmitter.next(message);
  }

  clearMessage(){
    const message = new Message('', 0, '');
    this.showMessageEmitter.next(message);
  }
}
