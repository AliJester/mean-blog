import { BlogStateInterface } from '../../../blog/store/types/blogState.interface';
import { AdminStateInterface } from '../../../admin/store/types/adminState.interface';

export interface AppStateInterface {
  blog: BlogStateInterface;
  admin: AdminStateInterface;
}
