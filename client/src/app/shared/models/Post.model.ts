import { Category } from './Category.model';
import { Tag } from './Tag.model';

export interface Post {
  category: Category
  tags: Tag[]
  title: string
  photo: string
  text:string
  author:string
  updatedDate:Date
  isMain: boolean
  isHidden: boolean
  availableByToken: boolean
  availabilityToken: string
  mainPostBriefDesc: string
  coolId: string
  createdDate: Date
  cutPosition:number
  _id?: string
}
