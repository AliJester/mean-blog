export interface QueryParams {
  limit: number,
  page: number,
  tag?: string,
  category?: string,
  author?: string,
}
