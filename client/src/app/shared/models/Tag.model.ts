import { Post } from './Post.model';

export class Tag {
  name: string;
  _id?: string;
  posts?: string[];

  constructor(name: string, posts?: string[],  id?: string) {
    this._id = id;
    this.name = name;
    this.posts = posts;
  }
}
