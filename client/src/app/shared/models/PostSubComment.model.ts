export interface PostSubComment {
  _id?: string
  author: string
  replyPostAuthor: string
  replyPostId: string
  text: string
  createdDate: Date
}
