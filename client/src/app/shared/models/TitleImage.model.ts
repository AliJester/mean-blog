export interface TitleImage {
  nightImageSrc?: string
  morningImageSrc?: string
  dayImageSrc?: string
  eveningImageSrc?: string
  defaultImageSrc?: string
  id?: string
  _id?: string
}
