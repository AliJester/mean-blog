export class Category {
  name: string;
  _id?: string;

  constructor(name: string, id?: string) {
    this._id = id;
    this.name = name;
  }

}
