export interface TitleImageList {
  nightImage?: File
  morningImage?: File
  dayImage?: File
  eveningImage?: File
  defaultImage?: File
}
