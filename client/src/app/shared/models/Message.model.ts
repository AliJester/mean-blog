export class Message {
  message: string;
  timeout: number;
  type: string

  constructor(message: string, timeout: number, type: string) {
    this.message = message;
    this.timeout = timeout;
    this.type = type;
  }
}
