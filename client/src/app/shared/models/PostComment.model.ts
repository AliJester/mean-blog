import { PostSubComment } from './PostSubComment.model';

export interface PostComment {
  _id?: string
  author: string
  text: string
  createdDate: Date
  postId: string
  postCoolId: string
  subComments?: PostSubComment[]
}
