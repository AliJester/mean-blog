import { Directive } from '@angular/core';
import { FormControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';

@Directive({
  selector: '[aliPassword]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useClass: PasswordValidatorDirective,
      multi: true
    }
  ]
})
export class PasswordValidatorDirective implements Validator {

  validator: ValidatorFn;
  constructor() {
    this.validator = this.passwordValidator();
  }

  validate(control: FormControl) {
    return this.validator(control);
  }

  passwordValidator(): ValidatorFn {
    return (control: FormControl) => {
      if (control.value != null && control.value !== '') {
        let isValid = /[A-Z].*\d|\d.*[A-Z]/.test(control.value);
        return isValid ? null : { passwordNotValid: true };
      } else {
        return null;
      }
    };
  }
}
