import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';

@Directive({
  selector: '[aliStringWithoutSpaces]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: StringWithoutSpacesDirective,
      multi: true,
    }
  ]
})
export class StringWithoutSpacesDirective implements Validator {

  validator: ValidatorFn;
  constructor() {
    this.validator = this.stringWithoutSpacesValidator();
  }

  validate(control: AbstractControl) {
    return this.validator(control);
  }

  stringWithoutSpacesValidator(): ValidatorFn {
    return (control: AbstractControl) => {
      if (control.value != null && control.value !== '') {
        let isValid = /^\S+$/.test(control.value);
        return isValid ? null : { stringWithoutSpaces: true };
      } else {
        return null;
      }
    };
  }
}
