import { Attribute, Directive } from '@angular/core';
import { FormControl, NG_VALIDATORS, Validator } from '@angular/forms';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[aliConfirmPassword]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useClass: ConfirmPasswordValidatorDirective,
      multi: true
    }
  ]
})
export class ConfirmPasswordValidatorDirective implements Validator {
  constructor(@Attribute('aliConfirmPassword') public PasswordControl: string) { }

  validate(control: FormControl) {

    const Password = control.root.get(this.PasswordControl);
    const ConfirmPassword = control;

    if (ConfirmPassword.value === null) {
      return null;
    }

    if (Password) {
      const subscription: Subscription = Password.valueChanges.subscribe(() => {
        ConfirmPassword.updateValueAndValidity();
        subscription.unsubscribe();
      });
    }
    return Password && Password.value !== ConfirmPassword.value ? { passwordMatchError: true } : null;
  }
}
