export const DAY_PART = {
  NIGHT: {
    BEGINNING: 0,
    ENDING: 5,
    NAME: 'night',
  },
  MORNING: {
    BEGINNING: 6,
    ENDING: 11,
    NAME: 'morning',
  },
  DAY: {
    BEGINNING: 12,
    ENDING: 17,
    NAME: 'day',
  },
  EVENING: {
    BEGINNING: 18,
    ENDING: 23,
    NAME: 'evening',
  },
}


