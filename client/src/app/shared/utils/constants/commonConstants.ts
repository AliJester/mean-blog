export const ADMIN_API_URL = '/api/admin';
export const ADMIN_URL = '/admin';
export const LOCAL_STORAGE = 'blog-auth-token';
export const CUT_POST = '#CreateCutHere/#';
