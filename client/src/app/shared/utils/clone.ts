export const clone = function(obj) {
  let clonedObject;

  if (obj === null || typeof obj !== 'object') {
    return obj;
  }

  if (obj instanceof Array) {
    clonedObject = [];
    for (let i = 0; i < obj.length; i++) {
      clonedObject[i] = clone(obj[i]);
    }
    return clonedObject;
  }

  if (obj instanceof Date) {
    clonedObject = new Date(obj.valueOf());
    return clonedObject;
  }

  if (obj instanceof RegExp) {
    clonedObject = RegExp(obj.source, obj.flags);
    return clonedObject;
  }

  if (obj instanceof Object) {
    clonedObject = new obj.constructor();
    for (let attr in obj) {
      if (obj.hasOwnProperty(attr)) {
        clonedObject[attr] = clone(obj[attr]);
      }
    }
    return clonedObject;
  }
  throw new Error('Object not cloned');
}
