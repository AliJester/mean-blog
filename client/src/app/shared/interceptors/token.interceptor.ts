import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { AUTH, NOT_FOUND } from '../utils/constants/urlConstants';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService,
    private router: Router,
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.authService.isAuthenticated()) {
      // req = req.clone({
      //   setParams: {
      //     auth: this.auth.getToken()
      //   }
      // });
      //req = this.addToken(req, this.authService.getToken());
    }
    return next.handle(req)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status === 401) {
            //this.auth.logout()
            this.router.navigate([AUTH], {
              queryParams: {
                authFailed: true,
              }
            })
          }
          if (error.status === 404) {
            this.router.navigate([NOT_FOUND])
          }
          return throwError(error);
        })
      );
  }
  private addToken(req: HttpRequest<any>, token: string) {
    return req.clone({
      setHeaders: {
        Authorization: token,
      }
    });
  }
}
