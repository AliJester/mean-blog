import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/layouts/header/header.component';
import { RouterModule } from '@angular/router';
import { ShowMessageComponent } from './components/show-message/show-message.component';
import { CodeEditorPipe } from './pipes/code-editor.pipe';
import { LoaderComponent } from './components/loader/loader.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { LinkToHiddenPostComponent } from './components/posts/link-to-hidden-post/link-to-hidden-post.component';
import { TagsComponent } from './components/tags/tags.component';
import { FormsModule } from '@angular/forms';
import { EditMainPostComponent } from './components/posts/edit-main-post/edit-main-post.component';
import { ConfirmModalComponent } from './components/confirm-modal/confirm-modal.component';

@NgModule({
  declarations: [
    CodeEditorPipe,
    EditMainPostComponent,
    HeaderComponent,
    LinkToHiddenPostComponent,
    LoaderComponent,
    PaginationComponent,
    ShowMessageComponent,
    TagsComponent,
    ConfirmModalComponent,

  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
  ],
    exports: [
        CodeEditorPipe,
        EditMainPostComponent,
        HeaderComponent,
        LoaderComponent,
        LinkToHiddenPostComponent,
        PaginationComponent,
        ShowMessageComponent,
        TagsComponent,
        ConfirmModalComponent,
    ]
})
export class SharedModule {
}
