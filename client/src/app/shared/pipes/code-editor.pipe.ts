import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'codeEditor'
})
export class CodeEditorPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    const result = this.getCodePart(value);
    return result;
  }

  getCodePart(text: string) : string {
    const codeTextOpenPos = text.indexOf('<pre class="ql-syntax" spellcheck="false">');
    if (!~codeTextOpenPos) {
      return text;
    }
    const codeTextClosePos = text.indexOf('</pre>');
    let resultBeforeCode = text.slice(0, codeTextOpenPos); //, codeTextClosePos);
    let resultAfterCode = text.slice(codeTextClosePos+6,); //, codeTextClosePos);
    const codePart = text.slice(codeTextOpenPos + 42, codeTextClosePos);
    const finalResult = resultBeforeCode
      + text.slice(codeTextOpenPos, codeTextOpenPos + 42)
      + this.generateTable(this.splitCodeIntoLines(codePart))
      + '</pre>'
      + resultAfterCode;

    return finalResult;

  }

  splitCodeIntoLines(text: string) : string[] {
    const lines = text.split("\n");
    return lines;
  }

  generateTable(lines: string[]) : string {
    let result = '';
    const numLines = lines.length;
    result = this.generateLineNumbers(numLines);
    return result;
  }

  generateLineNumbers(lineNum:number): string {
    let result = '';
    for (let i = 1; i <= lineNum; i++) {
      result += '<div class="line">' + i + '</div>';
    }
    return result;
  }
}
