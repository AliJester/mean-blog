import { Pipe, PipeTransform } from '@angular/core';
import { CUT_POST } from '../utils/constants/commonConstants';

@Pipe({
  name: 'postText'
})
export class PostTextPipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    return value.replace(CUT_POST, '');
  }

}
